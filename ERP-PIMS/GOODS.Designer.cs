﻿namespace ERP_PIMS
{
    partial class GOODS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label grnoLabel;
            System.Windows.Forms.Label grdateLabel;
            System.Windows.Forms.Label po_noLabel;
            System.Windows.Forms.Label itemcodeLabel;
            System.Windows.Forms.Label qtyrcvdLabel;
            System.Windows.Forms.Label qtyaccptdLabel;
            System.Windows.Forms.Label rateLabel;
            System.Windows.Forms.Label valueLabel;
            System.Windows.Forms.Label statusLabel;
            System.Windows.Forms.Label po_noLabel1;
            System.Windows.Forms.Label grnoLabel1;
            System.Windows.Forms.Label itemcodeLabel1;
            System.Windows.Forms.Label qtyrjctdLabel;
            System.Windows.Forms.Label reasonLabel;
            System.Windows.Forms.Label grvnoLabel;
            System.Windows.Forms.Label quotationnoLabel;
            System.Windows.Forms.Label quotationnoLabel1;
            System.Windows.Forms.Label rfqnoLabel;
            System.Windows.Forms.Label prnoLabel;
            System.Windows.Forms.Label prnoLabel1;
            System.Windows.Forms.Label rfqnoLabel1;
            this.rFQMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.lblUsercode = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.grvnoTextBox = new System.Windows.Forms.TextBox();
            this.rfqnoTextBox = new System.Windows.Forms.TextBox();
            this.quotationMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.prnoTextBox = new System.Windows.Forms.TextBox();
            this.prnoComboBox = new System.Windows.Forms.ComboBox();
            this.pRMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.po_noTextBox1 = new System.Windows.Forms.TextBox();
            this.grnoTextBox1 = new System.Windows.Forms.TextBox();
            this.itemcodeTextBox1 = new System.Windows.Forms.TextBox();
            this.qtyrjctdTextBox = new System.Windows.Forms.TextBox();
            this.reasonTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.po_noComboBox1 = new System.Windows.Forms.ComboBox();
            this.rfqnoComboBox = new System.Windows.Forms.ComboBox();
            this.quantityTextBox = new System.Windows.Forms.TextBox();
            this.rateTextBox = new System.Windows.Forms.TextBox();
            this.quotationnoComboBox = new System.Windows.Forms.ComboBox();
            this.pOMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.quotationnoTextBox = new System.Windows.Forms.TextBox();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.po_noComboBox = new System.Windows.Forms.ComboBox();
            this.gRMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grnoTextBox = new System.Windows.Forms.TextBox();
            this.grdateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.qtyaccptdTextBox = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gRVMasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gRVMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gRMasterDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLabel12 = new System.Windows.Forms.LinkLabel();
            this.linkLabel11 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.linkLabel8 = new System.Windows.Forms.LinkLabel();
            this.linkLabel7 = new System.Windows.Forms.LinkLabel();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.linkLabel13 = new System.Windows.Forms.LinkLabel();
            this.linkLabel14 = new System.Windows.Forms.LinkLabel();
            this.gRMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.GRMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.gRVMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.GRVMasterTableAdapter();
            this.pOMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.POMasterTableAdapter();
            this.pRMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.PRMasterTableAdapter();
            this.quotationMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.QuotationMasterTableAdapter();
            this.rFQMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.RFQMasterTableAdapter();
            grnoLabel = new System.Windows.Forms.Label();
            grdateLabel = new System.Windows.Forms.Label();
            po_noLabel = new System.Windows.Forms.Label();
            itemcodeLabel = new System.Windows.Forms.Label();
            qtyrcvdLabel = new System.Windows.Forms.Label();
            qtyaccptdLabel = new System.Windows.Forms.Label();
            rateLabel = new System.Windows.Forms.Label();
            valueLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            po_noLabel1 = new System.Windows.Forms.Label();
            grnoLabel1 = new System.Windows.Forms.Label();
            itemcodeLabel1 = new System.Windows.Forms.Label();
            qtyrjctdLabel = new System.Windows.Forms.Label();
            reasonLabel = new System.Windows.Forms.Label();
            grvnoLabel = new System.Windows.Forms.Label();
            quotationnoLabel = new System.Windows.Forms.Label();
            quotationnoLabel1 = new System.Windows.Forms.Label();
            rfqnoLabel = new System.Windows.Forms.Label();
            prnoLabel = new System.Windows.Forms.Label();
            prnoLabel1 = new System.Windows.Forms.Label();
            rfqnoLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rFQMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quotationMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRMasterBindingSource)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pOMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRMasterBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gRVMasterDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRVMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRMasterDataGridView)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // grnoLabel
            // 
            grnoLabel.AutoSize = true;
            grnoLabel.Location = new System.Drawing.Point(13, 73);
            grnoLabel.Name = "grnoLabel";
            grnoLabel.Size = new System.Drawing.Size(43, 13);
            grnoLabel.TabIndex = 21;
            grnoLabel.Text = "GR No:";
            // 
            // grdateLabel
            // 
            grdateLabel.AutoSize = true;
            grdateLabel.Location = new System.Drawing.Point(13, 100);
            grdateLabel.Name = "grdateLabel";
            grdateLabel.Size = new System.Drawing.Size(52, 13);
            grdateLabel.TabIndex = 23;
            grdateLabel.Text = "GR Date:";
            // 
            // po_noLabel
            // 
            po_noLabel.AutoSize = true;
            po_noLabel.Location = new System.Drawing.Point(13, 125);
            po_noLabel.Name = "po_noLabel";
            po_noLabel.Size = new System.Drawing.Size(42, 13);
            po_noLabel.TabIndex = 25;
            po_noLabel.Text = "PO No:";
            // 
            // itemcodeLabel
            // 
            itemcodeLabel.AutoSize = true;
            itemcodeLabel.Location = new System.Drawing.Point(13, 151);
            itemcodeLabel.Name = "itemcodeLabel";
            itemcodeLabel.Size = new System.Drawing.Size(54, 13);
            itemcodeLabel.TabIndex = 27;
            itemcodeLabel.Text = "Itemcode:";
            // 
            // qtyrcvdLabel
            // 
            qtyrcvdLabel.AutoSize = true;
            qtyrcvdLabel.Location = new System.Drawing.Point(13, 234);
            qtyrcvdLabel.Name = "qtyrcvdLabel";
            qtyrcvdLabel.Size = new System.Drawing.Size(87, 13);
            qtyrcvdLabel.TabIndex = 29;
            qtyrcvdLabel.Text = "Quatity recieved:";
            // 
            // qtyaccptdLabel
            // 
            qtyaccptdLabel.AutoSize = true;
            qtyaccptdLabel.Location = new System.Drawing.Point(13, 260);
            qtyaccptdLabel.Name = "qtyaccptdLabel";
            qtyaccptdLabel.Size = new System.Drawing.Size(97, 13);
            qtyaccptdLabel.TabIndex = 31;
            qtyaccptdLabel.Text = "Quantity accepted:";
            // 
            // rateLabel
            // 
            rateLabel.AutoSize = true;
            rateLabel.Location = new System.Drawing.Point(13, 178);
            rateLabel.Name = "rateLabel";
            rateLabel.Size = new System.Drawing.Size(33, 13);
            rateLabel.TabIndex = 33;
            rateLabel.Text = "Rate:";
            // 
            // valueLabel
            // 
            valueLabel.AutoSize = true;
            valueLabel.Location = new System.Drawing.Point(13, 204);
            valueLabel.Name = "valueLabel";
            valueLabel.Size = new System.Drawing.Size(37, 13);
            valueLabel.TabIndex = 35;
            valueLabel.Text = "Value:";
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Location = new System.Drawing.Point(13, 289);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(40, 13);
            statusLabel.TabIndex = 37;
            statusLabel.Text = "Status:";
            // 
            // po_noLabel1
            // 
            po_noLabel1.AutoSize = true;
            po_noLabel1.Location = new System.Drawing.Point(26, 131);
            po_noLabel1.Name = "po_noLabel1";
            po_noLabel1.Size = new System.Drawing.Size(42, 13);
            po_noLabel1.TabIndex = 24;
            po_noLabel1.Text = "PO No:";
            // 
            // grnoLabel1
            // 
            grnoLabel1.AutoSize = true;
            grnoLabel1.Location = new System.Drawing.Point(26, 158);
            grnoLabel1.Name = "grnoLabel1";
            grnoLabel1.Size = new System.Drawing.Size(43, 13);
            grnoLabel1.TabIndex = 26;
            grnoLabel1.Text = "GR No:";
            // 
            // itemcodeLabel1
            // 
            itemcodeLabel1.AutoSize = true;
            itemcodeLabel1.Location = new System.Drawing.Point(24, 185);
            itemcodeLabel1.Name = "itemcodeLabel1";
            itemcodeLabel1.Size = new System.Drawing.Size(54, 13);
            itemcodeLabel1.TabIndex = 28;
            itemcodeLabel1.Text = "Itemcode:";
            // 
            // qtyrjctdLabel
            // 
            qtyrjctdLabel.AutoSize = true;
            qtyrjctdLabel.Location = new System.Drawing.Point(26, 211);
            qtyrjctdLabel.Name = "qtyrjctdLabel";
            qtyrjctdLabel.Size = new System.Drawing.Size(90, 13);
            qtyrjctdLabel.TabIndex = 30;
            qtyrjctdLabel.Text = "Quantity rejected:";
            // 
            // reasonLabel
            // 
            reasonLabel.AutoSize = true;
            reasonLabel.Location = new System.Drawing.Point(26, 237);
            reasonLabel.Name = "reasonLabel";
            reasonLabel.Size = new System.Drawing.Size(47, 13);
            reasonLabel.TabIndex = 32;
            reasonLabel.Text = "Reason:";
            // 
            // grvnoLabel
            // 
            grvnoLabel.AutoSize = true;
            grvnoLabel.Location = new System.Drawing.Point(23, 73);
            grvnoLabel.Name = "grvnoLabel";
            grvnoLabel.Size = new System.Drawing.Size(41, 13);
            grvnoLabel.TabIndex = 33;
            grvnoLabel.Text = "GrvNo:";
            // 
            // quotationnoLabel
            // 
            quotationnoLabel.AutoSize = true;
            quotationnoLabel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rFQMasterBindingSource, "rfqno", true));
            quotationnoLabel.Location = new System.Drawing.Point(37, 327);
            quotationnoLabel.Name = "quotationnoLabel";
            quotationnoLabel.Size = new System.Drawing.Size(66, 13);
            quotationnoLabel.TabIndex = 41;
            quotationnoLabel.Text = "quotationno:";
            quotationnoLabel.Visible = false;
            // 
            // rFQMasterBindingSource
            // 
            this.rFQMasterBindingSource.DataMember = "RFQMaster";
            this.rFQMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // quotationnoLabel1
            // 
            quotationnoLabel1.AutoSize = true;
            quotationnoLabel1.Location = new System.Drawing.Point(8, 391);
            quotationnoLabel1.Name = "quotationnoLabel1";
            quotationnoLabel1.Size = new System.Drawing.Size(66, 13);
            quotationnoLabel1.TabIndex = 42;
            quotationnoLabel1.Text = "quotationno:";
            quotationnoLabel1.Visible = false;
            // 
            // rfqnoLabel
            // 
            rfqnoLabel.AutoSize = true;
            rfqnoLabel.Location = new System.Drawing.Point(20, 388);
            rfqnoLabel.Name = "rfqnoLabel";
            rfqnoLabel.Size = new System.Drawing.Size(34, 13);
            rfqnoLabel.TabIndex = 45;
            rfqnoLabel.Text = "rfqno:";
            rfqnoLabel.Visible = false;
            // 
            // prnoLabel
            // 
            prnoLabel.AutoSize = true;
            prnoLabel.Location = new System.Drawing.Point(57, 324);
            prnoLabel.Name = "prnoLabel";
            prnoLabel.Size = new System.Drawing.Size(31, 13);
            prnoLabel.TabIndex = 46;
            prnoLabel.Text = "prno:";
            prnoLabel.Visible = false;
            // 
            // prnoLabel1
            // 
            prnoLabel1.AutoSize = true;
            prnoLabel1.Location = new System.Drawing.Point(23, 413);
            prnoLabel1.Name = "prnoLabel1";
            prnoLabel1.Size = new System.Drawing.Size(31, 13);
            prnoLabel1.TabIndex = 48;
            prnoLabel1.Text = "prno:";
            prnoLabel1.Visible = false;
            // 
            // rfqnoLabel1
            // 
            rfqnoLabel1.AutoSize = true;
            rfqnoLabel1.Location = new System.Drawing.Point(40, 416);
            rfqnoLabel1.Name = "rfqnoLabel1";
            rfqnoLabel1.Size = new System.Drawing.Size(34, 13);
            rfqnoLabel1.TabIndex = 49;
            rfqnoLabel1.Text = "rfqno:";
            rfqnoLabel1.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(80, 357);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(143, 24);
            this.button3.TabIndex = 2;
            this.button3.Text = "ADD TO GRV MASTER";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 528);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Generate Report";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 557);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(143, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(81, 77);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 16;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(17, 77);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(53, 17);
            this.radioButton1.TabIndex = 8;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "MR id";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(17, 17);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(45, 17);
            this.radioButton3.TabIndex = 6;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Item";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(17, 48);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(47, 17);
            this.radioButton4.TabIndex = 7;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "User";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(81, 16);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 1;
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(81, 48);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 4;
            // 
            // lblUsercode
            // 
            this.lblUsercode.AutoSize = true;
            this.lblUsercode.Location = new System.Drawing.Point(13, 483);
            this.lblUsercode.Name = "lblUsercode";
            this.lblUsercode.Size = new System.Drawing.Size(0, 13);
            this.lblUsercode.TabIndex = 19;
            this.lblUsercode.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cambria", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(7, -2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 28);
            this.label9.TabIndex = 20;
            this.label9.Text = "GOODS";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SkyBlue;
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(161, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(629, 550);
            this.panel1.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(19, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(443, 19);
            this.label16.TabIndex = 0;
            this.label16.Text = "GOODS RECEIPT NOTE AND GOODS RETURNED TO VENDOR";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.PowderBlue;
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Location = new System.Drawing.Point(23, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(593, 491);
            this.panel2.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(13, 11);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(577, 477);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(569, 451);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "ADD";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.PowderBlue;
            this.panel5.Controls.Add(grvnoLabel);
            this.panel5.Controls.Add(this.grvnoTextBox);
            this.panel5.Controls.Add(prnoLabel1);
            this.panel5.Controls.Add(rfqnoLabel);
            this.panel5.Controls.Add(this.rfqnoTextBox);
            this.panel5.Controls.Add(prnoLabel);
            this.panel5.Controls.Add(this.prnoTextBox);
            this.panel5.Controls.Add(this.prnoComboBox);
            this.panel5.Controls.Add(po_noLabel1);
            this.panel5.Controls.Add(this.po_noTextBox1);
            this.panel5.Controls.Add(grnoLabel1);
            this.panel5.Controls.Add(this.grnoTextBox1);
            this.panel5.Controls.Add(itemcodeLabel1);
            this.panel5.Controls.Add(this.itemcodeTextBox1);
            this.panel5.Controls.Add(qtyrjctdLabel);
            this.panel5.Controls.Add(this.qtyrjctdTextBox);
            this.panel5.Controls.Add(reasonLabel);
            this.panel5.Controls.Add(this.reasonTextBox);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.button3);
            this.panel5.Enabled = false;
            this.panel5.Location = new System.Drawing.Point(291, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(275, 439);
            this.panel5.TabIndex = 22;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // grvnoTextBox
            // 
            this.grvnoTextBox.Location = new System.Drawing.Point(121, 70);
            this.grvnoTextBox.Name = "grvnoTextBox";
            this.grvnoTextBox.ReadOnly = true;
            this.grvnoTextBox.Size = new System.Drawing.Size(130, 20);
            this.grvnoTextBox.TabIndex = 34;
            // 
            // rfqnoTextBox
            // 
            this.rfqnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "rfqno", true));
            this.rfqnoTextBox.Location = new System.Drawing.Point(60, 385);
            this.rfqnoTextBox.Name = "rfqnoTextBox";
            this.rfqnoTextBox.Size = new System.Drawing.Size(100, 20);
            this.rfqnoTextBox.TabIndex = 46;
            this.rfqnoTextBox.TextChanged += new System.EventHandler(this.rfqnoTextBox_TextChanged);
            // 
            // quotationMasterBindingSource
            // 
            this.quotationMasterBindingSource.DataMember = "QuotationMaster";
            this.quotationMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // prnoTextBox
            // 
            this.prnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rFQMasterBindingSource, "prno", true));
            this.prnoTextBox.Location = new System.Drawing.Point(94, 321);
            this.prnoTextBox.Name = "prnoTextBox";
            this.prnoTextBox.Size = new System.Drawing.Size(100, 20);
            this.prnoTextBox.TabIndex = 47;
            this.prnoTextBox.TextChanged += new System.EventHandler(this.prnoTextBox_TextChanged);
            // 
            // prnoComboBox
            // 
            this.prnoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "prno", true));
            this.prnoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pRMasterBindingSource, "prno", true));
            this.prnoComboBox.DataSource = this.pRMasterBindingSource;
            this.prnoComboBox.DisplayMember = "prno";
            this.prnoComboBox.FormattingEnabled = true;
            this.prnoComboBox.Location = new System.Drawing.Point(60, 410);
            this.prnoComboBox.Name = "prnoComboBox";
            this.prnoComboBox.Size = new System.Drawing.Size(121, 21);
            this.prnoComboBox.TabIndex = 49;
            this.prnoComboBox.ValueMember = "prno";
            // 
            // pRMasterBindingSource
            // 
            this.pRMasterBindingSource.DataMember = "PRMaster";
            this.pRMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // po_noTextBox1
            // 
            this.po_noTextBox1.Location = new System.Drawing.Point(122, 129);
            this.po_noTextBox1.Name = "po_noTextBox1";
            this.po_noTextBox1.ReadOnly = true;
            this.po_noTextBox1.Size = new System.Drawing.Size(130, 20);
            this.po_noTextBox1.TabIndex = 25;
            // 
            // grnoTextBox1
            // 
            this.grnoTextBox1.Location = new System.Drawing.Point(122, 155);
            this.grnoTextBox1.Name = "grnoTextBox1";
            this.grnoTextBox1.ReadOnly = true;
            this.grnoTextBox1.Size = new System.Drawing.Size(130, 20);
            this.grnoTextBox1.TabIndex = 27;
            // 
            // itemcodeTextBox1
            // 
            this.itemcodeTextBox1.Location = new System.Drawing.Point(122, 181);
            this.itemcodeTextBox1.Name = "itemcodeTextBox1";
            this.itemcodeTextBox1.ReadOnly = true;
            this.itemcodeTextBox1.Size = new System.Drawing.Size(130, 20);
            this.itemcodeTextBox1.TabIndex = 29;
            // 
            // qtyrjctdTextBox
            // 
            this.qtyrjctdTextBox.Location = new System.Drawing.Point(122, 207);
            this.qtyrjctdTextBox.Name = "qtyrjctdTextBox";
            this.qtyrjctdTextBox.ReadOnly = true;
            this.qtyrjctdTextBox.Size = new System.Drawing.Size(130, 20);
            this.qtyrjctdTextBox.TabIndex = 31;
            // 
            // reasonTextBox
            // 
            this.reasonTextBox.Location = new System.Drawing.Point(122, 234);
            this.reasonTextBox.Name = "reasonTextBox";
            this.reasonTextBox.Size = new System.Drawing.Size(130, 20);
            this.reasonTextBox.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 19);
            this.label1.TabIndex = 22;
            this.label1.Text = "GOODS RETURNED TO VENDOR";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.PowderBlue;
            this.panel3.Controls.Add(this.po_noComboBox1);
            this.panel3.Controls.Add(rfqnoLabel1);
            this.panel3.Controls.Add(this.rfqnoComboBox);
            this.panel3.Controls.Add(this.quantityTextBox);
            this.panel3.Controls.Add(this.rateTextBox);
            this.panel3.Controls.Add(quotationnoLabel1);
            this.panel3.Controls.Add(this.quotationnoComboBox);
            this.panel3.Controls.Add(quotationnoLabel);
            this.panel3.Controls.Add(this.quotationnoTextBox);
            this.panel3.Controls.Add(this.amountTextBox);
            this.panel3.Controls.Add(this.itemcodeTextBox);
            this.panel3.Controls.Add(this.po_noComboBox);
            this.panel3.Controls.Add(grnoLabel);
            this.panel3.Controls.Add(this.grnoTextBox);
            this.panel3.Controls.Add(grdateLabel);
            this.panel3.Controls.Add(this.grdateDateTimePicker);
            this.panel3.Controls.Add(po_noLabel);
            this.panel3.Controls.Add(itemcodeLabel);
            this.panel3.Controls.Add(qtyrcvdLabel);
            this.panel3.Controls.Add(qtyaccptdLabel);
            this.panel3.Controls.Add(this.qtyaccptdTextBox);
            this.panel3.Controls.Add(rateLabel);
            this.panel3.Controls.Add(this.button7);
            this.panel3.Controls.Add(valueLabel);
            this.panel3.Controls.Add(statusLabel);
            this.panel3.Controls.Add(this.statusTextBox);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Location = new System.Drawing.Point(6, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(279, 439);
            this.panel3.TabIndex = 21;
            // 
            // po_noComboBox1
            // 
            this.po_noComboBox1.FormattingEnabled = true;
            this.po_noComboBox1.Location = new System.Drawing.Point(128, 122);
            this.po_noComboBox1.Name = "po_noComboBox1";
            this.po_noComboBox1.Size = new System.Drawing.Size(130, 21);
            this.po_noComboBox1.TabIndex = 51;
            this.po_noComboBox1.SelectedIndexChanged += new System.EventHandler(this.po_noComboBox1_SelectedIndexChanged);
            // 
            // rfqnoComboBox
            // 
            this.rfqnoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rFQMasterBindingSource, "rfqno", true));
            this.rfqnoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pRMasterBindingSource, "prno", true));
            this.rfqnoComboBox.DataSource = this.rFQMasterBindingSource;
            this.rfqnoComboBox.DisplayMember = "rfqno";
            this.rfqnoComboBox.FormattingEnabled = true;
            this.rfqnoComboBox.Location = new System.Drawing.Point(80, 413);
            this.rfqnoComboBox.Name = "rfqnoComboBox";
            this.rfqnoComboBox.Size = new System.Drawing.Size(121, 21);
            this.rfqnoComboBox.TabIndex = 50;
            this.rfqnoComboBox.ValueMember = "prno";
            // 
            // quantityTextBox
            // 
            this.quantityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "quantity", true));
            this.quantityTextBox.Location = new System.Drawing.Point(128, 231);
            this.quantityTextBox.Name = "quantityTextBox";
            this.quantityTextBox.ReadOnly = true;
            this.quantityTextBox.Size = new System.Drawing.Size(130, 20);
            this.quantityTextBox.TabIndex = 48;
            // 
            // rateTextBox
            // 
            this.rateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "rate", true));
            this.rateTextBox.Location = new System.Drawing.Point(128, 175);
            this.rateTextBox.Name = "rateTextBox";
            this.rateTextBox.ReadOnly = true;
            this.rateTextBox.Size = new System.Drawing.Size(130, 20);
            this.rateTextBox.TabIndex = 44;
            // 
            // quotationnoComboBox
            // 
            this.quotationnoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "quotationno", true));
            this.quotationnoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pOMasterBindingSource, "quotationno", true));
            this.quotationnoComboBox.DataSource = this.quotationMasterBindingSource;
            this.quotationnoComboBox.DisplayMember = "quotationno";
            this.quotationnoComboBox.FormattingEnabled = true;
            this.quotationnoComboBox.Location = new System.Drawing.Point(80, 388);
            this.quotationnoComboBox.Name = "quotationnoComboBox";
            this.quotationnoComboBox.Size = new System.Drawing.Size(121, 21);
            this.quotationnoComboBox.TabIndex = 43;
            this.quotationnoComboBox.ValueMember = "quotationno";
            this.quotationnoComboBox.SelectedIndexChanged += new System.EventHandler(this.quotationnoComboBox_SelectedIndexChanged);
            // 
            // pOMasterBindingSource
            // 
            this.pOMasterBindingSource.DataMember = "POMaster";
            this.pOMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // quotationnoTextBox
            // 
            this.quotationnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "quotationno", true));
            this.quotationnoTextBox.Location = new System.Drawing.Point(109, 324);
            this.quotationnoTextBox.Name = "quotationnoTextBox";
            this.quotationnoTextBox.Size = new System.Drawing.Size(100, 20);
            this.quotationnoTextBox.TabIndex = 42;
            this.quotationnoTextBox.TextChanged += new System.EventHandler(this.quotationnoTextBox_TextChanged);
            // 
            // amountTextBox
            // 
            this.amountTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "amount", true));
            this.amountTextBox.Location = new System.Drawing.Point(128, 205);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.ReadOnly = true;
            this.amountTextBox.Size = new System.Drawing.Size(130, 20);
            this.amountTextBox.TabIndex = 41;
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(128, 149);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.ReadOnly = true;
            this.itemcodeTextBox.Size = new System.Drawing.Size(130, 20);
            this.itemcodeTextBox.TabIndex = 40;
            // 
            // po_noComboBox
            // 
            this.po_noComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "po_no", true));
            this.po_noComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.gRMasterBindingSource, "po_no", true));
            this.po_noComboBox.DataSource = this.pOMasterBindingSource;
            this.po_noComboBox.DisplayMember = "po_no";
            this.po_noComboBox.FormattingEnabled = true;
            this.po_noComboBox.Location = new System.Drawing.Point(128, 122);
            this.po_noComboBox.Name = "po_noComboBox";
            this.po_noComboBox.Size = new System.Drawing.Size(130, 21);
            this.po_noComboBox.TabIndex = 39;
            this.po_noComboBox.ValueMember = "po_no";
            this.po_noComboBox.SelectedIndexChanged += new System.EventHandler(this.po_noComboBox_SelectedIndexChanged);
            // 
            // gRMasterBindingSource
            // 
            this.gRMasterBindingSource.DataMember = "GRMaster";
            this.gRMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // grnoTextBox
            // 
            this.grnoTextBox.Location = new System.Drawing.Point(128, 70);
            this.grnoTextBox.Name = "grnoTextBox";
            this.grnoTextBox.ReadOnly = true;
            this.grnoTextBox.Size = new System.Drawing.Size(130, 20);
            this.grnoTextBox.TabIndex = 22;
            // 
            // grdateDateTimePicker
            // 
            this.grdateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.grdateDateTimePicker.Location = new System.Drawing.Point(128, 94);
            this.grdateDateTimePicker.MinDate = new System.DateTime(2009, 7, 1, 0, 0, 0, 0);
            this.grdateDateTimePicker.Name = "grdateDateTimePicker";
            this.grdateDateTimePicker.Size = new System.Drawing.Size(130, 20);
            this.grdateDateTimePicker.TabIndex = 24;
            // 
            // qtyaccptdTextBox
            // 
            this.qtyaccptdTextBox.Location = new System.Drawing.Point(128, 257);
            this.qtyaccptdTextBox.Name = "qtyaccptdTextBox";
            this.qtyaccptdTextBox.Size = new System.Drawing.Size(130, 20);
            this.qtyaccptdTextBox.TabIndex = 32;
            this.qtyaccptdTextBox.TextChanged += new System.EventHandler(this.qtyaccptdTextBox_TextChanged);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(98, 357);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(143, 24);
            this.button7.TabIndex = 18;
            this.button7.Text = "ADD TO GR MASTER";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button4_Click);
            // 
            // statusTextBox
            // 
            this.statusTextBox.Location = new System.Drawing.Point(128, 286);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.Size = new System.Drawing.Size(130, 20);
            this.statusTextBox.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(13, 12);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(152, 17);
            this.label17.TabIndex = 21;
            this.label17.Text = "GOODS RECEIPT NOTE";
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.gRVMasterDataGridView);
            this.tabPage2.Controls.Add(this.gRMasterDataGridView);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(569, 451);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "VIEW";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gRVMasterDataGridView
            // 
            this.gRVMasterDataGridView.AllowUserToAddRows = false;
            this.gRVMasterDataGridView.AutoGenerateColumns = false;
            this.gRVMasterDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gRVMasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15});
            this.gRVMasterDataGridView.DataSource = this.gRVMasterBindingSource;
            this.gRVMasterDataGridView.Location = new System.Drawing.Point(6, 125);
            this.gRVMasterDataGridView.Name = "gRVMasterDataGridView";
            this.gRVMasterDataGridView.Size = new System.Drawing.Size(557, 310);
            this.gRVMasterDataGridView.TabIndex = 20;
            this.gRVMasterDataGridView.Visible = false;
            this.gRVMasterDataGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gRVMasterDataGridView_RowHeaderMouseClick);
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "grvno";
            this.dataGridViewTextBoxColumn10.HeaderText = "GRV No";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "po_no";
            this.dataGridViewTextBoxColumn11.HeaderText = "PO No";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "grno";
            this.dataGridViewTextBoxColumn12.HeaderText = "GR No";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "itemcode";
            this.dataGridViewTextBoxColumn13.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "qtyrjctd";
            this.dataGridViewTextBoxColumn14.HeaderText = "Quantity Rejected";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "reason";
            this.dataGridViewTextBoxColumn15.HeaderText = "Reason";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // gRVMasterBindingSource
            // 
            this.gRVMasterBindingSource.DataMember = "GRVMaster";
            this.gRVMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // gRMasterDataGridView
            // 
            this.gRMasterDataGridView.AllowUserToAddRows = false;
            this.gRMasterDataGridView.AutoGenerateColumns = false;
            this.gRMasterDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gRMasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.gRMasterDataGridView.DataSource = this.gRMasterBindingSource;
            this.gRMasterDataGridView.Location = new System.Drawing.Point(3, 125);
            this.gRMasterDataGridView.Name = "gRMasterDataGridView";
            this.gRMasterDataGridView.Size = new System.Drawing.Size(560, 310);
            this.gRMasterDataGridView.TabIndex = 20;
            this.gRMasterDataGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gRMasterDataGridView_RowHeaderMouseClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "grno";
            this.dataGridViewTextBoxColumn1.HeaderText = "GR No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "grdate";
            this.dataGridViewTextBoxColumn2.HeaderText = "GR Date";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "po_no";
            this.dataGridViewTextBoxColumn3.HeaderText = "PO No";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "itemcode";
            this.dataGridViewTextBoxColumn4.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "qtyrcvd";
            this.dataGridViewTextBoxColumn5.HeaderText = "Quantity Recieved";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "qtyaccptd";
            this.dataGridViewTextBoxColumn6.HeaderText = "Quantity Accepted";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "rate";
            this.dataGridViewTextBoxColumn7.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "value";
            this.dataGridViewTextBoxColumn8.HeaderText = "Value";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "status";
            this.dataGridViewTextBoxColumn9.HeaderText = "Status";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.textBox20);
            this.groupBox4.Controls.Add(this.radioButton6);
            this.groupBox4.Controls.Add(this.radioButton7);
            this.groupBox4.Controls.Add(this.textBox21);
            this.groupBox4.Location = new System.Drawing.Point(281, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(282, 117);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Search for GRV by";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(201, 69);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 21;
            this.button5.Text = "SEARCH";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBox20
            // 
            this.textBox20.Enabled = false;
            this.textBox20.Location = new System.Drawing.Point(91, 70);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(100, 20);
            this.textBox20.TabIndex = 16;
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(17, 71);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(68, 17);
            this.radioButton6.TabIndex = 8;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "GRV No.";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Checked = true;
            this.radioButton7.Location = new System.Drawing.Point(17, 34);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(45, 17);
            this.radioButton7.TabIndex = 6;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Item";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton7_CheckedChanged);
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(91, 33);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(100, 20);
            this.textBox21.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox2.Controls.Add(this.textBox11);
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton5);
            this.groupBox2.Controls.Add(this.textBox12);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(269, 117);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Search for GRN by";
            // 
            // textBox11
            // 
            this.textBox11.Enabled = false;
            this.textBox11.Location = new System.Drawing.Point(81, 71);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 16;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(187, 70);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 17;
            this.button6.Text = "SEARCH";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.fillByItemcodeGRNToolStripButton_Click);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(17, 71);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(61, 17);
            this.radioButton2.TabIndex = 8;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "GR No.";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged_1);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Checked = true;
            this.radioButton5.Location = new System.Drawing.Point(17, 34);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(45, 17);
            this.radioButton5.TabIndex = 6;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Item";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged_1);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(81, 33);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 1;
            this.textBox12.TextChanged += new System.EventHandler(this.textBox12_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkLabel12);
            this.groupBox1.Controls.Add(this.linkLabel11);
            this.groupBox1.Controls.Add(this.linkLabel3);
            this.groupBox1.Controls.Add(this.linkLabel2);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Location = new System.Drawing.Point(12, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(143, 168);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PURCHASE";
            // 
            // linkLabel12
            // 
            this.linkLabel12.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel12.AutoSize = true;
            this.linkLabel12.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel12.LinkColor = System.Drawing.Color.White;
            this.linkLabel12.Location = new System.Drawing.Point(12, 134);
            this.linkLabel12.Name = "linkLabel12";
            this.linkLabel12.Size = new System.Drawing.Size(79, 17);
            this.linkLabel12.TabIndex = 32;
            this.linkLabel12.TabStop = true;
            this.linkLabel12.Text = "Cost Center";
            this.linkLabel12.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel12_LinkClicked);
            // 
            // linkLabel11
            // 
            this.linkLabel11.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel11.AutoSize = true;
            this.linkLabel11.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel11.LinkColor = System.Drawing.Color.White;
            this.linkLabel11.Location = new System.Drawing.Point(12, 105);
            this.linkLabel11.Name = "linkLabel11";
            this.linkLabel11.Size = new System.Drawing.Size(52, 17);
            this.linkLabel11.TabIndex = 31;
            this.linkLabel11.TabStop = true;
            this.linkLabel11.Text = "Budget";
            this.linkLabel11.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel11_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel3.LinkColor = System.Drawing.Color.White;
            this.linkLabel3.Location = new System.Drawing.Point(11, 82);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(106, 17);
            this.linkLabel3.TabIndex = 23;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Purchase Order";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.LinkColor = System.Drawing.Color.White;
            this.linkLabel2.Location = new System.Drawing.Point(11, 54);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(77, 17);
            this.linkLabel2.TabIndex = 22;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Quotations";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(11, 25);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(119, 17);
            this.linkLabel1.TabIndex = 21;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Purchase Request";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.linkLabel8);
            this.groupBox3.Controls.Add(this.linkLabel7);
            this.groupBox3.Controls.Add(this.linkLabel6);
            this.groupBox3.Controls.Add(this.linkLabel5);
            this.groupBox3.Location = new System.Drawing.Point(12, 211);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(143, 155);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "INVENTORY";
            // 
            // linkLabel8
            // 
            this.linkLabel8.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel8.AutoSize = true;
            this.linkLabel8.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel8.LinkColor = System.Drawing.Color.White;
            this.linkLabel8.Location = new System.Drawing.Point(12, 108);
            this.linkLabel8.Name = "linkLabel8";
            this.linkLabel8.Size = new System.Drawing.Size(43, 17);
            this.linkLabel8.TabIndex = 27;
            this.linkLabel8.TabStop = true;
            this.linkLabel8.Text = "Stock";
            this.linkLabel8.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
            // 
            // linkLabel7
            // 
            this.linkLabel7.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel7.AutoSize = true;
            this.linkLabel7.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel7.LinkColor = System.Drawing.Color.White;
            this.linkLabel7.Location = new System.Drawing.Point(12, 57);
            this.linkLabel7.Name = "linkLabel7";
            this.linkLabel7.Size = new System.Drawing.Size(110, 17);
            this.linkLabel7.TabIndex = 26;
            this.linkLabel7.TabStop = true;
            this.linkLabel7.Text = "Material Receipt";
            this.linkLabel7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel7_LinkClicked);
            // 
            // linkLabel6
            // 
            this.linkLabel6.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel6.LinkColor = System.Drawing.Color.White;
            this.linkLabel6.Location = new System.Drawing.Point(11, 82);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(40, 17);
            this.linkLabel6.TabIndex = 26;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "Issue";
            this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel6_LinkClicked);
            // 
            // linkLabel5
            // 
            this.linkLabel5.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel5.LinkColor = System.Drawing.Color.White;
            this.linkLabel5.Location = new System.Drawing.Point(11, 27);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(113, 17);
            this.linkLabel5.TabIndex = 25;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "Material Request";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.linkLabel13);
            this.groupBox6.Controls.Add(this.linkLabel14);
            this.groupBox6.Location = new System.Drawing.Point(12, 372);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(143, 117);
            this.groupBox6.TabIndex = 23;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "GOODS";
            // 
            // linkLabel13
            // 
            this.linkLabel13.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel13.AutoSize = true;
            this.linkLabel13.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel13.LinkColor = System.Drawing.Color.White;
            this.linkLabel13.Location = new System.Drawing.Point(11, 65);
            this.linkLabel13.Name = "linkLabel13";
            this.linkLabel13.Size = new System.Drawing.Size(69, 17);
            this.linkLabel13.TabIndex = 30;
            this.linkLabel13.TabStop = true;
            this.linkLabel13.Text = "GRV & GRN";
            this.linkLabel13.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel13_LinkClicked);
            // 
            // linkLabel14
            // 
            this.linkLabel14.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel14.AutoSize = true;
            this.linkLabel14.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel14.LinkColor = System.Drawing.Color.White;
            this.linkLabel14.Location = new System.Drawing.Point(12, 35);
            this.linkLabel14.Name = "linkLabel14";
            this.linkLabel14.Size = new System.Drawing.Size(37, 17);
            this.linkLabel14.TabIndex = 29;
            this.linkLabel14.TabStop = true;
            this.linkLabel14.Text = "Item";
            this.linkLabel14.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel14_LinkClicked);
            // 
            // gRMasterTableAdapter
            // 
            this.gRMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = this.gRMasterTableAdapter;
            this.tableAdapterManager.GRVMasterTableAdapter = this.gRVMasterTableAdapter;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = this.pOMasterTableAdapter;
            this.tableAdapterManager.PRMasterTableAdapter = this.pRMasterTableAdapter;
            this.tableAdapterManager.QuotationMasterTableAdapter = this.quotationMasterTableAdapter;
            this.tableAdapterManager.RFQMasterTableAdapter = this.rFQMasterTableAdapter;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // gRVMasterTableAdapter
            // 
            this.gRVMasterTableAdapter.ClearBeforeFill = true;
            // 
            // pOMasterTableAdapter
            // 
            this.pOMasterTableAdapter.ClearBeforeFill = true;
            // 
            // pRMasterTableAdapter
            // 
            this.pRMasterTableAdapter.ClearBeforeFill = true;
            // 
            // quotationMasterTableAdapter
            // 
            this.quotationMasterTableAdapter.ClearBeforeFill = true;
            // 
            // rFQMasterTableAdapter
            // 
            this.rFQMasterTableAdapter.ClearBeforeFill = true;
            // 
            // GOODS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(791, 606);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblUsercode);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.MaximizeBox = false;
            this.Name = "GOODS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GOODS";
            this.Load += new System.EventHandler(this.GRN_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rFQMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quotationMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRMasterBindingSource)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pOMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRMasterBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gRVMasterDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRVMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRMasterDataGridView)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        public System.Windows.Forms.Label lblUsercode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel8;
        private System.Windows.Forms.LinkLabel linkLabel7;
        private System.Windows.Forms.LinkLabel linkLabel6;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.LinkLabel linkLabel12;
        private System.Windows.Forms.LinkLabel linkLabel11;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.LinkLabel linkLabel13;
        private System.Windows.Forms.LinkLabel linkLabel14;
        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource gRMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.GRMasterTableAdapter gRMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox grnoTextBox;
        private System.Windows.Forms.DateTimePicker grdateDateTimePicker;
        private System.Windows.Forms.TextBox qtyaccptdTextBox;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button7;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.GRVMasterTableAdapter gRVMasterTableAdapter;
        private System.Windows.Forms.BindingSource gRVMasterBindingSource;
        private System.Windows.Forms.TextBox po_noTextBox1;
        private System.Windows.Forms.TextBox grnoTextBox1;
        private System.Windows.Forms.TextBox itemcodeTextBox1;
        private System.Windows.Forms.TextBox qtyrjctdTextBox;
        private System.Windows.Forms.TextBox reasonTextBox;
        private System.Windows.Forms.TextBox grvnoTextBox;
        private System.Windows.Forms.DataGridView gRMasterDataGridView;
        private System.Windows.Forms.DataGridView gRVMasterDataGridView;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.POMasterTableAdapter pOMasterTableAdapter;
        private System.Windows.Forms.BindingSource pOMasterBindingSource;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private System.Windows.Forms.ComboBox po_noComboBox;
        private System.Windows.Forms.TextBox quotationnoTextBox;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.QuotationMasterTableAdapter quotationMasterTableAdapter;
        private System.Windows.Forms.BindingSource quotationMasterBindingSource;
        private System.Windows.Forms.TextBox rateTextBox;
        private System.Windows.Forms.ComboBox quotationnoComboBox;
        private System.Windows.Forms.TextBox rfqnoTextBox;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.RFQMasterTableAdapter rFQMasterTableAdapter;
        private System.Windows.Forms.BindingSource rFQMasterBindingSource;
        private System.Windows.Forms.TextBox prnoTextBox;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.PRMasterTableAdapter pRMasterTableAdapter;
        private System.Windows.Forms.BindingSource pRMasterBindingSource;
        private System.Windows.Forms.TextBox quantityTextBox;
        private System.Windows.Forms.ComboBox rfqnoComboBox;
        private System.Windows.Forms.ComboBox prnoComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.ComboBox po_noComboBox1;
    }
}