﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class EditQuotation : Form
    {
        public EditQuotation(RELERPDBDataSet reds ,int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.quotationMasterBindingSource.DataSource = this.rELERPDBDataSet;
            this.quotationMasterBindingSource.Position = position;

            //this.quotationMasterBindingSource.Position=

        }

        private void quotationMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.quotationMasterBindingSource.EndEdit();
                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("Quotation details updated.", "ERP-PIMS");
                }
                else
                {
                    MessageBox.Show("Quotation details not updated.", "ERP-PIMS");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.quotationMasterBindingSource.RemoveCurrent();
                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("Quotation details deleted.", "ERP-PIMS");
                }
                else
                {
                    MessageBox.Show("Quotation details not deleted.", "ERP-PIMS");
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error occured" + exp.ToString(), "ERP-PIMS");
            }
            this.Close();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
