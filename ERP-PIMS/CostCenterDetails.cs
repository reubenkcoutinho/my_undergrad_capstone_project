﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class CostCenterDetails : Form
    {
        public CostCenterDetails(RELERPDBDataSet reds, int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.costCenterMasterBindingSource.DataSource = this.rELERPDBDataSet;
            this.costCenterMasterBindingSource.Position = position;

         }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                this.Validate();
                this.costCenterMasterBindingSource.EndEdit();

                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("Cost Center details updated.", "ERP-PIMS");

                }
                else
                {
                    MessageBox.Show("Cost Center details not updated.", "ERP-PIMS");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }
        }

        private void CostCenterDetails_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            this.Validate();
            this.costCenterMasterBindingSource.RemoveCurrent();
            if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
            {
                MessageBox.Show("Cost Center details deleted.", "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("Cost Center details not deleted.", "ERP-PIMS");
            }
        }
    }
}
