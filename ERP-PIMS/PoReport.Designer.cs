﻿namespace ERP_PIMS
{
    partial class PoReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.RELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.POMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.POMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.POMasterTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.RELERPDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.POMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "RELERPDBDataSet_POMaster";
            reportDataSource1.Value = this.POMasterBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "ERP_PIMS.POreport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(12, 71);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(510, 358);
            this.reportViewer1.TabIndex = 0;
            // 
            // RELERPDBDataSet
            // 
            this.RELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.RELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // POMasterBindingSource
            // 
            this.POMasterBindingSource.DataMember = "POMaster";
            this.POMasterBindingSource.DataSource = this.RELERPDBDataSet;
            // 
            // POMasterTableAdapter
            // 
            this.POMasterTableAdapter.ClearBeforeFill = true;
            // 
            // PoReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 441);
            this.Controls.Add(this.reportViewer1);
            this.Name = "PoReport";
            this.Text = "PoReport";
            this.Load += new System.EventHandler(this.PoReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RELERPDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.POMasterBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource POMasterBindingSource;
        private RELERPDBDataSet RELERPDBDataSet;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.POMasterTableAdapter POMasterTableAdapter;
    }
}