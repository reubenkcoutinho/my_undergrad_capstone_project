﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class MRecp : Form

    {
        string usrDetails, usrName, usrCode;
        string msg;
        int val;
        string query = "select * from MRMaster";
        string table = "MRMaster";
        DbOperations dbo = new DbOperations();
        DataSet mRDataset = new DataSet();
        
        
        public MRecp()
        {
            InitializeComponent();
        }

        private void mRMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.mRMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void MRecp_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.MRMaster' table. You can move, or remove it, as needed.
            this.mRMasterTableAdapter.Fill(this.rELERPDBDataSet.MRMaster);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        

        private void fillByToolStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.mRMasterTableAdapter.FillBy(this.rELERPDBDataSet.MRMaster, ((int)(System.Convert.ChangeType(textBox1.Text, typeof(int)))));
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            quotations abc = new quotations();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PO abc = new PO();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();

        }

        private void linkLabel12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();

        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();

        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Stock stk = new Stock();
            stk.MdiParent = this.MdiParent;
            stk.Show();
            this.Hide();
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

      
        }
    }

