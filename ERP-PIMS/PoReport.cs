﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class PoReport : Form
    {
        public PoReport()
        {
            InitializeComponent();
        }

        private void PoReport_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'RELERPDBDataSet.POMaster' table. You can move, or remove it, as needed.
            this.POMasterTableAdapter.Fill(this.RELERPDBDataSet.POMaster);

            this.reportViewer1.RefreshReport();
        }
    }
}
