﻿namespace ERP_PIMS
{
    partial class quotations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label rateLabel;
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.lblUsercode = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.linkLabel17 = new System.Windows.Forms.LinkLabel();
            this.linkLabel18 = new System.Windows.Forms.LinkLabel();
            this.linkLabel16 = new System.Windows.Forms.LinkLabel();
            this.label25 = new System.Windows.Forms.Label();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel20 = new System.Windows.Forms.LinkLabel();
            this.linkLabel21 = new System.Windows.Forms.LinkLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.linkLabel19 = new System.Windows.Forms.LinkLabel();
            this.linkLabel22 = new System.Windows.Forms.LinkLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.linkLabel9 = new System.Windows.Forms.LinkLabel();
            this.linkLabel10 = new System.Windows.Forms.LinkLabel();
            this.linkLabel15 = new System.Windows.Forms.LinkLabel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.quotationMasterDataGridView = new System.Windows.Forms.DataGridView();
            this.quotationMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rateTextBox = new System.Windows.Forms.TextBox();
            this.pRMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.prnoComboBox = new System.Windows.Forms.ComboBox();
            this.prnoTextBox = new System.Windows.Forms.TextBox();
            this.rFQMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.quotationMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.QuotationMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.pRMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.PRMasterTableAdapter();
            this.rFQMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.RFQMasterTableAdapter();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            rateLabel = new System.Windows.Forms.Label();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quotationMasterDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quotationMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pRMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rFQMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rateLabel
            // 
            rateLabel.AutoSize = true;
            rateLabel.Location = new System.Drawing.Point(412, 78);
            rateLabel.Name = "rateLabel";
            rateLabel.Size = new System.Drawing.Size(30, 13);
            rateLabel.TabIndex = 72;
            rateLabel.Text = "Rate";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(17, 17);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(45, 17);
            this.radioButton1.TabIndex = 6;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Item";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(17, 52);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(47, 17);
            this.radioButton2.TabIndex = 7;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "User";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(125, 16);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 1;
            // 
            // textBox9
            // 
            this.textBox9.Enabled = false;
            this.textBox9.Location = new System.Drawing.Point(125, 51);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 4;
            // 
            // lblUsercode
            // 
            this.lblUsercode.AutoSize = true;
            this.lblUsercode.Location = new System.Drawing.Point(13, 532);
            this.lblUsercode.Name = "lblUsercode";
            this.lblUsercode.Size = new System.Drawing.Size(0, 13);
            this.lblUsercode.TabIndex = 18;
            this.lblUsercode.Visible = false;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.linkLabel17);
            this.groupBox6.Controls.Add(this.linkLabel18);
            this.groupBox6.Location = new System.Drawing.Point(16, 385);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(143, 117);
            this.groupBox6.TabIndex = 79;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "GOODS";
            // 
            // linkLabel17
            // 
            this.linkLabel17.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel17.AutoSize = true;
            this.linkLabel17.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel17.LinkColor = System.Drawing.Color.White;
            this.linkLabel17.Location = new System.Drawing.Point(11, 65);
            this.linkLabel17.Name = "linkLabel17";
            this.linkLabel17.Size = new System.Drawing.Size(68, 17);
            this.linkLabel17.TabIndex = 30;
            this.linkLabel17.TabStop = true;
            this.linkLabel17.Text = "GRV & GRN";
            this.linkLabel17.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel17_LinkClicked);
            // 
            // linkLabel18
            // 
            this.linkLabel18.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel18.AutoSize = true;
            this.linkLabel18.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel18.LinkColor = System.Drawing.Color.White;
            this.linkLabel18.Location = new System.Drawing.Point(12, 35);
            this.linkLabel18.Name = "linkLabel18";
            this.linkLabel18.Size = new System.Drawing.Size(37, 17);
            this.linkLabel18.TabIndex = 29;
            this.linkLabel18.TabStop = true;
            this.linkLabel18.Text = "Item";
            this.linkLabel18.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel18_LinkClicked);
            // 
            // linkLabel16
            // 
            this.linkLabel16.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel16.AutoSize = true;
            this.linkLabel16.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel16.LinkColor = System.Drawing.Color.White;
            this.linkLabel16.Location = new System.Drawing.Point(12, 105);
            this.linkLabel16.Name = "linkLabel16";
            this.linkLabel16.Size = new System.Drawing.Size(52, 17);
            this.linkLabel16.TabIndex = 31;
            this.linkLabel16.TabStop = true;
            this.linkLabel16.Text = "Budget";
            this.linkLabel16.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel16_LinkClicked);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(19, 11);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(162, 19);
            this.label25.TabIndex = 0;
            this.label25.Text = "QUOTATION DETAILS";
            // 
            // linkLabel4
            // 
            this.linkLabel4.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel4.LinkColor = System.Drawing.Color.White;
            this.linkLabel4.Location = new System.Drawing.Point(12, 108);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(43, 17);
            this.linkLabel4.TabIndex = 27;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Stock";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // linkLabel20
            // 
            this.linkLabel20.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel20.AutoSize = true;
            this.linkLabel20.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel20.LinkColor = System.Drawing.Color.White;
            this.linkLabel20.Location = new System.Drawing.Point(11, 82);
            this.linkLabel20.Name = "linkLabel20";
            this.linkLabel20.Size = new System.Drawing.Size(106, 17);
            this.linkLabel20.TabIndex = 23;
            this.linkLabel20.TabStop = true;
            this.linkLabel20.Text = "Purchase Order";
            this.linkLabel20.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel20_LinkClicked);
            // 
            // linkLabel21
            // 
            this.linkLabel21.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel21.AutoSize = true;
            this.linkLabel21.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel21.LinkColor = System.Drawing.Color.White;
            this.linkLabel21.Location = new System.Drawing.Point(11, 54);
            this.linkLabel21.Name = "linkLabel21";
            this.linkLabel21.Size = new System.Drawing.Size(77, 17);
            this.linkLabel21.TabIndex = 22;
            this.linkLabel21.TabStop = true;
            this.linkLabel21.Text = "Quotations";
            this.linkLabel21.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel21_LinkClicked);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.linkLabel19);
            this.groupBox4.Controls.Add(this.linkLabel16);
            this.groupBox4.Controls.Add(this.linkLabel20);
            this.groupBox4.Controls.Add(this.linkLabel21);
            this.groupBox4.Controls.Add(this.linkLabel22);
            this.groupBox4.Location = new System.Drawing.Point(16, 39);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(143, 168);
            this.groupBox4.TabIndex = 77;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "PURCHASE";
            // 
            // linkLabel19
            // 
            this.linkLabel19.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel19.AutoSize = true;
            this.linkLabel19.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel19.LinkColor = System.Drawing.Color.White;
            this.linkLabel19.Location = new System.Drawing.Point(12, 134);
            this.linkLabel19.Name = "linkLabel19";
            this.linkLabel19.Size = new System.Drawing.Size(79, 17);
            this.linkLabel19.TabIndex = 32;
            this.linkLabel19.TabStop = true;
            this.linkLabel19.Text = "Cost Center";
            this.linkLabel19.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel19_LinkClicked);
            // 
            // linkLabel22
            // 
            this.linkLabel22.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel22.AutoSize = true;
            this.linkLabel22.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel22.LinkColor = System.Drawing.Color.White;
            this.linkLabel22.Location = new System.Drawing.Point(11, 25);
            this.linkLabel22.Name = "linkLabel22";
            this.linkLabel22.Size = new System.Drawing.Size(119, 17);
            this.linkLabel22.TabIndex = 21;
            this.linkLabel22.TabStop = true;
            this.linkLabel22.Text = "Purchase Request";
            this.linkLabel22.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel22_LinkClicked);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.linkLabel4);
            this.groupBox3.Controls.Add(this.linkLabel9);
            this.groupBox3.Controls.Add(this.linkLabel10);
            this.groupBox3.Controls.Add(this.linkLabel15);
            this.groupBox3.Location = new System.Drawing.Point(16, 213);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(143, 155);
            this.groupBox3.TabIndex = 78;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "INVENTORY";
            // 
            // linkLabel9
            // 
            this.linkLabel9.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel9.AutoSize = true;
            this.linkLabel9.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel9.LinkColor = System.Drawing.Color.White;
            this.linkLabel9.Location = new System.Drawing.Point(12, 57);
            this.linkLabel9.Name = "linkLabel9";
            this.linkLabel9.Size = new System.Drawing.Size(110, 17);
            this.linkLabel9.TabIndex = 26;
            this.linkLabel9.TabStop = true;
            this.linkLabel9.Text = "Material Receipt";
            this.linkLabel9.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel9_LinkClicked);
            // 
            // linkLabel10
            // 
            this.linkLabel10.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel10.AutoSize = true;
            this.linkLabel10.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel10.LinkColor = System.Drawing.Color.White;
            this.linkLabel10.Location = new System.Drawing.Point(11, 82);
            this.linkLabel10.Name = "linkLabel10";
            this.linkLabel10.Size = new System.Drawing.Size(40, 17);
            this.linkLabel10.TabIndex = 26;
            this.linkLabel10.TabStop = true;
            this.linkLabel10.Text = "Issue";
            this.linkLabel10.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel10_LinkClicked);
            // 
            // linkLabel15
            // 
            this.linkLabel15.ActiveLinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel15.AutoSize = true;
            this.linkLabel15.Font = new System.Drawing.Font("Cambria", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel15.LinkColor = System.Drawing.Color.White;
            this.linkLabel15.Location = new System.Drawing.Point(11, 27);
            this.linkLabel15.Name = "linkLabel15";
            this.linkLabel15.Size = new System.Drawing.Size(113, 17);
            this.linkLabel15.TabIndex = 25;
            this.linkLabel15.TabStop = true;
            this.linkLabel15.Text = "Material Request";
            this.linkLabel15.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel15_LinkClicked);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.SkyBlue;
            this.panel5.Controls.Add(this.label25);
            this.panel5.Controls.Add(this.panel2);
            this.panel5.Location = new System.Drawing.Point(165, 39);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(668, 594);
            this.panel5.TabIndex = 76;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.PowderBlue;
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Location = new System.Drawing.Point(22, 43);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(592, 544);
            this.panel2.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Window;
            this.panel8.Controls.Add(this.quotationMasterDataGridView);
            this.panel8.Controls.Add(this.groupBox1);
            this.panel8.Controls.Add(this.panel1);
            this.panel8.Location = new System.Drawing.Point(14, 11);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(632, 540);
            this.panel8.TabIndex = 0;
            // 
            // quotationMasterDataGridView
            // 
            this.quotationMasterDataGridView.AllowUserToAddRows = false;
            this.quotationMasterDataGridView.AllowUserToOrderColumns = true;
            this.quotationMasterDataGridView.AutoGenerateColumns = false;
            this.quotationMasterDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.quotationMasterDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.quotationMasterDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.quotationMasterDataGridView.DataSource = this.quotationMasterBindingSource;
            this.quotationMasterDataGridView.Location = new System.Drawing.Point(3, 274);
            this.quotationMasterDataGridView.Name = "quotationMasterDataGridView";
            this.quotationMasterDataGridView.Size = new System.Drawing.Size(572, 256);
            this.quotationMasterDataGridView.TabIndex = 39;
            this.quotationMasterDataGridView.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.quotationMasterDataGridView_RowHeaderMouseClick);
            // 
            // quotationMasterBindingSource
            // 
            this.quotationMasterBindingSource.DataMember = "QuotationMaster";
            this.quotationMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.textBox10);
            this.groupBox1.Location = new System.Drawing.Point(3, 226);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(572, 49);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search By";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(87, 24);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 41;
            this.label10.Text = "Quotation No";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(466, 19);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 40;
            this.button5.Text = "SEARCH";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(179, 21);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PowderBlue;
            this.panel1.Controls.Add(rateLabel);
            this.panel1.Controls.Add(this.rateTextBox);
            this.panel1.Controls.Add(this.prnoComboBox);
            this.panel1.Controls.Add(this.prnoTextBox);
            this.panel1.Controls.Add(this.comboBox2);
            this.panel1.Controls.Add(this.itemcodeTextBox);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.textBox8);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.textBox5);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(575, 217);
            this.panel1.TabIndex = 37;
            // 
            // rateTextBox
            // 
            this.rateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "rate", true));
            this.rateTextBox.Location = new System.Drawing.Point(464, 75);
            this.rateTextBox.Name = "rateTextBox";
            this.rateTextBox.ReadOnly = true;
            this.rateTextBox.Size = new System.Drawing.Size(100, 20);
            this.rateTextBox.TabIndex = 73;
            // 
            // pRMasterBindingSource
            // 
            this.pRMasterBindingSource.DataMember = "PRMaster";
            this.pRMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // prnoComboBox
            // 
            this.prnoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "prno", true));
            this.prnoComboBox.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.pRMasterBindingSource, "prno", true));
            this.prnoComboBox.DataSource = this.pRMasterBindingSource;
            this.prnoComboBox.DisplayMember = "prno";
            this.prnoComboBox.FormattingEnabled = true;
            this.prnoComboBox.Location = new System.Drawing.Point(179, 149);
            this.prnoComboBox.Name = "prnoComboBox";
            this.prnoComboBox.Size = new System.Drawing.Size(121, 21);
            this.prnoComboBox.TabIndex = 72;
            this.prnoComboBox.ValueMember = "prno";
            // 
            // prnoTextBox
            // 
            this.prnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rFQMasterBindingSource, "prno", true));
            this.prnoTextBox.Location = new System.Drawing.Point(20, 149);
            this.prnoTextBox.Name = "prnoTextBox";
            this.prnoTextBox.Size = new System.Drawing.Size(100, 20);
            this.prnoTextBox.TabIndex = 71;
            this.prnoTextBox.TextChanged += new System.EventHandler(this.prnoTextBox_TextChanged);
            // 
            // rFQMasterBindingSource
            // 
            this.rFQMasterBindingSource.DataMember = "RFQMaster";
            this.rFQMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // comboBox2
            // 
            this.comboBox2.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.quotationMasterBindingSource, "rfqno", true));
            this.comboBox2.DataSource = this.rFQMasterBindingSource;
            this.comboBox2.DisplayMember = "rfqno";
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(290, 48);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(100, 21);
            this.comboBox2.TabIndex = 70;
            this.comboBox2.ValueMember = "rfqno";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(290, 74);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.ReadOnly = true;
            this.itemcodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.itemcodeTextBox.TabIndex = 69;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(290, 104);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(100, 20);
            this.dateTimePicker2.TabIndex = 40;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 15);
            this.label9.TabIndex = 37;
            this.label9.Text = "ADD QUOTATION";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(100, 48);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(112, 20);
            this.textBox1.TabIndex = 27;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(476, 107);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 36;
            this.button4.Text = "ADD";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Quotation No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "RFQ No";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(100, 74);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(112, 20);
            this.dateTimePicker1.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Quotation Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(218, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Item Code";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(412, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 34;
            this.label8.Text = "Tax";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(464, 48);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 33;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(218, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 13);
            this.label6.TabIndex = 25;
            this.label6.Text = "Delivery Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Payment Terms";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(100, 100);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(112, 20);
            this.textBox5.TabIndex = 30;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Cambria", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(15, 8);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(155, 28);
            this.label33.TabIndex = 80;
            this.label33.Text = "QUOTATIONS";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(-65, 444);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(0, 13);
            this.label26.TabIndex = 65;
            this.label26.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(16, 537);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(141, 23);
            this.button2.TabIndex = 22;
            this.button2.Text = "Generate Report";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 566);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // quotationMasterTableAdapter
            // 
            this.quotationMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = null;
            this.tableAdapterManager.GRVMasterTableAdapter = null;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = null;
            this.tableAdapterManager.PRMasterTableAdapter = this.pRMasterTableAdapter;
            this.tableAdapterManager.QuotationMasterTableAdapter = this.quotationMasterTableAdapter;
            this.tableAdapterManager.RFQMasterTableAdapter = this.rFQMasterTableAdapter;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // pRMasterTableAdapter
            // 
            this.pRMasterTableAdapter.ClearBeforeFill = true;
            // 
            // rFQMasterTableAdapter
            // 
            this.rFQMasterTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "quotationno";
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Quotation No";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "rfqno";
            this.dataGridViewTextBoxColumn2.HeaderText = "RFQ No";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "quotation_date";
            this.dataGridViewTextBoxColumn3.HeaderText = "Quotation Date";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "itemcode";
            this.dataGridViewTextBoxColumn4.HeaderText = "Item Code";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "rate";
            this.dataGridViewTextBoxColumn5.HeaderText = "Rate";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "deliverydate";
            this.dataGridViewTextBoxColumn6.HeaderText = "Delivery Date";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "payment_terms";
            this.dataGridViewTextBoxColumn7.HeaderText = "Payment Terms";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "tax";
            this.dataGridViewTextBoxColumn8.HeaderText = "Tax";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // quotations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(791, 638);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.lblUsercode);
            this.MaximizeBox = false;
            this.Name = "quotations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quotations";
            this.Load += new System.EventHandler(this.quotations_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.quotationMasterDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quotationMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pRMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rFQMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox9;
        public System.Windows.Forms.Label lblUsercode;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.LinkLabel linkLabel17;
        private System.Windows.Forms.LinkLabel linkLabel18;
        private System.Windows.Forms.LinkLabel linkLabel16;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.LinkLabel linkLabel20;
        private System.Windows.Forms.LinkLabel linkLabel21;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.LinkLabel linkLabel19;
        private System.Windows.Forms.LinkLabel linkLabel22;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.LinkLabel linkLabel9;
        private System.Windows.Forms.LinkLabel linkLabel10;
        private System.Windows.Forms.LinkLabel linkLabel15;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label10;
        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource quotationMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.QuotationMasterTableAdapter quotationMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView quotationMasterDataGridView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.RFQMasterTableAdapter rFQMasterTableAdapter;
        private System.Windows.Forms.BindingSource rFQMasterBindingSource;
        private System.Windows.Forms.TextBox prnoTextBox;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.PRMasterTableAdapter pRMasterTableAdapter;
        private System.Windows.Forms.BindingSource pRMasterBindingSource;
        private System.Windows.Forms.ComboBox prnoComboBox;
        private System.Windows.Forms.TextBox rateTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    }
}