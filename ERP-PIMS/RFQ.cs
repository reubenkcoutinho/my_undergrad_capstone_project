﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class RFQ : Form
    {
        string usrDetails, usrName, usrCode, msg;
        int val;
        string table = "RFQMaster";
        string query = "Select * From RFQMaster";
        DataSet quoDataset = new DataSet();
        DataSet fromPr = new DataSet();
        DataSet fromRfq = new DataSet();
        DbOperations dbo = new DbOperations();
        public RFQ()
        {
            InitializeComponent();
        }

        private void RFQ_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.RFQMaster' table. You can move, or remove it, as needed.
            this.rFQMasterTableAdapter.Fill(this.rELERPDBDataSet.RFQMaster);

        }
        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }
        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void rFQMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.rFQMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                this.rFQMasterTableAdapter.FillByRFQ(this.rELERPDBDataSet.RFQMaster, ((int)(System.Convert.ChangeType(textBox1.Text, typeof(int)))));
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
        #region LinkLabel
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            quotations abc = new quotations();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PO abc = new PO();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Stock stk = new Stock();
            stk.MdiParent = this.MdiParent;
            stk.Show();
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }
        #endregion

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }

       
    }
}
