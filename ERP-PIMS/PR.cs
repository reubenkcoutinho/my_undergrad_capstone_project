﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class PR : Form
    {

        string usrDetails, usrName, usrCode;
        string msg;
        int val;
        string query = "select * from PRMaster";
        string table = "PRMaster";
        DbOperations dbo = new DbOperations();
        DataSet prDataset = new DataSet();
        public PR()
        {
            InitializeComponent();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        /*private void PR_Load(object sender, EventArgs e)
        {
            this.lblDetails.Text += this.usrName;
            this.lblStatus.Text += this.usrDetails;
            this.lblUsercode.Text = this.usrCode;
        }*/

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }


        private void tabPage2_Click(object sender, EventArgs e)
        {
            //form reload
        }


        private void button4_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // ADD BUTTON
            prDataset = dbo.Select(query, table);
            DataColumn[] keys = new DataColumn[1];
            keys[0] = prDataset.Tables["PRMaster"].Columns["prno"];
            prDataset.Tables["PRMaster"].PrimaryKey = keys;

            DataRow findRow = prDataset.Tables["PRMaster"].Rows.Find(textBox2.Text);

            if (findRow == null)
            {

                DataRow thisRow = prDataset.Tables["PRMaster"].NewRow();
                thisRow["prno"] = textBox2.Text;
                thisRow["itemcode"] = comboBox1.Text;
                thisRow["datereq"] = dateTimePicker1.Text;
                thisRow["quantity"] = textBox8.Text;
                thisRow["rate"] = textBox7.Text;
                thisRow["usercode"] = Convert.ToInt16(this.usrCode);
                //thisRow["status"] = "Fresh";
                prDataset.Tables["PRMaster"].Rows.Add(thisRow);
                msg = dbo.Update(prDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("PR already exists", "ERP-PIMS");
            }
            PR_Load(sender, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //VIEW BUTTON
            //prDataset = dbo.Select("SELECT * FROM PRMaster where prno=" + textBox5.Text, table);
            //dataGridView1.DataSource = prDataset.Tables[table];
        }

        private void PR_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.ItemMaster' table. You can move, or remove it, as needed.
            this.itemMasterTableAdapter.Fill(this.rELERPDBDataSet.ItemMaster);
            comboBox1.Text = textBox7.Text = textBox8.Text = textBox13.Text = textBox14.Text = "";

            // TODO: This line of code loads data into the 'rELERPDBDataSet.PRMaster' table. You can move, or remove it, as needed.
            
            //string qry="SELECT * FROM PRMaster";
            //if (this.usrDetails == "employee")
            //{
            //    button4.Enabled = groupBox2.Enabled = button6.Enabled = dataGridView2.Enabled = false;
            //    qry = "SELECT * FROM PRMaster where usercode=" + this.usrCode;
            //}
            prDataset = dbo.Select(query, table);
            ////dataGridView1.DataSource = prDataset.Tables[table];
            foreach (DataRow theRow in prDataset.Tables["PRMaster"].Rows)
            {

                msg = theRow["prno"].ToString();
                val = Convert.ToInt16(msg);
            }
            textBox2.Text = (++val).ToString();

            if (usrDetails == "employee")
            {
                this.pRMasterTableAdapter.FillByemplogin(this.rELERPDBDataSet.PRMaster, new System.Nullable<int>(((int)(System.Convert.ChangeType(usrCode, typeof(int))))));
                groupBox6.Enabled = false;
                linkLabel6.Enabled = false;
                linkLabel7.Enabled = false;
                linkLabel8.Enabled = false;
                linkLabel2.Enabled = false;
                linkLabel3.Enabled = false;
                linkLabel11.Enabled = false;
                linkLabel12.Enabled = false;
                button4.Enabled = false;
              
                

            }
            else
            {
                this.pRMasterTableAdapter.Fill(this.rELERPDBDataSet.PRMaster);
            }




        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                if (usrDetails == "employee")
                {
                    this.pRMasterTableAdapter.FillByusrnitm(this.rELERPDBDataSet.PRMaster, new System.Nullable<int>(((int)(System.Convert.ChangeType(usrCode, typeof(int))))), new System.Nullable<int>(((int)(System.Convert.ChangeType(textBox14.Text, typeof(int))))));
                }
                else
                {

                    if (radioButton1.Checked)
                    {
                        this.pRMasterTableAdapter.FillByitmcode(this.rELERPDBDataSet.PRMaster, ((int)(System.Convert.ChangeType(textBox14.Text, typeof(int)))));
                    }
                    else if (radioButton2.Checked)
                    {
                        this.pRMasterTableAdapter.FillByusercode(this.rELERPDBDataSet.PRMaster, ((int)(System.Convert.ChangeType(textBox13.Text, typeof(int)))));
                    }
                }

            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox13.Text = textBox14.Text = "";
            textBox14.Enabled = true;
            textBox13.Enabled = false;

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (usrDetails != "employee")
            {

                textBox13.Text = textBox14.Text = "";
                textBox14.Enabled = false;
                textBox13.Enabled = true;
            }
        }

        private void pRMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.pRMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    this.pRMasterTableAdapter.FillBy(this.rELERPDBDataSet.PRMaster, new System.Nullable<int>(((int)(System.Convert.ChangeType(aToolStripTextBox.Text, typeof(int))))));
            //}
            //catch (System.Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show(ex.Message);
            //}

        }

        private void fillByusercodeToolStripButton_Click(object sender, EventArgs e)
        {
            //try
            //{

            //}
            //catch (System.Exception ex)
            //{
            //    System.Windows.Forms.MessageBox.Show(ex.Message);
            //}
        }

        #region LinkedLabel
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            quotations abc = new quotations();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PO abc = new PO();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Stock stk = new Stock();
            stk.MdiParent = this.MdiParent;
            stk.Show();
            this.Hide();
        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }
        #endregion

        private void button2_Click(object sender, EventArgs e)
        {
            PR_Load(sender, e);
        }

        private void pRMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (usrDetails == "employee")
            {
                MessageBox.Show("You dont have sufficient rights to perform this operation","ERP-PIMS");
            }
            else
            {

                DataRowView myDataRowView = (DataRowView)pRMasterDataGridView.SelectedRows[0].DataBoundItem;
                RELERPDBDataSet.PRMasterRow thisRow = (RELERPDBDataSet.PRMasterRow)myDataRowView.Row;
                string str = thisRow["status"].ToString();
                switch(str)
                {
                    case "True":
                        MessageBox.Show("PR has already been authorized.", "ERP-PIMS");
                        break;

                    case "False":
                        MessageBox.Show("PR has already been rejected.", "ERP-PIMS");
                        break;
                    default:
                    int myPosition = rELERPDBDataSet.PRMaster.Rows.IndexOf(thisRow);
                    PRdetails frm = new PRdetails(rELERPDBDataSet, myPosition);
                    frm.ShowDialog();
                    break;
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        

       

       

    }
}