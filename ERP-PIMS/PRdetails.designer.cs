﻿namespace ERP_PIMS
{
    partial class PRdetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label prnoLabel;
            System.Windows.Forms.Label datereqLabel;
            System.Windows.Forms.Label itemcodeLabel;
            System.Windows.Forms.Label quantityLabel;
            System.Windows.Forms.Label rateLabel;
            System.Windows.Forms.Label usercodeLabel;
            System.Windows.Forms.Label statusLabel;
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.prnoTextBox = new System.Windows.Forms.TextBox();
            this.pRMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.datereqDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.quantityTextBox = new System.Windows.Forms.TextBox();
            this.rateTextBox = new System.Windows.Forms.TextBox();
            this.usercodeTextBox = new System.Windows.Forms.TextBox();
            this.statusCheckBox = new System.Windows.Forms.CheckBox();
            this.pRMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.PRMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            prnoLabel = new System.Windows.Forms.Label();
            datereqLabel = new System.Windows.Forms.Label();
            itemcodeLabel = new System.Windows.Forms.Label();
            quantityLabel = new System.Windows.Forms.Label();
            rateLabel = new System.Windows.Forms.Label();
            usercodeLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pRMasterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // prnoLabel
            // 
            prnoLabel.AutoSize = true;
            prnoLabel.Location = new System.Drawing.Point(34, 53);
            prnoLabel.Name = "prnoLabel";
            prnoLabel.Size = new System.Drawing.Size(31, 13);
            prnoLabel.TabIndex = 18;
            prnoLabel.Text = "prno:";
            // 
            // datereqLabel
            // 
            datereqLabel.AutoSize = true;
            datereqLabel.Location = new System.Drawing.Point(34, 80);
            datereqLabel.Name = "datereqLabel";
            datereqLabel.Size = new System.Drawing.Size(46, 13);
            datereqLabel.TabIndex = 20;
            datereqLabel.Text = "datereq:";
            // 
            // itemcodeLabel
            // 
            itemcodeLabel.AutoSize = true;
            itemcodeLabel.Location = new System.Drawing.Point(34, 105);
            itemcodeLabel.Name = "itemcodeLabel";
            itemcodeLabel.Size = new System.Drawing.Size(53, 13);
            itemcodeLabel.TabIndex = 22;
            itemcodeLabel.Text = "itemcode:";
            // 
            // quantityLabel
            // 
            quantityLabel.AutoSize = true;
            quantityLabel.Location = new System.Drawing.Point(34, 131);
            quantityLabel.Name = "quantityLabel";
            quantityLabel.Size = new System.Drawing.Size(47, 13);
            quantityLabel.TabIndex = 24;
            quantityLabel.Text = "quantity:";
            // 
            // rateLabel
            // 
            rateLabel.AutoSize = true;
            rateLabel.Location = new System.Drawing.Point(34, 157);
            rateLabel.Name = "rateLabel";
            rateLabel.Size = new System.Drawing.Size(28, 13);
            rateLabel.TabIndex = 26;
            rateLabel.Text = "rate:";
            // 
            // usercodeLabel
            // 
            usercodeLabel.AutoSize = true;
            usercodeLabel.Location = new System.Drawing.Point(34, 183);
            usercodeLabel.Name = "usercodeLabel";
            usercodeLabel.Size = new System.Drawing.Size(54, 13);
            usercodeLabel.TabIndex = 28;
            usercodeLabel.Text = "usercode:";
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Location = new System.Drawing.Point(34, 219);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(51, 13);
            statusLabel.TabIndex = 29;
            statusLabel.Text = "Authorize";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(21, 268);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "Authorize";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(125, 268);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "Cancel PR";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(226, 268);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 17;
            this.button3.Text = "Close Form";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 19);
            this.label1.TabIndex = 18;
            this.label1.Text = "Edit Purchase Details";
            // 
            // prnoTextBox
            // 
            this.prnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "prno", true));
            this.prnoTextBox.Location = new System.Drawing.Point(94, 50);
            this.prnoTextBox.Name = "prnoTextBox";
            this.prnoTextBox.Size = new System.Drawing.Size(200, 20);
            this.prnoTextBox.TabIndex = 19;
            // 
            // pRMasterBindingSource
            // 
            this.pRMasterBindingSource.DataMember = "PRMaster";
            this.pRMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // datereqDateTimePicker
            // 
            this.datereqDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.pRMasterBindingSource, "datereq", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.datereqDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datereqDateTimePicker.Location = new System.Drawing.Point(94, 76);
            this.datereqDateTimePicker.MinDate = new System.DateTime(2009, 7, 1, 0, 0, 0, 0);
            this.datereqDateTimePicker.Name = "datereqDateTimePicker";
            this.datereqDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.datereqDateTimePicker.TabIndex = 21;
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(94, 102);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.Size = new System.Drawing.Size(200, 20);
            this.itemcodeTextBox.TabIndex = 23;
            // 
            // quantityTextBox
            // 
            this.quantityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "quantity", true));
            this.quantityTextBox.Location = new System.Drawing.Point(94, 128);
            this.quantityTextBox.Name = "quantityTextBox";
            this.quantityTextBox.Size = new System.Drawing.Size(200, 20);
            this.quantityTextBox.TabIndex = 25;
            // 
            // rateTextBox
            // 
            this.rateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "rate", true));
            this.rateTextBox.Location = new System.Drawing.Point(94, 154);
            this.rateTextBox.Name = "rateTextBox";
            this.rateTextBox.Size = new System.Drawing.Size(200, 20);
            this.rateTextBox.TabIndex = 27;
            // 
            // usercodeTextBox
            // 
            this.usercodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "usercode", true));
            this.usercodeTextBox.Location = new System.Drawing.Point(94, 180);
            this.usercodeTextBox.Name = "usercodeTextBox";
            this.usercodeTextBox.Size = new System.Drawing.Size(200, 20);
            this.usercodeTextBox.TabIndex = 29;
            // 
            // statusCheckBox
            // 
            this.statusCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.pRMasterBindingSource, "status", true));
            this.statusCheckBox.Location = new System.Drawing.Point(94, 214);
            this.statusCheckBox.Name = "statusCheckBox";
            this.statusCheckBox.Size = new System.Drawing.Size(104, 24);
            this.statusCheckBox.TabIndex = 30;
            this.statusCheckBox.UseVisualStyleBackColor = true;
            // 
            // pRMasterTableAdapter
            // 
            this.pRMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = null;
            this.tableAdapterManager.GRVMasterTableAdapter = null;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = null;
            this.tableAdapterManager.PRMasterTableAdapter = this.pRMasterTableAdapter;
            this.tableAdapterManager.QuotationMasterTableAdapter = null;
            this.tableAdapterManager.RFQMasterTableAdapter = null;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // PRdetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(343, 311);
            this.Controls.Add(statusLabel);
            this.Controls.Add(this.statusCheckBox);
            this.Controls.Add(prnoLabel);
            this.Controls.Add(this.prnoTextBox);
            this.Controls.Add(datereqLabel);
            this.Controls.Add(this.datereqDateTimePicker);
            this.Controls.Add(itemcodeLabel);
            this.Controls.Add(this.itemcodeTextBox);
            this.Controls.Add(quantityLabel);
            this.Controls.Add(this.quantityTextBox);
            this.Controls.Add(rateLabel);
            this.Controls.Add(this.rateTextBox);
            this.Controls.Add(usercodeLabel);
            this.Controls.Add(this.usercodeTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "PRdetails";
            this.Text = "PRdetails";
            ((System.ComponentModel.ISupportInitialize)(this.pRMasterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource pRMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.PRMasterTableAdapter pRMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox prnoTextBox;
        private System.Windows.Forms.DateTimePicker datereqDateTimePicker;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private System.Windows.Forms.TextBox quantityTextBox;
        private System.Windows.Forms.TextBox rateTextBox;
        private System.Windows.Forms.TextBox usercodeTextBox;
        private System.Windows.Forms.CheckBox statusCheckBox;


    }
}