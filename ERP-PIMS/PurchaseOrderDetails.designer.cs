﻿namespace ERP_PIMS
{
    partial class PurchaseOrderDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label po_noLabel;
            System.Windows.Forms.Label quotationnoLabel;
            System.Windows.Forms.Label itemcodeLabel;
            System.Windows.Forms.Label amountLabel;
            System.Windows.Forms.Label deliverydateLabel;
            System.Windows.Forms.Label deliverytimeLabel;
            System.Windows.Forms.Label taxesLabel;
            System.Windows.Forms.Label payment_termsLabel;
            System.Windows.Forms.Label budgetcodeLabel;
            System.Windows.Forms.Label cc_codeLabel;
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.pOMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pOMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.POMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.po_noTextBox = new System.Windows.Forms.TextBox();
            this.quotationnoTextBox = new System.Windows.Forms.TextBox();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.taxesTextBox = new System.Windows.Forms.TextBox();
            this.payment_termsTextBox = new System.Windows.Forms.TextBox();
            this.budgetcodeTextBox = new System.Windows.Forms.TextBox();
            this.cc_codeTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            po_noLabel = new System.Windows.Forms.Label();
            quotationnoLabel = new System.Windows.Forms.Label();
            itemcodeLabel = new System.Windows.Forms.Label();
            amountLabel = new System.Windows.Forms.Label();
            deliverydateLabel = new System.Windows.Forms.Label();
            deliverytimeLabel = new System.Windows.Forms.Label();
            taxesLabel = new System.Windows.Forms.Label();
            payment_termsLabel = new System.Windows.Forms.Label();
            budgetcodeLabel = new System.Windows.Forms.Label();
            cc_codeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // po_noLabel
            // 
            po_noLabel.AutoSize = true;
            po_noLabel.Location = new System.Drawing.Point(43, 19);
            po_noLabel.Name = "po_noLabel";
            po_noLabel.Size = new System.Drawing.Size(37, 13);
            po_noLabel.TabIndex = 1;
            po_noLabel.Text = "po no:";
            // 
            // quotationnoLabel
            // 
            quotationnoLabel.AutoSize = true;
            quotationnoLabel.Location = new System.Drawing.Point(43, 45);
            quotationnoLabel.Name = "quotationnoLabel";
            quotationnoLabel.Size = new System.Drawing.Size(66, 13);
            quotationnoLabel.TabIndex = 3;
            quotationnoLabel.Text = "quotationno:";
            // 
            // itemcodeLabel
            // 
            itemcodeLabel.AutoSize = true;
            itemcodeLabel.Location = new System.Drawing.Point(43, 71);
            itemcodeLabel.Name = "itemcodeLabel";
            itemcodeLabel.Size = new System.Drawing.Size(53, 13);
            itemcodeLabel.TabIndex = 5;
            itemcodeLabel.Text = "itemcode:";
            // 
            // amountLabel
            // 
            amountLabel.AutoSize = true;
            amountLabel.Location = new System.Drawing.Point(43, 97);
            amountLabel.Name = "amountLabel";
            amountLabel.Size = new System.Drawing.Size(45, 13);
            amountLabel.TabIndex = 7;
            amountLabel.Text = "amount:";
            // 
            // deliverydateLabel
            // 
            deliverydateLabel.AutoSize = true;
            deliverydateLabel.Location = new System.Drawing.Point(43, 123);
            deliverydateLabel.Name = "deliverydateLabel";
            deliverydateLabel.Size = new System.Drawing.Size(67, 13);
            deliverydateLabel.TabIndex = 9;
            deliverydateLabel.Text = "deliverydate:";
            // 
            // deliverytimeLabel
            // 
            deliverytimeLabel.AutoSize = true;
            deliverytimeLabel.Location = new System.Drawing.Point(43, 149);
            deliverytimeLabel.Name = "deliverytimeLabel";
            deliverytimeLabel.Size = new System.Drawing.Size(65, 13);
            deliverytimeLabel.TabIndex = 11;
            deliverytimeLabel.Text = "deliverytime:";
            // 
            // taxesLabel
            // 
            taxesLabel.AutoSize = true;
            taxesLabel.Location = new System.Drawing.Point(43, 175);
            taxesLabel.Name = "taxesLabel";
            taxesLabel.Size = new System.Drawing.Size(35, 13);
            taxesLabel.TabIndex = 13;
            taxesLabel.Text = "taxes:";
            // 
            // payment_termsLabel
            // 
            payment_termsLabel.AutoSize = true;
            payment_termsLabel.Location = new System.Drawing.Point(43, 201);
            payment_termsLabel.Name = "payment_termsLabel";
            payment_termsLabel.Size = new System.Drawing.Size(78, 13);
            payment_termsLabel.TabIndex = 15;
            payment_termsLabel.Text = "payment terms:";
            // 
            // budgetcodeLabel
            // 
            budgetcodeLabel.AutoSize = true;
            budgetcodeLabel.Location = new System.Drawing.Point(43, 227);
            budgetcodeLabel.Name = "budgetcodeLabel";
            budgetcodeLabel.Size = new System.Drawing.Size(67, 13);
            budgetcodeLabel.TabIndex = 17;
            budgetcodeLabel.Text = "budgetcode:";
            // 
            // cc_codeLabel
            // 
            cc_codeLabel.AutoSize = true;
            cc_codeLabel.Location = new System.Drawing.Point(43, 253);
            cc_codeLabel.Name = "cc_codeLabel";
            cc_codeLabel.Size = new System.Drawing.Size(49, 13);
            cc_codeLabel.TabIndex = 19;
            cc_codeLabel.Text = "cc code:";
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pOMasterBindingSource
            // 
            this.pOMasterBindingSource.DataMember = "POMaster";
            this.pOMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // pOMasterTableAdapter
            // 
            this.pOMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = null;
            this.tableAdapterManager.GRVMasterTableAdapter = null;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = this.pOMasterTableAdapter;
            this.tableAdapterManager.PRMasterTableAdapter = null;
            this.tableAdapterManager.QuotationMasterTableAdapter = null;
            this.tableAdapterManager.RFQMasterTableAdapter = null;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // po_noTextBox
            // 
            this.po_noTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "po_no", true));
            this.po_noTextBox.Location = new System.Drawing.Point(127, 16);
            this.po_noTextBox.Name = "po_noTextBox";
            this.po_noTextBox.ReadOnly = true;
            this.po_noTextBox.Size = new System.Drawing.Size(100, 20);
            this.po_noTextBox.TabIndex = 2;
            // 
            // quotationnoTextBox
            // 
            this.quotationnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "quotationno", true));
            this.quotationnoTextBox.Location = new System.Drawing.Point(127, 42);
            this.quotationnoTextBox.Name = "quotationnoTextBox";
            this.quotationnoTextBox.ReadOnly = true;
            this.quotationnoTextBox.Size = new System.Drawing.Size(100, 20);
            this.quotationnoTextBox.TabIndex = 4;
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(127, 68);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.ReadOnly = true;
            this.itemcodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.itemcodeTextBox.TabIndex = 6;
            // 
            // amountTextBox
            // 
            this.amountTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "amount", true));
            this.amountTextBox.Location = new System.Drawing.Point(127, 94);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.ReadOnly = true;
            this.amountTextBox.Size = new System.Drawing.Size(100, 20);
            this.amountTextBox.TabIndex = 8;
            // 
            // taxesTextBox
            // 
            this.taxesTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "taxes", true));
            this.taxesTextBox.Location = new System.Drawing.Point(127, 172);
            this.taxesTextBox.Name = "taxesTextBox";
            this.taxesTextBox.ReadOnly = true;
            this.taxesTextBox.Size = new System.Drawing.Size(100, 20);
            this.taxesTextBox.TabIndex = 14;
            // 
            // payment_termsTextBox
            // 
            this.payment_termsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "payment_terms", true));
            this.payment_termsTextBox.Location = new System.Drawing.Point(127, 198);
            this.payment_termsTextBox.Name = "payment_termsTextBox";
            this.payment_termsTextBox.Size = new System.Drawing.Size(100, 20);
            this.payment_termsTextBox.TabIndex = 16;
            // 
            // budgetcodeTextBox
            // 
            this.budgetcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "budgetcode", true));
            this.budgetcodeTextBox.Location = new System.Drawing.Point(127, 224);
            this.budgetcodeTextBox.Name = "budgetcodeTextBox";
            this.budgetcodeTextBox.ReadOnly = true;
            this.budgetcodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.budgetcodeTextBox.TabIndex = 18;
            // 
            // cc_codeTextBox
            // 
            this.cc_codeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pOMasterBindingSource, "cc_code", true));
            this.cc_codeTextBox.Location = new System.Drawing.Point(127, 250);
            this.cc_codeTextBox.Name = "cc_codeTextBox";
            this.cc_codeTextBox.ReadOnly = true;
            this.cc_codeTextBox.Size = new System.Drawing.Size(100, 20);
            this.cc_codeTextBox.TabIndex = 20;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 290);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(118, 290);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 22;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(224, 290);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 23;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.pOMasterBindingSource, "deliverydate", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(127, 120);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePicker1.TabIndex = 24;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.pOMasterBindingSource, "deliverytime", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "t"));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker2.Location = new System.Drawing.Point(127, 146);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(100, 20);
            this.dateTimePicker2.TabIndex = 25;
            // 
            // PurchaseOrderDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(310, 345);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(po_noLabel);
            this.Controls.Add(this.po_noTextBox);
            this.Controls.Add(quotationnoLabel);
            this.Controls.Add(this.quotationnoTextBox);
            this.Controls.Add(itemcodeLabel);
            this.Controls.Add(this.itemcodeTextBox);
            this.Controls.Add(amountLabel);
            this.Controls.Add(this.amountTextBox);
            this.Controls.Add(deliverydateLabel);
            this.Controls.Add(deliverytimeLabel);
            this.Controls.Add(taxesLabel);
            this.Controls.Add(this.taxesTextBox);
            this.Controls.Add(payment_termsLabel);
            this.Controls.Add(this.payment_termsTextBox);
            this.Controls.Add(budgetcodeLabel);
            this.Controls.Add(this.budgetcodeTextBox);
            this.Controls.Add(cc_codeLabel);
            this.Controls.Add(this.cc_codeTextBox);
            this.Name = "PurchaseOrderDetails";
            this.Text = "PurchaseOrderDetails";
            this.Load += new System.EventHandler(this.PurchaseOrderDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pOMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource pOMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.POMasterTableAdapter pOMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox po_noTextBox;
        private System.Windows.Forms.TextBox quotationnoTextBox;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.TextBox taxesTextBox;
        private System.Windows.Forms.TextBox payment_termsTextBox;
        private System.Windows.Forms.TextBox budgetcodeTextBox;
        private System.Windows.Forms.TextBox cc_codeTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
    }
}