﻿namespace ERP_PIMS
{
    partial class GRNDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label grnoLabel;
            System.Windows.Forms.Label grdateLabel;
            System.Windows.Forms.Label po_noLabel;
            System.Windows.Forms.Label itemcodeLabel;
            System.Windows.Forms.Label qtyrcvdLabel;
            System.Windows.Forms.Label qtyaccptdLabel;
            System.Windows.Forms.Label rateLabel;
            System.Windows.Forms.Label valueLabel;
            System.Windows.Forms.Label statusLabel;
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.gRMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gRMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.GRMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.grnoTextBox = new System.Windows.Forms.TextBox();
            this.grdateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.po_noTextBox = new System.Windows.Forms.TextBox();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.qtyrcvdTextBox = new System.Windows.Forms.TextBox();
            this.qtyaccptdTextBox = new System.Windows.Forms.TextBox();
            this.rateTextBox = new System.Windows.Forms.TextBox();
            this.valueTextBox = new System.Windows.Forms.TextBox();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            grnoLabel = new System.Windows.Forms.Label();
            grdateLabel = new System.Windows.Forms.Label();
            po_noLabel = new System.Windows.Forms.Label();
            itemcodeLabel = new System.Windows.Forms.Label();
            qtyrcvdLabel = new System.Windows.Forms.Label();
            qtyaccptdLabel = new System.Windows.Forms.Label();
            rateLabel = new System.Windows.Forms.Label();
            valueLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grnoLabel
            // 
            grnoLabel.AutoSize = true;
            grnoLabel.Location = new System.Drawing.Point(29, 21);
            grnoLabel.Name = "grnoLabel";
            grnoLabel.Size = new System.Drawing.Size(31, 13);
            grnoLabel.TabIndex = 1;
            grnoLabel.Text = "grno:";
            // 
            // grdateLabel
            // 
            grdateLabel.AutoSize = true;
            grdateLabel.Location = new System.Drawing.Point(29, 48);
            grdateLabel.Name = "grdateLabel";
            grdateLabel.Size = new System.Drawing.Size(40, 13);
            grdateLabel.TabIndex = 3;
            grdateLabel.Text = "grdate:";
            // 
            // po_noLabel
            // 
            po_noLabel.AutoSize = true;
            po_noLabel.Location = new System.Drawing.Point(29, 73);
            po_noLabel.Name = "po_noLabel";
            po_noLabel.Size = new System.Drawing.Size(37, 13);
            po_noLabel.TabIndex = 5;
            po_noLabel.Text = "po no:";
            // 
            // itemcodeLabel
            // 
            itemcodeLabel.AutoSize = true;
            itemcodeLabel.Location = new System.Drawing.Point(29, 99);
            itemcodeLabel.Name = "itemcodeLabel";
            itemcodeLabel.Size = new System.Drawing.Size(53, 13);
            itemcodeLabel.TabIndex = 7;
            itemcodeLabel.Text = "itemcode:";
            // 
            // qtyrcvdLabel
            // 
            qtyrcvdLabel.AutoSize = true;
            qtyrcvdLabel.Location = new System.Drawing.Point(29, 125);
            qtyrcvdLabel.Name = "qtyrcvdLabel";
            qtyrcvdLabel.Size = new System.Drawing.Size(45, 13);
            qtyrcvdLabel.TabIndex = 9;
            qtyrcvdLabel.Text = "qtyrcvd:";
            // 
            // qtyaccptdLabel
            // 
            qtyaccptdLabel.AutoSize = true;
            qtyaccptdLabel.Location = new System.Drawing.Point(29, 151);
            qtyaccptdLabel.Name = "qtyaccptdLabel";
            qtyaccptdLabel.Size = new System.Drawing.Size(57, 13);
            qtyaccptdLabel.TabIndex = 11;
            qtyaccptdLabel.Text = "qtyaccptd:";
            // 
            // rateLabel
            // 
            rateLabel.AutoSize = true;
            rateLabel.Location = new System.Drawing.Point(29, 177);
            rateLabel.Name = "rateLabel";
            rateLabel.Size = new System.Drawing.Size(28, 13);
            rateLabel.TabIndex = 13;
            rateLabel.Text = "rate:";
            // 
            // valueLabel
            // 
            valueLabel.AutoSize = true;
            valueLabel.Location = new System.Drawing.Point(29, 203);
            valueLabel.Name = "valueLabel";
            valueLabel.Size = new System.Drawing.Size(36, 13);
            valueLabel.TabIndex = 15;
            valueLabel.Text = "value:";
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Location = new System.Drawing.Point(29, 229);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(38, 13);
            statusLabel.TabIndex = 17;
            statusLabel.Text = "status:";
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gRMasterBindingSource
            // 
            this.gRMasterBindingSource.DataMember = "GRMaster";
            this.gRMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // gRMasterTableAdapter
            // 
            this.gRMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = this.gRMasterTableAdapter;
            this.tableAdapterManager.GRVMasterTableAdapter = null;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = null;
            this.tableAdapterManager.PRMasterTableAdapter = null;
            this.tableAdapterManager.QuotationMasterTableAdapter = null;
            this.tableAdapterManager.RFQMasterTableAdapter = null;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // grnoTextBox
            // 
            this.grnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRMasterBindingSource, "grno", true));
            this.grnoTextBox.Location = new System.Drawing.Point(92, 18);
            this.grnoTextBox.Name = "grnoTextBox";
            this.grnoTextBox.Size = new System.Drawing.Size(132, 20);
            this.grnoTextBox.TabIndex = 2;
            // 
            // grdateDateTimePicker
            // 
            this.grdateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.gRMasterBindingSource, "grdate", true));
            this.grdateDateTimePicker.Location = new System.Drawing.Point(92, 44);
            this.grdateDateTimePicker.Name = "grdateDateTimePicker";
            this.grdateDateTimePicker.Size = new System.Drawing.Size(132, 20);
            this.grdateDateTimePicker.TabIndex = 4;
            // 
            // po_noTextBox
            // 
            this.po_noTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRMasterBindingSource, "po_no", true));
            this.po_noTextBox.Location = new System.Drawing.Point(92, 70);
            this.po_noTextBox.Name = "po_noTextBox";
            this.po_noTextBox.Size = new System.Drawing.Size(132, 20);
            this.po_noTextBox.TabIndex = 6;
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(92, 96);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.Size = new System.Drawing.Size(132, 20);
            this.itemcodeTextBox.TabIndex = 8;
            // 
            // qtyrcvdTextBox
            // 
            this.qtyrcvdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRMasterBindingSource, "qtyrcvd", true));
            this.qtyrcvdTextBox.Location = new System.Drawing.Point(92, 122);
            this.qtyrcvdTextBox.Name = "qtyrcvdTextBox";
            this.qtyrcvdTextBox.Size = new System.Drawing.Size(132, 20);
            this.qtyrcvdTextBox.TabIndex = 10;
            // 
            // qtyaccptdTextBox
            // 
            this.qtyaccptdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRMasterBindingSource, "qtyaccptd", true));
            this.qtyaccptdTextBox.Location = new System.Drawing.Point(92, 148);
            this.qtyaccptdTextBox.Name = "qtyaccptdTextBox";
            this.qtyaccptdTextBox.Size = new System.Drawing.Size(132, 20);
            this.qtyaccptdTextBox.TabIndex = 12;
            // 
            // rateTextBox
            // 
            this.rateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRMasterBindingSource, "rate", true));
            this.rateTextBox.Location = new System.Drawing.Point(92, 174);
            this.rateTextBox.Name = "rateTextBox";
            this.rateTextBox.Size = new System.Drawing.Size(132, 20);
            this.rateTextBox.TabIndex = 14;
            // 
            // valueTextBox
            // 
            this.valueTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRMasterBindingSource, "value", true));
            this.valueTextBox.Location = new System.Drawing.Point(92, 200);
            this.valueTextBox.Name = "valueTextBox";
            this.valueTextBox.Size = new System.Drawing.Size(132, 20);
            this.valueTextBox.TabIndex = 16;
            // 
            // statusTextBox
            // 
            this.statusTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRMasterBindingSource, "status", true));
            this.statusTextBox.Location = new System.Drawing.Point(92, 226);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.Size = new System.Drawing.Size(132, 20);
            this.statusTextBox.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 277);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(189, 277);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 20;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // GRNDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 312);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(grnoLabel);
            this.Controls.Add(this.grnoTextBox);
            this.Controls.Add(grdateLabel);
            this.Controls.Add(this.grdateDateTimePicker);
            this.Controls.Add(po_noLabel);
            this.Controls.Add(this.po_noTextBox);
            this.Controls.Add(itemcodeLabel);
            this.Controls.Add(this.itemcodeTextBox);
            this.Controls.Add(qtyrcvdLabel);
            this.Controls.Add(this.qtyrcvdTextBox);
            this.Controls.Add(qtyaccptdLabel);
            this.Controls.Add(this.qtyaccptdTextBox);
            this.Controls.Add(rateLabel);
            this.Controls.Add(this.rateTextBox);
            this.Controls.Add(valueLabel);
            this.Controls.Add(this.valueTextBox);
            this.Controls.Add(statusLabel);
            this.Controls.Add(this.statusTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "GRNDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GRNDetails";
            this.Load += new System.EventHandler(this.GRNDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource gRMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.GRMasterTableAdapter gRMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox grnoTextBox;
        private System.Windows.Forms.DateTimePicker grdateDateTimePicker;
        private System.Windows.Forms.TextBox po_noTextBox;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private System.Windows.Forms.TextBox qtyrcvdTextBox;
        private System.Windows.Forms.TextBox qtyaccptdTextBox;
        private System.Windows.Forms.TextBox rateTextBox;
        private System.Windows.Forms.TextBox valueTextBox;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}