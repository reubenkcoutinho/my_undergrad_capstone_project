﻿namespace ERP_PIMS
{
    partial class menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PRMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RFQMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qtsMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.poMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.budgetMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mreqMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stockMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goodMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grnMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cascadeMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horizontalMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verticalMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.LblUsercode = new System.Windows.Forms.ToolStripStatusLabel();
            this.LblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblDetails = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuStripItem,
            this.purchaseMenuStripItem,
            this.inventoryMenuStripItem,
            this.goodMenuStripItem,
            this.adminMenuStripItem,
            this.windowMenuStripItem,
            this.helpMenuStripItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(530, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileMenuStripItem
            // 
            this.fileMenuStripItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutMenuStripItem});
            this.fileMenuStripItem.Name = "fileMenuStripItem";
            this.fileMenuStripItem.Size = new System.Drawing.Size(37, 20);
            this.fileMenuStripItem.Text = "&File";
            // 
            // logoutMenuStripItem
            // 
            this.logoutMenuStripItem.Name = "logoutMenuStripItem";
            this.logoutMenuStripItem.Size = new System.Drawing.Size(112, 22);
            this.logoutMenuStripItem.Text = "&Logout";
            this.logoutMenuStripItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // purchaseMenuStripItem
            // 
            this.purchaseMenuStripItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PRMenuStripItem,
            this.RFQMenuStripItem,
            this.qtsMenuStripItem,
            this.poMenuStripItem,
            this.budgetMenuStripItem});
            this.purchaseMenuStripItem.Name = "purchaseMenuStripItem";
            this.purchaseMenuStripItem.Size = new System.Drawing.Size(67, 20);
            this.purchaseMenuStripItem.Text = "&Purchase";
            // 
            // PRMenuStripItem
            // 
            this.PRMenuStripItem.Name = "PRMenuStripItem";
            this.PRMenuStripItem.Size = new System.Drawing.Size(200, 22);
            this.PRMenuStripItem.Text = "&Raise PR";
            this.PRMenuStripItem.Click += new System.EventHandler(this.raisePRToolStripMenuItem_Click);
            // 
            // RFQMenuStripItem
            // 
            this.RFQMenuStripItem.Name = "RFQMenuStripItem";
            this.RFQMenuStripItem.Size = new System.Drawing.Size(200, 22);
            this.RFQMenuStripItem.Text = "R&FQ";
            this.RFQMenuStripItem.Click += new System.EventHandler(this.RFQMenuStripItem_Click);
            // 
            // qtsMenuStripItem
            // 
            this.qtsMenuStripItem.Name = "qtsMenuStripItem";
            this.qtsMenuStripItem.Size = new System.Drawing.Size(200, 22);
            this.qtsMenuStripItem.Text = "&Quotations";
            this.qtsMenuStripItem.Click += new System.EventHandler(this.qoutationsToolStripMenuItem_Click);
            // 
            // poMenuStripItem
            // 
            this.poMenuStripItem.Name = "poMenuStripItem";
            this.poMenuStripItem.Size = new System.Drawing.Size(200, 22);
            this.poMenuStripItem.Text = "Purchase &Order";
            this.poMenuStripItem.Click += new System.EventHandler(this.purchaseOrderToolStripMenuItem_Click);
            // 
            // budgetMenuStripItem
            // 
            this.budgetMenuStripItem.Name = "budgetMenuStripItem";
            this.budgetMenuStripItem.Size = new System.Drawing.Size(200, 22);
            this.budgetMenuStripItem.Text = "&Budget and Cost Center";
            this.budgetMenuStripItem.Click += new System.EventHandler(this.budgetToolStripMenuItem_Click);
            // 
            // inventoryMenuStripItem
            // 
            this.inventoryMenuStripItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mreqMenuStripItem,
            this.issueMenuStripItem,
            this.stockMenuStripItem});
            this.inventoryMenuStripItem.Name = "inventoryMenuStripItem";
            this.inventoryMenuStripItem.Size = new System.Drawing.Size(69, 20);
            this.inventoryMenuStripItem.Text = "&Inventory";
            // 
            // mreqMenuStripItem
            // 
            this.mreqMenuStripItem.Name = "mreqMenuStripItem";
            this.mreqMenuStripItem.Size = new System.Drawing.Size(167, 22);
            this.mreqMenuStripItem.Text = "&Request Materials";
            this.mreqMenuStripItem.Click += new System.EventHandler(this.requestMaterialsToolStripMenuItem_Click);
            // 
            // issueMenuStripItem
            // 
            this.issueMenuStripItem.Name = "issueMenuStripItem";
            this.issueMenuStripItem.Size = new System.Drawing.Size(167, 22);
            this.issueMenuStripItem.Text = "I&ssue";
            this.issueMenuStripItem.Click += new System.EventHandler(this.issueToolStripMenuItem_Click);
            // 
            // stockMenuStripItem
            // 
            this.stockMenuStripItem.Name = "stockMenuStripItem";
            this.stockMenuStripItem.Size = new System.Drawing.Size(167, 22);
            this.stockMenuStripItem.Text = "&Stock";
            this.stockMenuStripItem.Click += new System.EventHandler(this.stockToolStripMenuItem_Click);
            // 
            // goodMenuStripItem
            // 
            this.goodMenuStripItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemMenuStripItem,
            this.grnMenuStripItem});
            this.goodMenuStripItem.Name = "goodMenuStripItem";
            this.goodMenuStripItem.Size = new System.Drawing.Size(53, 20);
            this.goodMenuStripItem.Text = "&Goods";
            // 
            // itemMenuStripItem
            // 
            this.itemMenuStripItem.Name = "itemMenuStripItem";
            this.itemMenuStripItem.Size = new System.Drawing.Size(184, 22);
            this.itemMenuStripItem.Text = "I&tem";
            this.itemMenuStripItem.Click += new System.EventHandler(this.itemToolStripMenuItem_Click);
            // 
            // grnMenuStripItem
            // 
            this.grnMenuStripItem.Name = "grnMenuStripItem";
            this.grnMenuStripItem.Size = new System.Drawing.Size(184, 22);
            this.grnMenuStripItem.Text = "GRN and GRV &Details";
            this.grnMenuStripItem.Click += new System.EventHandler(this.goodsReciptNoteToolStripMenuItem_Click);
            // 
            // adminMenuStripItem
            // 
            this.adminMenuStripItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userMenuStripItem});
            this.adminMenuStripItem.Name = "adminMenuStripItem";
            this.adminMenuStripItem.Size = new System.Drawing.Size(55, 20);
            this.adminMenuStripItem.Text = "&Admin";
            // 
            // userMenuStripItem
            // 
            this.userMenuStripItem.Name = "userMenuStripItem";
            this.userMenuStripItem.Size = new System.Drawing.Size(97, 22);
            this.userMenuStripItem.Text = "&User";
            this.userMenuStripItem.Click += new System.EventHandler(this.userToolStripMenuItem_Click);
            // 
            // windowMenuStripItem
            // 
            this.windowMenuStripItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cascadeMenuStripItem,
            this.horizontalMenuStripItem,
            this.verticalMenuStripItem});
            this.windowMenuStripItem.Name = "windowMenuStripItem";
            this.windowMenuStripItem.Size = new System.Drawing.Size(63, 20);
            this.windowMenuStripItem.Text = "&Window";
            // 
            // cascadeMenuStripItem
            // 
            this.cascadeMenuStripItem.Name = "cascadeMenuStripItem";
            this.cascadeMenuStripItem.Size = new System.Drawing.Size(151, 22);
            this.cascadeMenuStripItem.Text = "&Cascade";
            this.cascadeMenuStripItem.Click += new System.EventHandler(this.cascadeMenuStripItem_Click);
            // 
            // horizontalMenuStripItem
            // 
            this.horizontalMenuStripItem.Name = "horizontalMenuStripItem";
            this.horizontalMenuStripItem.Size = new System.Drawing.Size(151, 22);
            this.horizontalMenuStripItem.Text = "Tile &Horizontal";
            this.horizontalMenuStripItem.Click += new System.EventHandler(this.horizontalMenuStripItem_Click);
            // 
            // verticalMenuStripItem
            // 
            this.verticalMenuStripItem.Name = "verticalMenuStripItem";
            this.verticalMenuStripItem.Size = new System.Drawing.Size(151, 22);
            this.verticalMenuStripItem.Text = "Tile &Vertical";
            this.verticalMenuStripItem.Click += new System.EventHandler(this.verticalMenuStripItem_Click);
            // 
            // helpMenuStripItem
            // 
            this.helpMenuStripItem.Name = "helpMenuStripItem";
            this.helpMenuStripItem.Size = new System.Drawing.Size(44, 20);
            this.helpMenuStripItem.Text = "&Help";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LblUsercode,
            this.LblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 400);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(530, 22);
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStripInfo";
            // 
            // LblUsercode
            // 
            this.LblUsercode.Name = "LblUsercode";
            this.LblUsercode.Size = new System.Drawing.Size(72, 17);
            this.LblUsercode.Text = "LblUsercode";
            this.LblUsercode.Visible = false;
            // 
            // LblStatus
            // 
            this.LblStatus.Name = "LblStatus";
            this.LblStatus.Size = new System.Drawing.Size(55, 17);
            this.LblStatus.Text = "LblStatus";
            this.LblStatus.ToolTipText = "What You Are...";
            // 
            // lblDetails
            // 
            this.lblDetails.AutoSize = true;
            this.lblDetails.Location = new System.Drawing.Point(414, 9);
            this.lblDetails.Name = "lblDetails";
            this.lblDetails.Size = new System.Drawing.Size(34, 13);
            this.lblDetails.TabIndex = 13;
            this.lblDetails.Text = "Hello ";
            this.lblDetails.Visible = false;
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(530, 422);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblDetails);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.menu_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.menu_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem PRMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem qtsMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem poMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem mreqMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem issueMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem goodMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem adminMenuStripItem;
        private System.Windows.Forms.ToolStripStatusLabel LblUsercode;
        private System.Windows.Forms.ToolStripMenuItem stockMenuStripItem;
        private System.Windows.Forms.ToolStripStatusLabel LblStatus;
        private System.Windows.Forms.ToolStripMenuItem itemMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem grnMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem budgetMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem userMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem logoutMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem windowMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem cascadeMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem horizontalMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem verticalMenuStripItem;
        public System.Windows.Forms.Label lblDetails;
        private System.Windows.Forms.ToolStripMenuItem RFQMenuStripItem;
    }
}