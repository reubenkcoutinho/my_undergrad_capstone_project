﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class Budget : Form
    {
        string usrDetails, usrName, usrCode;
        string query = "select * from BudgetMaster";
        string queryc = "select * from CostCenterMaster";
        string table = "BudgetMaster";
        string ctable = "CostCenterMaster";
        string msg,msg2;
        int val,val1,val2;
        DataSet budDataset = new DataSet();
        DataSet ccDataset = new DataSet();
        DbOperations dbo = new DbOperations();
        public Budget()
        {
            InitializeComponent();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //query = "select description,unit_of_measure from ItemMaster";

            budDataset = dbo.Select(query, table);

            DataColumn[] keys = new DataColumn[1];
            keys[0] = budDataset.Tables["BudgetMaster"].Columns["budgetcode"];
            budDataset.Tables["BudgetMaster"].PrimaryKey = keys;

            DataRow findRow = budDataset.Tables["BudgetMaster"].Rows.Find(textBox1.Text);

            if (findRow == null)
            {

                DataRow thisRow = budDataset.Tables["BudgetMaster"].NewRow();
                thisRow["budgetcode"] = textBox1.Text;
                thisRow["description"] = textBox2.Text;
                //thisRow["from_period"] = textBox3.Text;
                //thisRow["to_period"] = textBox4.Text;
                //thisRow["amount"] = textBox5.Text;
                budDataset.Tables[table].Rows.Add(thisRow);
                msg = dbo.Update(budDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("Item already exists", "ERP-PIMS");
            }
            Budget_Load(sender, e);

        }


        private void button2_Click(object sender, EventArgs e)
        {
            menu menufrm = new menu();
            menufrm.usrDetails = this.usrDetails;
            menufrm.usrCode = this.usrCode;
            menufrm.usrName = this.usrName;
            this.Hide();
            menufrm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //budDataset = dbo.Select("SELECT * FROM BudgetMaster where budgetcode=" + textBox12.Text, table);
            //dataGridView1.DataSource = budDataset.Tables[table];
        }

        private void Budget_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.CostCenterMaster' table. You can move, or remove it, as needed.
            this.costCenterMasterTableAdapter.Fill(this.rELERPDBDataSet.CostCenterMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.BudgetMaster' table. You can move, or remove it, as needed.
            this.budgetMasterTableAdapter.Fill(this.rELERPDBDataSet.BudgetMaster);
            //this.lblDetails.Text += this.usrName;
            //this.lblStatus.Text += this.usrDetails;
            //this.lblUsercode.Text = this.usrCode;
            budDataset = dbo.Select(query, table);
            ccDataset = dbo.Select(queryc, ctable);

            foreach (DataRow theRow in budDataset.Tables[table].Rows)
            {

                msg = theRow["budgetcode"].ToString();
                val1 = Convert.ToInt16(msg);

            }
            textBox26.Text = (++val1).ToString();

            foreach (DataRow therow in ccDataset.Tables[ctable].Rows)
            {
                msg2 = therow["cc_code"].ToString();
                val2 = Convert.ToInt16(msg2);
            }
            textBox10.Text = (++val2).ToString();
            //textBox2.Text = textBox3.Text = "";

        }

        private void button7_Click(object sender, EventArgs e)
        {

            try
            {

                budDataset = dbo.Select(query, table);

                DataColumn[] keys = new DataColumn[1];
                keys[0] = budDataset.Tables[table].Columns["budgetcode"];
                budDataset.Tables[table].PrimaryKey = keys;

                DataRow findRow = budDataset.Tables[table].Rows.Find(textBox26.Text);

                if (findRow == null)
                {

                    DataRow thisRow = budDataset.Tables[table].NewRow();
                    thisRow["budgetcode"] = textBox26.Text;
                    thisRow["description"] = textBox25.Text;
                    thisRow["from_period"] = dateTimePicker1.Value.Year.ToString();
                    thisRow["to_period"] = dateTimePicker2.Value.Year.ToString();
                    thisRow["amount"] = textBox22.Text;

                    budDataset.Tables[table].Rows.Add(thisRow);
                    msg = dbo.Update(budDataset, query, table);
                    MessageBox.Show(msg, "ERP-PIMS");


                }
                else
                {
                    MessageBox.Show("Budget details already added", "ERP-PIMS");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            finally
            {
                Budget_Load(sender, e);
            }
         
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            quotations abc = new quotations();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PO abc = new PO();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel16_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel15_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {

            try
            {

                ccDataset = dbo.Select(queryc, ctable);

                DataColumn[] keys = new DataColumn[1];
                keys[0] = ccDataset.Tables[ctable].Columns["cc_code"];
                ccDataset.Tables[ctable].PrimaryKey = keys;

                DataRow findRow = ccDataset.Tables[ctable].Rows.Find(textBox10.Text);

                if (findRow == null)
                {

                    DataRow thisRow = ccDataset.Tables[ctable].NewRow();
                    thisRow["cc_code"] = textBox10.Text;
                    thisRow["description"] = textBox8.Text;
                    thisRow["from_period"] = dateTimePicker3.Value.Year.ToString();
                    thisRow["to_period"] = dateTimePicker4.Value.Year.ToString();
                    thisRow["amount"] = textBox9.Text;

                    ccDataset.Tables[ctable].Rows.Add(thisRow);
                    msg = dbo.Update(ccDataset, queryc, ctable);
                    MessageBox.Show(msg, "ERP-PIMS");






                }
                else
                {
                    MessageBox.Show("Cost Center details already added", "ERP-PIMS");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            finally
            {
                Budget_Load(sender, e);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                costCenterMasterDataGridView.Visible = false;
                budgetMasterDataGridView.Visible = true;
                budgetMasterDataGridView.Size = new System.Drawing.Size(563, 373);
                this.budgetMasterTableAdapter.FillBy(this.rELERPDBDataSet.BudgetMaster, Convert.ToInt16(textBox78.Text));
                //.Fill();
            }
            catch (Exception exc)
            { }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void budgetMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.budgetMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                budgetMasterDataGridView.Visible = false;
                costCenterMasterDataGridView.Visible = true;
                costCenterMasterDataGridView.Size = new System.Drawing.Size(563, 373);
                this.costCenterMasterDataGridView.Location = new System.Drawing.Point(3, 129);
                this.costCenterMasterTableAdapter.FillBy(this.rELERPDBDataSet.CostCenterMaster, Convert.ToInt16(textBox77.Text));
            }
            catch (Exception ev)
            { }
        }

        private void textBox26_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void budgetMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataRowView myDataRowView = (DataRowView)budgetMasterDataGridView.SelectedRows[0].DataBoundItem;
            RELERPDBDataSet.BudgetMasterRow thisRow = (RELERPDBDataSet.BudgetMasterRow)myDataRowView.Row;
            int myPosition = rELERPDBDataSet.BudgetMaster.Rows.IndexOf(thisRow);

            BudgetDetails frm = new BudgetDetails(rELERPDBDataSet, myPosition);
            frm.ShowDialog();
        }

        private void costCenterMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataRowView myDataRowView = (DataRowView)costCenterMasterDataGridView.SelectedRows[0].DataBoundItem;
            RELERPDBDataSet.CostCenterMasterRow thisRow = (RELERPDBDataSet.CostCenterMasterRow)myDataRowView.Row;
            int myPosition = rELERPDBDataSet.CostCenterMaster.Rows.IndexOf(thisRow);

            CostCenterDetails frm = new CostCenterDetails(rELERPDBDataSet, myPosition);
            frm.ShowDialog();
        }

        private void tabPage4_Enter(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.CostCenterMaster' table. You can move, or remove it, as needed.
            this.costCenterMasterTableAdapter.Fill(this.rELERPDBDataSet.CostCenterMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.BudgetMaster' table. You can move, or remove it, as needed.
            this.budgetMasterTableAdapter.Fill(this.rELERPDBDataSet.BudgetMaster);
            this.costCenterMasterDataGridView.Location = new System.Drawing.Point(3, 321);
            this.costCenterMasterDataGridView.Visible = true;
            this.costCenterMasterDataGridView.Size = new System.Drawing.Size(562, 181);
            this.budgetMasterDataGridView.Location = new System.Drawing.Point(3, 129);
            this.budgetMasterDataGridView.Visible = true;
            this.budgetMasterDataGridView.Size = new System.Drawing.Size(563, 167);
            textBox77.Text = textBox78.Text = "";
        }

     
       
    }
}
