﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class GRVDetails : Form
    {
        public GRVDetails(RELERPDBDataSet reds, int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.gRVMasterBindingSource.DataSource = this.rELERPDBDataSet;
            this.gRVMasterBindingSource.Position = position;
        }

        //private void gRVMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        //{
        //    this.Validate();
        //    this.gRVMasterBindingSource.EndEdit();
        //    this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        //}

        private void GRVDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.GRVMaster' table. You can move, or remove it, as needed.
            //this.gRVMasterTableAdapter.Fill(this.rELERPDBDataSet.GRVMaster);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.gRVMasterBindingSource.EndEdit();

                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("GRV details updated.", "ERP-PIMS");

                }
                else
                {
                    MessageBox.Show("GRV details not updated.", "ERP-PIMS");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }
        }
    }
}
