﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class user : Form
    {
        string usrDetails, usrName, usrCode;
        string msg;
        int val;
        string query = "select * from UserMaster";
        string table = "UserMaster";
        DbOperations dbo = new DbOperations();
        DataSet usrDataset = new DataSet();
        public user()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            menu menufrm = new menu();
            menufrm.usrDetails = this.usrDetails;
            menufrm.usrCode = this.usrCode;
            menufrm.usrName = this.usrName;
            this.Hide();
            menufrm.Show();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            usrDataset = dbo.Select(query, table);

            DataColumn[] keys = new DataColumn[1];
            keys[0] = usrDataset.Tables["UserMaster"].Columns["usercode"];
            usrDataset.Tables["UserMaster"].PrimaryKey = keys;

            DataRow findRow = usrDataset.Tables["UserMaster"].Rows.Find(textBox1.Text);

            if (findRow == null)
            {

                DataRow thisRow = usrDataset.Tables["UserMaster"].NewRow();
                thisRow["usercode"] = textBox1.Text;
                thisRow["name"] = textBox2.Text;
                thisRow["username"] = textBox3.Text;
                thisRow["password"] = textBox4.Text;
                thisRow["details"] = comboBox1.Text;
                usrDataset.Tables["UserMaster"].Rows.Add(thisRow);
                msg = dbo.Update(usrDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("User already exists", "ERP-PIMS");
            }
            user_Load(sender, e);
        }

        private void user_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.UserMaster' table. You can move, or remove it, as needed.
            this.userMasterTableAdapter.Fill(this.rELERPDBDataSet.UserMaster);
            usrDataset = dbo.Select(query, table);
            //dataGridView1.DataSource = usrDataset.Tables[table];
            //this.lblDetails.Text = "Hello " + this.usrName;
            //this.lblStatus.Text = "You are logged in as " + this.usrDetails;
            this.lblUsercode.Text = this.usrCode;
            foreach (DataRow theRow in usrDataset.Tables[table].Rows)
            {

                msg = theRow["usercode"].ToString();
                val = Convert.ToInt16(msg);
            }
            textBox1.Text = " " + ++val;
            textBox2.Text = textBox3.Text = textBox4.Text = comboBox1.Text = "";
            button6.Enabled = button2.Enabled = false;
            button1.Enabled = true;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            textBox10.Enabled = true;
            textBox11.Enabled = false;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            textBox11.Enabled = true;
            textBox10.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            usrDataset = dbo.Select(query, table);
            DataColumn[] keys = new DataColumn[1];
            keys[0] = usrDataset.Tables["UserMaster"].Columns["usercode"];
            usrDataset.Tables["UserMaster"].PrimaryKey = keys;

            DataRow findRow = usrDataset.Tables["UserMaster"].Rows.Find(textBox1.Text);

            if (findRow == null)
            {
                MessageBox.Show("User doesn't exists.Please add.", "ERP-PIMS");
            }
            else
            {

                findRow["usercode"] = textBox1.Text;
                findRow["name"] = textBox2.Text;
                findRow["username"] = textBox3.Text;
                findRow["password"] = textBox4.Text;
                findRow["details"] = comboBox1.Text;
                msg = dbo.Update(usrDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }

            user_Load(sender, e);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //textBox1.Text = dataGridView1.CurrentRow.Cells["usercode"].Value.ToString();
            //textBox2.Text = dataGridView1.CurrentRow.Cells["name"].Value.ToString();
            //textBox3.Text = dataGridView1.CurrentRow.Cells["username"].Value.ToString();
            //textBox4.Text = dataGridView1.CurrentRow.Cells["password"].Value.ToString();
            //comboBox1.Text = dataGridView1.CurrentRow.Cells["details"].Value.ToString();
            //button1.Enabled = false;
            //button6.Enabled = button2.Enabled = true;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButton3.Checked)
                {
                    this.userMasterTableAdapter.FillByUsercode(this.rELERPDBDataSet.UserMaster, ((int)(System.Convert.ChangeType(textBox10.Text, typeof(int)))));
                }
                else
                {
                    this.userMasterTableAdapter.FillByName(this.rELERPDBDataSet.UserMaster, textBox11.Text);
                }
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            user_Load(sender, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            usrDataset = dbo.Select(query, table);
            DataColumn[] keys = new DataColumn[1];
            keys[0] = usrDataset.Tables["UserMaster"].Columns["usercode"];
            usrDataset.Tables["UserMaster"].PrimaryKey = keys;

            DataRow findRow = usrDataset.Tables["UserMaster"].Rows.Find(textBox1.Text);

            if (findRow == null)
            {
                MessageBox.Show("User doesn't exists.Cannot delete.", "ERP-PIMS");
            }
            else
            {

                findRow.Delete();
                msg = dbo.Update(usrDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }

            user_Load(sender, e);
        }

        private void userMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.userMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void userMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataRowView myDataRowView = (DataRowView)userMasterDataGridView.SelectedRows[0].DataBoundItem;
            RELERPDBDataSet.UserMasterRow thisRow = (RELERPDBDataSet.UserMasterRow)myDataRowView.Row;
            int myPosition = rELERPDBDataSet.UserMaster.Rows.IndexOf(thisRow);
            UserDetails frm = new UserDetails(rELERPDBDataSet, myPosition);
            frm.ShowDialog();
        }

        private void radioButton4_CheckedChanged_1(object sender, EventArgs e)
        {
            textBox10.Enabled = false;
            textBox11.Enabled = true;
        }

        private void radioButton3_CheckedChanged_1(object sender, EventArgs e)
        {
            textBox10.Enabled = !false;
            textBox11.Enabled = !true;
        }

    }
}
