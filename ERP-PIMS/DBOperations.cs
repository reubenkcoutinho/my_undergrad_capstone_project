﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using ERP_PIMS.Properties;

namespace ERP_PIMS
{
    class DbOperations
    {
        // Specify SQL Server-specific connection string
        SqlConnection thisConnection = new SqlConnection(Settings.Default.RELERPDBConnectionString); 
        //= new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename='E:\Project\final year project\RELERPDB.mdf';Integrated Security=True;Connect Timeout=30;User Instance=True");

        SqlDataAdapter thisAdapter;

        SqlCommandBuilder thisBuilder;

        public DataSet Select(string query, string tablename)
        {
            try
            {
                // Create DataAdapter object
                thisAdapter = new SqlDataAdapter(query, thisConnection);

                // Create DataSet to contain related data tables, rows, and columns
                DataSet thisDataset = new DataSet();

                // Fill DataSet using query from parameter for DataAdapter
                thisAdapter.Fill(thisDataset, tablename);

                //Close Connection
                thisConnection.Close();

                return thisDataset;


            }
            catch (Exception exp)
            {
                return null;
            }
        }

        public string Update(DataSet queryDataset, string query, string tablename)
        {
            try
            {
                // Create DataAdapter object
                thisAdapter = new SqlDataAdapter(query, thisConnection);

                thisBuilder = new SqlCommandBuilder(thisAdapter);

                thisAdapter.Update(queryDataset, tablename);

                return "Operation Successful";
            }
            catch (Exception exp)
            {
                return "Operation Failed!!\n" + exp.Message;
            }
        }

        public void openConnection()
        {
            if (this.thisConnection.State == ConnectionState.Closed)
                this.thisConnection.Open();
        }

        public void closeConnection()
        {
            if (this.thisConnection.State == ConnectionState.Open)
                this.thisConnection.Close();
        }

        public SqlDataReader SelectDR(string query)
        {
            SqlDataReader dReader;
            try
            {

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = thisConnection;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                openConnection();
                dReader = cmd.ExecuteReader();
                //dReader.Close();
            }
            catch (Exception e)
            {
                dReader = null;
            }
            return dReader;
        }


    }
}
