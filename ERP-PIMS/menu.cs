﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ERP_PIMS
{
    public partial class menu : Form
    {
        bool[] check = new bool[10];
        public string usrDetails, usrName, usrCode;
        //int count = 0;
        
        public menu()
        {
            InitializeComponent();
        }

        

        private void menu_Load(object sender, EventArgs e)
        {
            if (usrDetails == "employee")
            {
                adminMenuStripItem.Visible = false;
                goodMenuStripItem.Visible = false;
                issueMenuStripItem.Enabled = false;
                stockMenuStripItem.Enabled = false;
                poMenuStripItem.Enabled = false;
                budgetMenuStripItem.Enabled = false;
                RFQMenuStripItem.Enabled = false;
                qtsMenuStripItem.Enabled = false;
                
            }
            if (usrDetails == "hod")
            {
                adminMenuStripItem.Visible = false;
            }
            //this.lblDetails.Text = ;
            this.LblStatus.Text ="Hello " + this.usrName + ".You are Logged in as " + this.usrDetails;
            this.LblUsercode.Text = this.usrCode;

        }

        private void issueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            issue isu = new issue();
            isu.setInfo(this.usrName, this.usrDetails, this.usrCode);
            isu.MdiParent = this;
            isu.Show();
           
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login lg = new Login();
            lg.Show();
            this.Hide();
        }

        private void raisePRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PR prfrm = new PR();
            prfrm.setInfo(this.usrName, this.usrDetails, this.usrCode);
            prfrm.MdiParent = this;
            prfrm.Show();
            
        }

        private void qoutationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            quotations qout = new quotations();
            qout.setInfo(this.usrName, this.usrDetails, this.usrCode);
            qout.MdiParent = this;
            qout.Show();
            
        }

        private void purchaseOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PO pofrm = new PO();
            pofrm.setInfo(this.usrName, this.usrDetails, this.usrCode);
            pofrm.MdiParent = this;
            pofrm.Show();
           
        }

        private void budgetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Budget bgt = new Budget();
            bgt.setInfo(this.usrName, this.usrDetails, this.usrCode);
            bgt.MdiParent = this;
            bgt.Show();
            
        }

     
        private void requestMaterialsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MReq mrq = new MReq();
            mrq.setInfo(this.usrName, this.usrDetails, this.usrCode);
            mrq.MdiParent = this;
            mrq.Show();
          
        }

        private void stockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stock stk = new Stock();
            stk.setInfo(this.usrName, this.usrDetails, this.usrCode);
            stk.MdiParent = this;
            stk.Show();
           
        }

        private void itemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Item itm = new Item();
            itm.setInfo(this.usrName, this.usrDetails, this.usrCode);
            itm.MdiParent = this;
            itm.Show();
           
        }

        private void goodsReciptNoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GOODS grnfrm = new GOODS();
            grnfrm.setInfo(this.usrName, this.usrDetails, this.usrCode);
            grnfrm.MdiParent = this;
            grnfrm.Show();
            
           
        }

        private void goodsReturnedToVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //GRV grvfrm = new GRV();
            //grvfrm.setInfo(this.usrName, this.usrDetails, this.usrCode);
            //grvfrm.MdiParent = this;
            //grvfrm.Show();
            
        }

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {
            user usr = new user();
            usr.setInfo(this.usrName, this.usrDetails, this.usrCode);
            usr.MdiParent = this;
            usr.Show();
           
        }

        private void cascadeMenuStripItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void horizontalMenuStripItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalMenuStripItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void RFQMenuStripItem_Click(object sender, EventArgs e)
        {
            RFQ rfqFrm = new RFQ();
            rfqFrm.setInfo(this.usrName, this.usrDetails, this.usrCode);
            rfqFrm.Show();
            rfqFrm.MdiParent = this;

            //GOODS grnfrm = new GOODS();
            //grnfrm
            //grnfrm.MdiParent = this;
            //grnfrm.Show();
        }

        private void menu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
