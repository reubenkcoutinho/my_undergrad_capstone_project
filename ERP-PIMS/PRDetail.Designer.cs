﻿namespace ERP_PIMS
{
    partial class PRDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label prnoLabel;
            System.Windows.Forms.Label datereqLabel;
            System.Windows.Forms.Label itemcodeLabel;
            System.Windows.Forms.Label quantityLabel;
            System.Windows.Forms.Label rateLabel;
            System.Windows.Forms.Label usercodeLabel;
            System.Windows.Forms.Label statusLabel;
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.pRMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pRMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.PRMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.prnoTextBox = new System.Windows.Forms.TextBox();
            this.datereqDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.quantityTextBox = new System.Windows.Forms.TextBox();
            this.rateTextBox = new System.Windows.Forms.TextBox();
            this.usercodeTextBox = new System.Windows.Forms.TextBox();
            this.statusComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            prnoLabel = new System.Windows.Forms.Label();
            datereqLabel = new System.Windows.Forms.Label();
            itemcodeLabel = new System.Windows.Forms.Label();
            quantityLabel = new System.Windows.Forms.Label();
            rateLabel = new System.Windows.Forms.Label();
            usercodeLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pRMasterBindingSource
            // 
            this.pRMasterBindingSource.DataMember = "PRMaster";
            this.pRMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // pRMasterTableAdapter
            // 
            this.pRMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = null;
            this.tableAdapterManager.GRVMasterTableAdapter = null;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = null;
            this.tableAdapterManager.PRMasterTableAdapter = this.pRMasterTableAdapter;
            this.tableAdapterManager.QuotationMasterTableAdapter = null;
            this.tableAdapterManager.RFQMasterTableAdapter = null;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // prnoLabel
            // 
            prnoLabel.AutoSize = true;
            prnoLabel.Location = new System.Drawing.Point(24, 50);
            prnoLabel.Name = "prnoLabel";
            prnoLabel.Size = new System.Drawing.Size(31, 13);
            prnoLabel.TabIndex = 1;
            prnoLabel.Text = "prno:";
            // 
            // prnoTextBox
            // 
            this.prnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "prno", true));
            this.prnoTextBox.Location = new System.Drawing.Point(84, 47);
            this.prnoTextBox.Name = "prnoTextBox";
            this.prnoTextBox.Size = new System.Drawing.Size(135, 20);
            this.prnoTextBox.TabIndex = 2;
            // 
            // datereqLabel
            // 
            datereqLabel.AutoSize = true;
            datereqLabel.Location = new System.Drawing.Point(24, 77);
            datereqLabel.Name = "datereqLabel";
            datereqLabel.Size = new System.Drawing.Size(46, 13);
            datereqLabel.TabIndex = 3;
            datereqLabel.Text = "datereq:";
            // 
            // datereqDateTimePicker
            // 
            this.datereqDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.pRMasterBindingSource, "datereq", true));
            this.datereqDateTimePicker.Location = new System.Drawing.Point(84, 73);
            this.datereqDateTimePicker.Name = "datereqDateTimePicker";
            this.datereqDateTimePicker.Size = new System.Drawing.Size(135, 20);
            this.datereqDateTimePicker.TabIndex = 4;
            // 
            // itemcodeLabel
            // 
            itemcodeLabel.AutoSize = true;
            itemcodeLabel.Location = new System.Drawing.Point(24, 102);
            itemcodeLabel.Name = "itemcodeLabel";
            itemcodeLabel.Size = new System.Drawing.Size(53, 13);
            itemcodeLabel.TabIndex = 5;
            itemcodeLabel.Text = "itemcode:";
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(84, 99);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.Size = new System.Drawing.Size(135, 20);
            this.itemcodeTextBox.TabIndex = 6;
            // 
            // quantityLabel
            // 
            quantityLabel.AutoSize = true;
            quantityLabel.Location = new System.Drawing.Point(24, 128);
            quantityLabel.Name = "quantityLabel";
            quantityLabel.Size = new System.Drawing.Size(47, 13);
            quantityLabel.TabIndex = 7;
            quantityLabel.Text = "quantity:";
            // 
            // quantityTextBox
            // 
            this.quantityTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "quantity", true));
            this.quantityTextBox.Location = new System.Drawing.Point(84, 125);
            this.quantityTextBox.Name = "quantityTextBox";
            this.quantityTextBox.Size = new System.Drawing.Size(135, 20);
            this.quantityTextBox.TabIndex = 8;
            // 
            // rateLabel
            // 
            rateLabel.AutoSize = true;
            rateLabel.Location = new System.Drawing.Point(24, 154);
            rateLabel.Name = "rateLabel";
            rateLabel.Size = new System.Drawing.Size(28, 13);
            rateLabel.TabIndex = 9;
            rateLabel.Text = "rate:";
            // 
            // rateTextBox
            // 
            this.rateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "rate", true));
            this.rateTextBox.Location = new System.Drawing.Point(84, 151);
            this.rateTextBox.Name = "rateTextBox";
            this.rateTextBox.Size = new System.Drawing.Size(135, 20);
            this.rateTextBox.TabIndex = 10;
            // 
            // usercodeLabel
            // 
            usercodeLabel.AutoSize = true;
            usercodeLabel.Location = new System.Drawing.Point(24, 180);
            usercodeLabel.Name = "usercodeLabel";
            usercodeLabel.Size = new System.Drawing.Size(54, 13);
            usercodeLabel.TabIndex = 11;
            usercodeLabel.Text = "usercode:";
            // 
            // usercodeTextBox
            // 
            this.usercodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "usercode", true));
            this.usercodeTextBox.Location = new System.Drawing.Point(84, 177);
            this.usercodeTextBox.Name = "usercodeTextBox";
            this.usercodeTextBox.Size = new System.Drawing.Size(135, 20);
            this.usercodeTextBox.TabIndex = 12;
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Location = new System.Drawing.Point(24, 206);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(38, 13);
            statusLabel.TabIndex = 13;
            statusLabel.Text = "status:";
            // 
            // statusComboBox
            // 
            this.statusComboBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pRMasterBindingSource, "status", true));
            this.statusComboBox.FormattingEnabled = true;
            this.statusComboBox.Items.AddRange(new object[] {
            "Fresh",
            "Authorize",
            "Cancelled"});
            this.statusComboBox.Location = new System.Drawing.Point(84, 203);
            this.statusComboBox.Name = "statusComboBox";
            this.statusComboBox.Size = new System.Drawing.Size(135, 21);
            this.statusComboBox.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 314);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(155, 314);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 16;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // PRDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 370);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(prnoLabel);
            this.Controls.Add(this.prnoTextBox);
            this.Controls.Add(datereqLabel);
            this.Controls.Add(this.datereqDateTimePicker);
            this.Controls.Add(itemcodeLabel);
            this.Controls.Add(this.itemcodeTextBox);
            this.Controls.Add(quantityLabel);
            this.Controls.Add(this.quantityTextBox);
            this.Controls.Add(rateLabel);
            this.Controls.Add(this.rateTextBox);
            this.Controls.Add(usercodeLabel);
            this.Controls.Add(this.usercodeTextBox);
            this.Controls.Add(statusLabel);
            this.Controls.Add(this.statusComboBox);
            this.Name = "PRDetail";
            this.Text = "PRDetail";
            this.Load += new System.EventHandler(this.PRDetail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pRMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource pRMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.PRMasterTableAdapter pRMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox prnoTextBox;
        private System.Windows.Forms.DateTimePicker datereqDateTimePicker;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private System.Windows.Forms.TextBox quantityTextBox;
        private System.Windows.Forms.TextBox rateTextBox;
        private System.Windows.Forms.TextBox usercodeTextBox;
        private System.Windows.Forms.ComboBox statusComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}