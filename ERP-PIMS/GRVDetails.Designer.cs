﻿namespace ERP_PIMS
{
    partial class GRVDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label grvnoLabel;
            System.Windows.Forms.Label po_noLabel;
            System.Windows.Forms.Label grnoLabel;
            System.Windows.Forms.Label itemcodeLabel;
            System.Windows.Forms.Label qtyrjctdLabel;
            System.Windows.Forms.Label reasonLabel;
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.gRVMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gRVMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.GRVMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.grvnoTextBox = new System.Windows.Forms.TextBox();
            this.po_noTextBox = new System.Windows.Forms.TextBox();
            this.grnoTextBox = new System.Windows.Forms.TextBox();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.qtyrjctdTextBox = new System.Windows.Forms.TextBox();
            this.reasonTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            grvnoLabel = new System.Windows.Forms.Label();
            po_noLabel = new System.Windows.Forms.Label();
            grnoLabel = new System.Windows.Forms.Label();
            itemcodeLabel = new System.Windows.Forms.Label();
            qtyrjctdLabel = new System.Windows.Forms.Label();
            reasonLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRVMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // grvnoLabel
            // 
            grvnoLabel.AutoSize = true;
            grvnoLabel.Location = new System.Drawing.Point(38, 28);
            grvnoLabel.Name = "grvnoLabel";
            grvnoLabel.Size = new System.Drawing.Size(37, 13);
            grvnoLabel.TabIndex = 1;
            grvnoLabel.Text = "grvno:";
            // 
            // po_noLabel
            // 
            po_noLabel.AutoSize = true;
            po_noLabel.Location = new System.Drawing.Point(38, 54);
            po_noLabel.Name = "po_noLabel";
            po_noLabel.Size = new System.Drawing.Size(37, 13);
            po_noLabel.TabIndex = 3;
            po_noLabel.Text = "po no:";
            // 
            // grnoLabel
            // 
            grnoLabel.AutoSize = true;
            grnoLabel.Location = new System.Drawing.Point(38, 80);
            grnoLabel.Name = "grnoLabel";
            grnoLabel.Size = new System.Drawing.Size(31, 13);
            grnoLabel.TabIndex = 5;
            grnoLabel.Text = "grno:";
            // 
            // itemcodeLabel
            // 
            itemcodeLabel.AutoSize = true;
            itemcodeLabel.Location = new System.Drawing.Point(38, 106);
            itemcodeLabel.Name = "itemcodeLabel";
            itemcodeLabel.Size = new System.Drawing.Size(53, 13);
            itemcodeLabel.TabIndex = 7;
            itemcodeLabel.Text = "itemcode:";
            // 
            // qtyrjctdLabel
            // 
            qtyrjctdLabel.AutoSize = true;
            qtyrjctdLabel.Location = new System.Drawing.Point(38, 132);
            qtyrjctdLabel.Name = "qtyrjctdLabel";
            qtyrjctdLabel.Size = new System.Drawing.Size(44, 13);
            qtyrjctdLabel.TabIndex = 9;
            qtyrjctdLabel.Text = "qtyrjctd:";
            // 
            // reasonLabel
            // 
            reasonLabel.AutoSize = true;
            reasonLabel.Location = new System.Drawing.Point(38, 158);
            reasonLabel.Name = "reasonLabel";
            reasonLabel.Size = new System.Drawing.Size(42, 13);
            reasonLabel.TabIndex = 11;
            reasonLabel.Text = "reason:";
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gRVMasterBindingSource
            // 
            this.gRVMasterBindingSource.DataMember = "GRVMaster";
            this.gRVMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // gRVMasterTableAdapter
            // 
            this.gRVMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = null;
            this.tableAdapterManager.GRVMasterTableAdapter = this.gRVMasterTableAdapter;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = null;
            this.tableAdapterManager.PRMasterTableAdapter = null;
            this.tableAdapterManager.QuotationMasterTableAdapter = null;
            this.tableAdapterManager.RFQMasterTableAdapter = null;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // grvnoTextBox
            // 
            this.grvnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRVMasterBindingSource, "grvno", true));
            this.grvnoTextBox.Location = new System.Drawing.Point(97, 25);
            this.grvnoTextBox.Name = "grvnoTextBox";
            this.grvnoTextBox.Size = new System.Drawing.Size(100, 20);
            this.grvnoTextBox.TabIndex = 2;
            // 
            // po_noTextBox
            // 
            this.po_noTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRVMasterBindingSource, "po_no", true));
            this.po_noTextBox.Location = new System.Drawing.Point(97, 51);
            this.po_noTextBox.Name = "po_noTextBox";
            this.po_noTextBox.Size = new System.Drawing.Size(100, 20);
            this.po_noTextBox.TabIndex = 4;
            // 
            // grnoTextBox
            // 
            this.grnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRVMasterBindingSource, "grno", true));
            this.grnoTextBox.Location = new System.Drawing.Point(97, 77);
            this.grnoTextBox.Name = "grnoTextBox";
            this.grnoTextBox.Size = new System.Drawing.Size(100, 20);
            this.grnoTextBox.TabIndex = 6;
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRVMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(97, 103);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.itemcodeTextBox.TabIndex = 8;
            // 
            // qtyrjctdTextBox
            // 
            this.qtyrjctdTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRVMasterBindingSource, "qtyrjctd", true));
            this.qtyrjctdTextBox.Location = new System.Drawing.Point(97, 129);
            this.qtyrjctdTextBox.Name = "qtyrjctdTextBox";
            this.qtyrjctdTextBox.Size = new System.Drawing.Size(100, 20);
            this.qtyrjctdTextBox.TabIndex = 10;
            // 
            // reasonTextBox
            // 
            this.reasonTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.gRVMasterBindingSource, "reason", true));
            this.reasonTextBox.Location = new System.Drawing.Point(97, 155);
            this.reasonTextBox.Name = "reasonTextBox";
            this.reasonTextBox.Size = new System.Drawing.Size(100, 20);
            this.reasonTextBox.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 229);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(197, 229);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 14;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // GRVDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 264);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(grvnoLabel);
            this.Controls.Add(this.grvnoTextBox);
            this.Controls.Add(po_noLabel);
            this.Controls.Add(this.po_noTextBox);
            this.Controls.Add(grnoLabel);
            this.Controls.Add(this.grnoTextBox);
            this.Controls.Add(itemcodeLabel);
            this.Controls.Add(this.itemcodeTextBox);
            this.Controls.Add(qtyrjctdLabel);
            this.Controls.Add(this.qtyrjctdTextBox);
            this.Controls.Add(reasonLabel);
            this.Controls.Add(this.reasonTextBox);
            this.Name = "GRVDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GRVDetails";
            this.Load += new System.EventHandler(this.GRVDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gRVMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource gRVMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.GRVMasterTableAdapter gRVMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox grvnoTextBox;
        private System.Windows.Forms.TextBox po_noTextBox;
        private System.Windows.Forms.TextBox grnoTextBox;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private System.Windows.Forms.TextBox qtyrjctdTextBox;
        private System.Windows.Forms.TextBox reasonTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}