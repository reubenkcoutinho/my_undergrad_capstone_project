﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class ItemDetails : Form
    {
        public ItemDetails(RELERPDBDataSet reds, int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.itemMasterBindingSource.DataSource = this.rELERPDBDataSet;
            this.itemMasterBindingSource.Position = position;
        }

        private void itemMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.itemMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void ItemDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.ItemMaster' table. You can move, or remove it, as needed.
            //this.itemMasterTableAdapter.Fill(this.rELERPDBDataSet.ItemMaster);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.itemMasterBindingSource.EndEdit();
                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("Item details updated.", "ERP-PIMS");
                }
                else
                {
                    MessageBox.Show("Item details not updated.", "ERP-PIMS");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }
            this.Close();
        }
    }
}
