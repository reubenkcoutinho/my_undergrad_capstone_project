﻿namespace ERP_PIMS
{
    partial class BudgetDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label budgetcodeLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label from_periodLabel;
            System.Windows.Forms.Label to_periodLabel;
            System.Windows.Forms.Label amountLabel;
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.budgetMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.budgetMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.BudgetMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.budgetcodeTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.from_periodTextBox = new System.Windows.Forms.TextBox();
            this.to_periodTextBox = new System.Windows.Forms.TextBox();
            this.amountTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            budgetcodeLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            from_periodLabel = new System.Windows.Forms.Label();
            to_periodLabel = new System.Windows.Forms.Label();
            amountLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.budgetMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // budgetcodeLabel
            // 
            budgetcodeLabel.AutoSize = true;
            budgetcodeLabel.Location = new System.Drawing.Point(62, 75);
            budgetcodeLabel.Name = "budgetcodeLabel";
            budgetcodeLabel.Size = new System.Drawing.Size(67, 13);
            budgetcodeLabel.TabIndex = 1;
            budgetcodeLabel.Text = "budgetcode:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(62, 101);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(61, 13);
            descriptionLabel.TabIndex = 3;
            descriptionLabel.Text = "description:";
            // 
            // from_periodLabel
            // 
            from_periodLabel.AutoSize = true;
            from_periodLabel.Location = new System.Drawing.Point(62, 127);
            from_periodLabel.Name = "from_periodLabel";
            from_periodLabel.Size = new System.Drawing.Size(62, 13);
            from_periodLabel.TabIndex = 5;
            from_periodLabel.Text = "from period:";
            // 
            // to_periodLabel
            // 
            to_periodLabel.AutoSize = true;
            to_periodLabel.Location = new System.Drawing.Point(62, 153);
            to_periodLabel.Name = "to_periodLabel";
            to_periodLabel.Size = new System.Drawing.Size(51, 13);
            to_periodLabel.TabIndex = 7;
            to_periodLabel.Text = "to period:";
            // 
            // amountLabel
            // 
            amountLabel.AutoSize = true;
            amountLabel.Location = new System.Drawing.Point(62, 179);
            amountLabel.Name = "amountLabel";
            amountLabel.Size = new System.Drawing.Size(45, 13);
            amountLabel.TabIndex = 9;
            amountLabel.Text = "amount:";
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // budgetMasterBindingSource
            // 
            this.budgetMasterBindingSource.DataMember = "BudgetMaster";
            this.budgetMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // budgetMasterTableAdapter
            // 
            this.budgetMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = this.budgetMasterTableAdapter;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = null;
            this.tableAdapterManager.GRVMasterTableAdapter = null;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = null;
            this.tableAdapterManager.PRMasterTableAdapter = null;
            this.tableAdapterManager.QuotationMasterTableAdapter = null;
            this.tableAdapterManager.RFQMasterTableAdapter = null;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // budgetcodeTextBox
            // 
            this.budgetcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.budgetMasterBindingSource, "budgetcode", true));
            this.budgetcodeTextBox.Location = new System.Drawing.Point(135, 72);
            this.budgetcodeTextBox.Name = "budgetcodeTextBox";
            this.budgetcodeTextBox.ReadOnly = true;
            this.budgetcodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.budgetcodeTextBox.TabIndex = 2;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.budgetMasterBindingSource, "description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(135, 98);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriptionTextBox.TabIndex = 4;
            // 
            // from_periodTextBox
            // 
            this.from_periodTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.budgetMasterBindingSource, "from_period", true));
            this.from_periodTextBox.Location = new System.Drawing.Point(135, 124);
            this.from_periodTextBox.Name = "from_periodTextBox";
            this.from_periodTextBox.ReadOnly = true;
            this.from_periodTextBox.Size = new System.Drawing.Size(100, 20);
            this.from_periodTextBox.TabIndex = 6;
            // 
            // to_periodTextBox
            // 
            this.to_periodTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.budgetMasterBindingSource, "to_period", true));
            this.to_periodTextBox.Location = new System.Drawing.Point(135, 150);
            this.to_periodTextBox.Name = "to_periodTextBox";
            this.to_periodTextBox.ReadOnly = true;
            this.to_periodTextBox.Size = new System.Drawing.Size(100, 20);
            this.to_periodTextBox.TabIndex = 8;
            // 
            // amountTextBox
            // 
            this.amountTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.budgetMasterBindingSource, "amount", true));
            this.amountTextBox.Location = new System.Drawing.Point(135, 176);
            this.amountTextBox.Name = "amountTextBox";
            this.amountTextBox.ReadOnly = true;
            this.amountTextBox.Size = new System.Drawing.Size(100, 20);
            this.amountTextBox.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 249);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(109, 249);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(208, 249);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 13;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 19);
            this.label1.TabIndex = 14;
            this.label1.Text = "Edit Budget Details";
            // 
            // BudgetDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(295, 284);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(budgetcodeLabel);
            this.Controls.Add(this.budgetcodeTextBox);
            this.Controls.Add(descriptionLabel);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(from_periodLabel);
            this.Controls.Add(this.from_periodTextBox);
            this.Controls.Add(to_periodLabel);
            this.Controls.Add(this.to_periodTextBox);
            this.Controls.Add(amountLabel);
            this.Controls.Add(this.amountTextBox);
            this.Name = "BudgetDetails";
            this.Text = "BudgetDetails";
            this.Load += new System.EventHandler(this.BudgetDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.budgetMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource budgetMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.BudgetMasterTableAdapter budgetMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox budgetcodeTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox from_periodTextBox;
        private System.Windows.Forms.TextBox to_periodTextBox;
        private System.Windows.Forms.TextBox amountTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
    }
}