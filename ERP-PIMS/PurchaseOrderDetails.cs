﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class PurchaseOrderDetails : Form
    {
        public PurchaseOrderDetails(RELERPDBDataSet reds, int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.pOMasterBindingSource .DataSource= this.rELERPDBDataSet;
            this.pOMasterBindingSource.Position = position;
        }

        private void pOMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.pOMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void PurchaseOrderDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.POMaster' table. You can move, or remove it, as needed.
            //this.pOMasterTableAdapter.Fill(this.rELERPDBDataSet.POMaster);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.pOMasterBindingSource.EndEdit();
                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("Purchase Order details updated.", "ERP-PIMS");
                }
                else
                {
                    MessageBox.Show("Purchase Order details not updated.", "ERP-PIMS");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.pOMasterBindingSource.RemoveCurrent();
                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("Purchase Order details deleted.", "ERP-PIMS");
                }
                else
                {
                    MessageBox.Show("Purchase Order details not deleted.", "ERP-PIMS");
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error occured" + exp.ToString(), "ERP-PIMS");
            }
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
