﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class Stock : Form
    {
        string usrDetails, usrName, usrCode;
        string msg;
        string query = "select * from Stockmaster";
        string table = "Stockmaster";
        //int val;
        DbOperations dbo = new DbOperations();
        DataSet stkDataset = new DataSet();
        public Stock()
        {
            InitializeComponent();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            menu menufrm = new menu();
            menufrm.usrDetails = this.usrDetails;
            menufrm.usrCode = this.usrCode;
            menufrm.usrName = this.usrName;
            this.Hide();
            menufrm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }

        private void Stock_Load(object sender, EventArgs e)
        {    
            //this.lblDetails.Text += this.usrName;
            //this.lblStatus.Text += this.usrDetails;
            this.lblUsercode.Text = this.usrCode;
            stkDataset = dbo.Select(query, table);
            //dataGridView1.DataSource = stkDataset.Tables[table];
            textBox1.Text = textBox2.Text = textBox3.Text = "";
           
            
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //query = "select description,unit_of_measure from ItemMaster";

            stkDataset = dbo.Select(query, table);

            DataColumn[] keys = new DataColumn[1];
            keys[0] = stkDataset.Tables["StockMaster"].Columns["itemcode"];
            stkDataset.Tables["StockMaster"].PrimaryKey = keys;

            DataRow findRow = stkDataset.Tables["StockMaster"].Rows.Find(textBox1.Text);

            if (findRow == null)
            {

                DataRow thisRow = stkDataset.Tables["StockMaster"].NewRow();
                thisRow["itemcode"] = textBox1.Text;
                thisRow["qty_in_hand"] = textBox2.Text;
                thisRow["location"] = textBox3.Text;
                thisRow["reorderlevel"] = textBox4.Text;
                thisRow["reorderqty"] = textBox5.Text;
                thisRow["safetystocklevel"] = textBox6.Text;
                thisRow["max_stock"] = textBox7.Text;
                thisRow["value"] = textBox8.Text;
                stkDataset.Tables["StockMaster"].Rows.Add(thisRow);
                msg = dbo.Update(stkDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("Entry already exists", "ERP-PIMS");
            }
            Stock_Load(sender, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
          stkDataset = dbo.Select("SELECT * FROM StockMaster where itemcode=" + textBox12.Text, table);
            //dataGridView1.DataSource = stkDataset.Tables[table];
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void linkLabel22_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();

        }

        private void linkLabel21_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            quotations abc = new quotations();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel20_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PO abc = new PO();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel16_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();

        }

        private void linkLabel19_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel15_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Stock stk = new Stock();
            stk.MdiParent = this.MdiParent;
            stk.Show();
            this.Hide();
        }

        private void linkLabel18_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel17_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

       
    }
}
