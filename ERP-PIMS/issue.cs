﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class issue : Form
    {
        string usrDetails, usrName, usrCode;
        string query = "select * from IssuesMaster";
        string msg;
        string table = "IssuesMaster";
        DbOperations dbo = new DbOperations();
        DataSet isuDataset = new DataSet();
        int val;

        public issue()
        {
            InitializeComponent();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
         }

        private void button2_Click(object sender, EventArgs e)
        {
            report Genrep = new report();
            Genrep.Show();
        }

        private void issue_Load(object sender, EventArgs e)
        {
            //this.lblDetails.Text += this.usrName;
            //this.lblStatus.Text += this.usrDetails;
            //this.lblUsercode.Text = this.usrCode;
            isuDataset = dbo.Select(query, table);
            //dataGridView1.DataSource = isuDataset.Tables[table];
            foreach (DataRow theRow in isuDataset.Tables[table].Rows)
            {

                msg = theRow["issue_code"].ToString();
                val = Convert.ToInt16(msg);
            }
            textBox1.Text = " " + ++val;
            textBox2.Text = textBox3.Text = "";
           
            
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
           //make button

            isuDataset = dbo.Select(query, table);

            DataColumn[] keys = new DataColumn[1];
            keys[0] = isuDataset.Tables["IssuesMaster"].Columns["issue_no"];
            isuDataset.Tables["IssuesMaster"].PrimaryKey = keys;

            DataRow findRow = isuDataset.Tables["IssuesMater"].Rows.Find(textBox1.Text);

            if (findRow == null)
            {

                DataRow thisRow = isuDataset.Tables["IssuesMaster"].NewRow();
                //issue number y u didnt place???
                //thisRow["issue_no"] = textBox1.Text;
                thisRow["mrno"] = textBox1.Text;
                thisRow["qty"] = textBox2.Text;
                thisRow["itemcode"] = textBox3.Text;
                //thisRow["cc_code"] = textBox4.Text;
                              
              
                isuDataset.Tables["IssuesMaster"].Rows.Add(thisRow);
                msg = dbo.Update(isuDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("Entry already exists", "ERP-PIMS");
            }
            issue_Load(sender, e);
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }
    }
}
