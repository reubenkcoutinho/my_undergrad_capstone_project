﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class UserDetails : Form
    {
        public UserDetails(RELERPDBDataSet reds, int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.userMasterBindingSource.DataSource = this.rELERPDBDataSet;
            this.userMasterBindingSource.Position = position;
        }

        private void userMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.userMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void UserDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.UserMaster' table. You can move, or remove it, as needed.
            //this.userMasterTableAdapter.Fill(this.rELERPDBDataSet.UserMaster);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.userMasterBindingSource.EndEdit();
                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("User details updated.", "ERP-PIMS");
                }
                else
                {
                    MessageBox.Show("User details not updated.", "ERP-PIMS");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }
            this.Close();
        }
    }
}
