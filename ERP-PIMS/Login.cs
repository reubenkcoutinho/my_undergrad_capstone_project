﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ERP_PIMS
{
    public partial class Login : Form
    {
        DbOperations dbo = new DbOperations();
        string details, name, code;
        bool userExists;
        public Login()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataSet loginDataset = dbo.Select("select * from UserMaster where username='" + textBox1.Text + "' and password='" + textBox2.Text + "'", "UserMaster");
            menu menufrm = new menu();
            if (loginDataset == null)
            {
                MessageBox.Show("Error has occured", "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                foreach (DataRow thisRow in loginDataset.Tables["UserMaster"].Rows)
                {
                    this.details = thisRow["details"].ToString();
                    this.name = thisRow["name"].ToString();
                    this.code = thisRow["usercode"].ToString();
                    userExists = true;
                }


                if (userExists)
                {
                    menufrm.usrDetails = this.details;
                    menufrm.usrCode = this.code;
                    menufrm.usrName = this.name;
                    menufrm.Show();
                    this.Hide();
                }

                else
                {
                    MessageBox.Show("User doesn't exist", "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
