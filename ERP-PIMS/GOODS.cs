﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class GOODS : Form
    {
        string usrDetails, usrName, usrCode;
        string msg;
        int val;
        string query = "select * from GRMaster";
        string table = "GRMaster";
        DbOperations dbo = new DbOperations();
        DataSet grDataset = new DataSet();
        DataSet fromPoDs = new DataSet();
        DataSet grvDataset = new DataSet();
        AutoCompleteStringCollection poCollection = new AutoCompleteStringCollection();
        public GOODS()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string str = "";
            if (radioButton5.Checked)
            {
                str = "SELECT * FROM GRMaster where itemcode=" + textBox12.Text;
            }
            else
            {
                str = "SELECT * FROM GRMaster where grno=" + textBox11.Text;
            }
            //grDataset = dbo.Select(str, table);
            //dataGridView1.DataSource = grDataset.Tables[table];
        }

        private void GRN_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.PRMaster' table. You can move, or remove it, as needed.
            this.pRMasterTableAdapter.Fill(this.rELERPDBDataSet.PRMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.RFQMaster' table. You can move, or remove it, as needed.
            this.rFQMasterTableAdapter.Fill(this.rELERPDBDataSet.RFQMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.QuotationMaster' table. You can move, or remove it, as needed.
            this.quotationMasterTableAdapter.Fill(this.rELERPDBDataSet.QuotationMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.POMaster' table. You can move, or remove it, as needed.
            this.pOMasterTableAdapter.Fill(this.rELERPDBDataSet.POMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.GRVMaster' table. You can move, or remove it, as needed.
            this.gRVMasterTableAdapter.Fill(this.rELERPDBDataSet.GRVMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.GRMaster' table. You can move, or remove it, as needed.
            this.gRMasterTableAdapter.Fill(this.rELERPDBDataSet.GRMaster);
            grDataset = dbo.Select(query, "GRMaster");
            //dataGridView1.DataSource = grDataset.Tables[table];
            //System.Data.SqlClient.SqlDataReader dReader;
            //System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            grvDataset = dbo.Select("select * from GRVMaster", "GRVMaster");
            //dataGridView1.DataSource = grvDataset.Tables[table];

            foreach (DataRow theRow in grvDataset.Tables["GRVMaster"].Rows)
            {

                msg = theRow["grvno"].ToString();
                val = Convert.ToInt16(msg);
            }
            grvnoTextBox.Text = (++val).ToString();
            foreach (DataRow theRow in grDataset.Tables["GRMaster"].Rows)
            {

                msg = theRow["grno"].ToString();
                val = Convert.ToInt16(msg);
            }
            grnoTextBox.Text = (++val).ToString();
            quotationnoComboBox.Visible = quotationnoTextBox.Visible =
                rfqnoComboBox.Visible = rfqnoTextBox.Visible =
                prnoComboBox.Visible = prnoTextBox.Visible = false;


            
            foreach (DataRow dr in this.rELERPDBDataSet.GRMaster.Rows)
            {
                
                foreach (DataRowView d in po_noComboBox.Items)
                {
                    if (!dr.ItemArray[2].Equals(d.Row.ItemArray[0]))
                    {
                        if (!checkComboBox(po_noComboBox1, Convert.ToInt32(d.Row.ItemArray[0])))
                        {
                            po_noComboBox1.Items.Add(d.Row.ItemArray[0]);
                        }
                    }
                    
                }
            }
            po_noComboBox1.SelectedIndex = 0;
            po_noComboBox1_SelectedIndexChanged(sender, e);


            //foreach (int d in po_noComboBox1.Items)
            //{
            //    if (d == 0)
            //    { }
            //    //if (!d.ItemArray[2].Equals(d.Row.ItemArray[0]))
            //    //{
            //    //    po_noComboBox1.Items.Add(d.Row.ItemArray[0]);
            //    //}
            //    //count++;
            //}
            
            //try
            //{
            //    //cmd.Connection = dbo.thisConnection;
            //    //cmd.CommandType = CommandType.Text;
            //    //cmd.CommandText = ;
            //    dbo.openConnection();
            //    dReader = dbo.SelectDR("SELECT * FROM POMaster");

            //    if (dReader.HasRows == true)
            //    {
            //        while (dReader.Read())
            //            poCollection.Add(dReader["po_no"].ToString());

            //    }
            //    dReader.Close();
            //    dbo.closeConnection();
            //    po_noComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //    po_noComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            //    po_noComboBox.AutoCompleteCustomSource = poCollection;
            //    //RELERPDBDataSet.GRMasterRow grnrow = rELERPDBDataSet.GRMaster.NewGRMasterRow();
            //    //grnrow.po_no = "";
            //    //grnrow.itemcode = Convert.ToInt16("0");
            //    //grnrow.rate = Convert.ToDecimal(rateTextBox.Text);
            //    //grnrow.value = Convert.ToDecimal(amountTextBox.Text);
            //    //grnrow.qtyrcvd = Convert.ToDecimal(qtyrcvdTextBox.Text);
            //    //grnrow.qtyaccptd = Convert.ToDecimal(qtyaccptdTextBox.Text);
            //    //grnrow.status = statusTextBox.Text;
            //    //this.rELERPDBDataSet.GRMaster.AddGRMasterRow(grnrow);
            //}
            //finally { }


            //foreach (ProductDataSet.ProductsRow prod in this.productDataSet.Products)
            //{
            //    oproduct.Add(prod.ProductName);
            //}
            //this.productNameComboBox.AutoCompleteCustomSource = oproduct;



        }

        private bool checkComboBox(ComboBox val,int value)
        {
            bool chk = false;
            foreach (int d in val.Items)
            {
                if (d.Equals(value))
                {
                    chk = true;
                    break;
                }
            }
            return chk;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            #region "Commented region"
            //query = "select description,unit_of_measure from ItemMaster";

            //grDataset = dbo.Select(query, table);

            //DataColumn[] keys = new DataColumn[1];
            //keys[0] = grDataset.Tables[table].Columns["grno"];
            //grDataset.Tables[table].PrimaryKey = keys;

            //DataRow findRow = grDataset.Tables[table].Rows.Find(textBox1.Text);

            //if (findRow == null)
            //{

            //    DataRow thisRow = grDataset.Tables[table].NewRow();
            //    thisRow["grno"] = textBox1.Text;
            //    thisRow["po_no"] = textBox2.Text;
            //    thisRow["itemcode"] = textBox3.Text;
            //    thisRow["rate"] = textBox4.Text;
            //    thisRow["value"] = textBox5.Text;
            //    thisRow["qtyrcvd"] = textBox6.Text;
            //    thisRow["qtyaccptd"] = textBox7.Text;
            //    thisRow["grdate"] = DateTime.Now.Date;
            //    thisRow["status"] = textBox13.Text;
            //    grDataset.Tables[table].Rows.Add(thisRow);
            //    msg = dbo.Update(grDataset, query, table);
            //    MessageBox.Show(msg, "ERP-PIMS");
            //}
            //else
            //{
            //    MessageBox.Show("GRN already added", "ERP-PIMS");
            //}
            //GRN_Load(sender, e);
            #endregion
            try
            {

                grvDataset = dbo.Select("select * from GRVMaster", "GRVMaster");

                DataColumn[] keys = new DataColumn[1];
                keys[0] = grvDataset.Tables["GRVMaster"].Columns["grvno"];
                grvDataset.Tables["GRVMaster"].PrimaryKey = keys;

                DataRow findRow = grvDataset.Tables["GRVMaster"].Rows.Find(grvnoTextBox.Text);

                if (findRow == null)
                {

                    DataRow thisRow = grvDataset.Tables["GRVMaster"].NewRow();
                    thisRow["grvno"] = grvnoTextBox.Text;
                    thisRow["po_no"] = po_noTextBox1.Text;
                    thisRow["grno"] = grnoTextBox1.Text;
                    thisRow["itemcode"] = itemcodeTextBox1.Text;
                    thisRow["qtyrjctd"] = qtyrjctdTextBox.Text;
                    thisRow["reason"] = reasonTextBox.Text;
                    grvDataset.Tables["GRVMaster"].Rows.Add(thisRow);
                    msg = dbo.Update(grvDataset, "select * from GRVMaster", "GRVMaster");
                    MessageBox.Show(msg, "ERP-PIMS");

                }
                else
                {
                    MessageBox.Show("GRV already added", "ERP-PIMS");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //textBox1.Text = dataGridView1.CurrentRow.Cells["grno"].Value.ToString();
            //textBox2.Text = dataGridView1.CurrentRow.Cells["po_no"].Value.ToString();
            //textBox3.Text = dataGridView1.CurrentRow.Cells["itemcode"].Value.ToString();
            //textBox4.Text = dataGridView1.CurrentRow.Cells["rate"].Value.ToString();
            //textBox5.Text = dataGridView1.CurrentRow.Cells["value"].Value.ToString();
            //textBox6.Text = dataGridView1.CurrentRow.Cells["qtyrcvd"].Value.ToString();
            //textBox7.Text = dataGridView1.CurrentRow.Cells["qtyaccptd"].Value.ToString();
            //textBox13.Text = dataGridView1.CurrentRow.Cells["status"].Value.ToString();
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            textBox12.Enabled = true;
            textBox11.Enabled = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox12.Enabled = !true;
            textBox11.Enabled = !false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {

                grDataset = dbo.Select(query, table);

                DataColumn[] keys = new DataColumn[1];
                keys[0] = grDataset.Tables[table].Columns["grno"];
                grDataset.Tables[table].PrimaryKey = keys;

                DataRow findRow = grDataset.Tables[table].Rows.Find(grnoTextBox.Text);

                if (findRow == null)
                {

                    DataRow thisRow = grDataset.Tables[table].NewRow();
                    thisRow["grno"] = grnoTextBox.Text;
                    thisRow["grdate"] = DateTime.Now.Date;
                    thisRow["po_no"] = po_noComboBox.Text;
                    thisRow["itemcode"] = itemcodeTextBox.Text;
                    thisRow["rate"] = rateTextBox.Text;
                    thisRow["value"] = amountTextBox.Text;
                    thisRow["qtyrcvd"] = Convert.ToDouble(quantityTextBox.Text); //qtyrcvdTextBox.Text;;
                    thisRow["qtyaccptd"] = Convert.ToDouble(quantityTextBox.Text);
                    thisRow["grdate"] = DateTime.Now.Date;
                    thisRow["status"] = statusTextBox.Text;
                    grDataset.Tables[table].Rows.Add(thisRow);
                    msg = dbo.Update(grDataset, query, table);
                    MessageBox.Show(msg, "ERP-PIMS");

                    int a, b;
                    b = Convert.ToInt16(qtyaccptdTextBox.Text);
                    a = Convert.ToInt16(quantityTextBox.Text);

                    if ((a - b) != 0)
                    {
                        panel5.Enabled = true;
                        qtyrjctdTextBox.Text = (a - b).ToString();
                        po_noTextBox1.Text = po_noComboBox.Text;
                        itemcodeTextBox1.Text = itemcodeTextBox.Text;
                        grnoTextBox1.Text = grnoTextBox.Text;

                    }




                }
                else
                {
                    MessageBox.Show("GRV already added", "ERP-PIMS");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

        }


        private void gRMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.gRMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Stock stk = new Stock();
            stk.MdiParent = this.MdiParent;
            stk.Show();
        }

        private void qtyaccptdTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (rateTextBox.Text != "" || qtyaccptdTextBox.Text != "" || quantityTextBox.Text != "")
                {
                    int x = Convert.ToInt16(rateTextBox.Text);
                    int y = Convert.ToInt16(qtyaccptdTextBox.Text);
                    int z = Convert.ToInt16(quantityTextBox.Text);
                    if (y <= z)
                    {
                        //amountTextBox.Text = (x * y).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Quantity accepted is more than quantity recieved", "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        qtyaccptdTextBox.Text = qtyaccptdTextBox.Text.Substring(0, qtyaccptdTextBox.Text.Length - 1);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void po_noTextBox_TextChanged(object sender, EventArgs e)
        {
            string str;
            DataSet fromQout = new DataSet();
            try
            {
                statusTextBox.Text = "";
                qtyaccptdTextBox.Text = "";
                if (po_noComboBox.Text != "")
                {
                    fromPoDs = dbo.Select("SELECT quotationno,itemcode from POMaster where po_no=" + po_noComboBox.Text, "POMaster");
                    if (fromPoDs.Tables["POMaster"].Rows.Count > 0)
                    {
                        str = fromPoDs.Tables["POMaster"].Rows[0]["quotationno"].ToString();
                        itemcodeTextBox.Text = fromPoDs.Tables["POMaster"].Rows[0]["itemcode"].ToString();
                        fromQout = dbo.Select("SELECT rate,quantity from QuotationMaster where quotationno=" + str, "QuotationMaster");
                        if (fromQout.Tables["QuotationMaster"].Rows.Count > 0)
                        {
                            rateTextBox.Text = fromQout.Tables["QuotationMaster"].Rows[0]["rate"].ToString();
                            quantityTextBox.Text = fromQout.Tables["QuotationMaster"].Rows[0]["quantity"].ToString();
                        }
                    }
                    //DataRow dr1 = rELERPDataSet.POMaster.Rows.Find(po_noComboBox.Text);
                    //if (dr1.Table.Rows.Count>0)
                    //{
                    //    str = rELERPDataSet.POMaster.Rows[0].Table.Rows[0]["quotationno"].ToString();
                    //    textBox2.Text = dr1.Table.Rows[0]["itemcode"].ToString();
                    //    DataRow dr =  rELERPDataSet.QuotationMaster.Rows.Find(str);
                    //    if (dr.Table.Rows.Count> 0)
                    //    {
                    //        textBox4.Text = dr.Table.Rows[0]["rate"].ToString(); // rELERPDataSet.QuotationMaster.Rows[0].Table.Rows[0]["rate"].ToString();
                    //        textBox3.Text = dr.Table.Rows[0]["quantity"].ToString();// rELERPDataSet.QuotationMaster.Rows[0].Table.Rows[0]["quantity"].ToString();
                    //    }
                    //}
                    else
                    {
                        MessageBox.Show("Such a PO number Does'nt exist", "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        itemcodeTextBox.Text = rateTextBox.Text = quantityTextBox.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERP-PIMS", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void fillByItemcodeGRNToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                gRMasterDataGridView.Visible = true;
                gRVMasterDataGridView.Visible = false;
                if (radioButton5.Checked)
                {
                    this.gRMasterTableAdapter.FillByGRNitemcode(this.rELERPDBDataSet.GRMaster, new System.Nullable<int>(((int)(System.Convert.ChangeType(textBox12.Text, typeof(int))))));
                }
                else
                {
                    this.gRMasterTableAdapter.FillByGRno(this.rELERPDBDataSet.GRMaster, ((int)(System.Convert.ChangeType(textBox11.Text, typeof(int)))));
                }

            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void fillByGRnoToolStripButton_Click(object sender, EventArgs e)
        {


        }

        private void radioButton5_CheckedChanged_1(object sender, EventArgs e)
        {
            textBox12.Enabled = true;
            textBox11.Enabled = false;
        }

        private void radioButton2_CheckedChanged_1(object sender, EventArgs e)
        {
            textBox12.Enabled = false;
            textBox11.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                gRMasterDataGridView.Visible = false;
                gRVMasterDataGridView.Visible = true;
                if (radioButton7.Checked)
                {
                    this.gRVMasterTableAdapter.FillBygrvitmcde(this.rELERPDBDataSet.GRVMaster, ((int)(System.Convert.ChangeType(textBox21.Text, typeof(int)))));

                }
                else
                {
                    this.gRVMasterTableAdapter.FillBygrvno(this.rELERPDBDataSet.GRVMaster, ((int)(System.Convert.ChangeType(textBox20.Text, typeof(int)))));

                }

            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void fillBygrvitmcdeToolStripButton_Click(object sender, EventArgs e)
        {


        }

        private void fillBygrvnoToolStripButton_Click(object sender, EventArgs e)
        {

        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            textBox21.Enabled = true;
            textBox20.Enabled = false;

        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            textBox21.Enabled = false;
            textBox20.Enabled = true;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            quotations abc = new quotations();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PO abc = new PO();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void quotationnoTextBox_TextChanged(object sender, EventArgs e)
        {
            quotationnoComboBox.Text = quotationnoTextBox.Text;
        }

        private void po_noComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        //private double calculateQuantity(double amt, double rate, double tax)
        //{
        //    double quantity;
        //    quantity = amt / rate / (1 + tax / 100);
        //    return quantity;
        //}

        private void quotationnoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void rfqnoTextBox_TextChanged(object sender, EventArgs e)
        {
            rfqnoComboBox.Text = rfqnoTextBox.Text;
        }

        private void prnoTextBox_TextChanged(object sender, EventArgs e)
        {
            prnoComboBox.Text = prnoTextBox.Text;
        }

        private void gRVMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataRowView myDataRowView = (DataRowView)gRVMasterDataGridView.SelectedRows[0].DataBoundItem;
            RELERPDBDataSet.GRVMasterRow thisRow = (RELERPDBDataSet.GRVMasterRow)myDataRowView.Row;
            int myPosition = rELERPDBDataSet.GRVMaster.Rows.IndexOf(thisRow);

            GRVDetails frm = new GRVDetails(rELERPDBDataSet, myPosition);
            frm.ShowDialog();
        }

        private void gRMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataRowView myDataRowView = (DataRowView)gRMasterDataGridView.SelectedRows[0].DataBoundItem;
            RELERPDBDataSet.GRMasterRow thisRow = (RELERPDBDataSet.GRMasterRow)myDataRowView.Row;
            int myPosition = rELERPDBDataSet.GRMaster.Rows.IndexOf(thisRow);

            GRNDetails frm = new GRNDetails(rELERPDBDataSet, myPosition);
            frm.ShowDialog();
        }

        private void po_noComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            po_noComboBox.Text = po_noComboBox1.Text;
        }


    }
}
