﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class MReq : Form
    {
        string usrDetails, usrName, usrCode;
        string msg;
        int val;
        string query = "select * from MReqMaster";
        string table = "MReqMaster";
        DataSet MReqDataset = new DataSet();
        DbOperations dbo = new DbOperations();
        public MReq()
        {
            InitializeComponent();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.MdiParent = this.MdiParent;
            Genrep.Show();
        }

        private void MReq_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.MReqMaster' table. You can move, or remove it, as needed.
            this.mReqMasterTableAdapter.Fill(this.rELERPDBDataSet.MReqMaster);
            string qry = "SELECT * FROM MReqMaster"; 
            MReqDataset = dbo.Select(qry, table);
            if (this.usrDetails == "employee")
            {
                //button3.Enabled = button5.Enabled = button1.Enabled = groupBox1.Enabled = dataGridView1.Enabled = false;
                qry = "SELECT * FROM MReqMaster where usercode=" + this.usrCode;
            }
            foreach (DataRow theRow in MReqDataset.Tables["MReqMaster"].Rows)
            {

                msg = theRow["mrno"].ToString();
                val = Convert.ToInt16(msg);
            }
            textBox10.Text = (++val).ToString();
           
            //dataGridView1.DataSource = MReqDataset.Tables[table];
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            textBox5.Enabled = true;
            textBox8.Enabled = textBox9.Enabled = false;
        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            textBox8.Enabled = true;
            textBox5.Enabled = textBox9.Enabled = false;
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            textBox9.Enabled = true;
            textBox5.Enabled = textBox8.Enabled = false;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            this.textBox1.Enabled = true;
            this.textBox6.Enabled = this.textBox7.Enabled = false;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            this.textBox6.Enabled = true;
            this.textBox1.Enabled = this.textBox7.Enabled = false;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            this.textBox7.Enabled = true;
            this.textBox6.Enabled = this.textBox1.Enabled = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // ADD BUTTON
            MReqDataset = dbo.Select(query, table);
            DataColumn[] keys = new DataColumn[1];
            keys[0] = MReqDataset.Tables["MReqMaster"].Columns["mrno"];
            MReqDataset.Tables["MReqMaster"].PrimaryKey = keys;

            DataRow findRow = MReqDataset.Tables["MReqMaster"].Rows.Find(textBox10.Text);

            if (findRow == null)
            {

                DataRow thisRow = MReqDataset.Tables["MReqMaster"].NewRow();
                thisRow["mrno"] = textBox10.Text;
                thisRow["mrdate"] = dateTimePicker1.Text;
                thisRow["mrqty"] = textBox12.Text;
                thisRow["itemcode"] = textBox13.Text;
                thisRow["usercode"] = textBox14.Text;

                MReqDataset.Tables["MReqMaster"].Rows.Add(thisRow);
                msg = dbo.Update(MReqDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("MR already exists", "ERP-PIMS");
            }
            MReq_Load(sender, e);
        }

        private void mReqMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.mReqMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

      

      

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (usrDetails == "employee")
                {
                   
               
                    this.mReqMasterTableAdapter.FillByITM(this.rELERPDBDataSet.MReqMaster, new System.Nullable<int>(((int)(System.Convert.ChangeType(textBox9.Text, typeof(int))))));

                }
                else
                {
                    if (radioButton7.Checked)
                    {
                        this.mReqMasterTableAdapter.FillByMRNO(this.rELERPDBDataSet.MReqMaster, ((int)(System.Convert.ChangeType(textBox5.Text, typeof(int)))));
                    } if (radioButton9.Checked)
                    {
                        this.mReqMasterTableAdapter.FillByUSR(this.rELERPDBDataSet.MReqMaster, new System.Nullable<int>(((int)(System.Convert.ChangeType(textBox8.Text, typeof(int))))));
                    }
                    if (radioButton8.Checked)
                    {
                        this.mReqMasterTableAdapter.FillByITM(this.rELERPDBDataSet.MReqMaster, new System.Nullable<int>(((int)(System.Convert.ChangeType(textBox9.Text, typeof(int))))));
                    }
                }
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);

            }
        }

        private void radioButton7_CheckedChanged_1(object sender, EventArgs e)
        {
            if (usrDetails != "employee")
            {

                textBox5.Enabled = true;
                textBox8.Enabled = false;
                textBox9.Enabled = false;
            }
        }

        private void radioButton9_CheckedChanged_1(object sender, EventArgs e)
        {
            if (usrDetails != "employee")
            {

                textBox5.Enabled = !true;
                textBox8.Enabled = !false;
                textBox9.Enabled = !true;
            }
        }

        private void radioButton8_CheckedChanged_1(object sender, EventArgs e)
        {
            textBox5.Enabled = !true;
            textBox8.Enabled = !true;
            textBox9.Enabled = !false;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            quotations abc = new quotations();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PO abc = new PO();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();

        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Stock stk = new Stock();
            stk.MdiParent = this.MdiParent;
            stk.Show();
            this.Hide();
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

    
      
    
    }
}
