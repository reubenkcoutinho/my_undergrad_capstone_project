﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace ERP_PIMS
{
    public partial class Item : Form
    {
        string usrDetails, usrName, usrCode;
        string msg;
        int val;
        string query = "select * from ItemMaster";
        string table = "ItemMaster";
        DbOperations dbo = new DbOperations();
        DataSet itmDataset = new DataSet();
        public Item()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            menu menufrm = new menu();
            menufrm.usrDetails = this.usrDetails;
            menufrm.usrCode = this.usrCode;
            menufrm.usrName = this.usrName;
            this.Hide();
            menufrm.Show();
        }

        private void Item_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.ItemMaster' table. You can move, or remove it, as needed.
            this.itemMasterTableAdapter.Fill(this.rELERPDBDataSet.ItemMaster);
            //this.lblDetails.Text = "Hello " + this.usrName;
            //this.lblStatus.Text = "You are logged in as " + this.usrDetails;
            this.lblUsercode.Text = this.usrCode;
            itmDataset = dbo.Select(query, table);
            //dataGridView1.DataSource = itmDataset.Tables[table];
            foreach (DataRow theRow in itmDataset.Tables[table].Rows)
            {

                msg = theRow["itemcode"].ToString();
                val = Convert.ToInt16(msg);
            }
            textBox1.Text = " " + ++val;
            textBox2.Text = textBox3.Text = "";
            button1.Enabled = true;
            button7.Enabled = button4.Enabled = false;
            
           
            
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            #region "Commented data"
            //itmDataset = dbo.Select(query, table);


            //DataColumn[] keys = new DataColumn[1];
            //keys[0] = itmDataset.Tables["ItemMaster"].Columns["itemcode"];
            //itmDataset.Tables["ItemMaster"].PrimaryKey = keys;


            //DataRow findRow = itmDataset.Tables["ItemMaster"].Rows.Find(textBox1.Text);

            //if (findRow == null)
            //{
            //    DataRow thisRow = itmDataset.Tables["ItemMaster"].NewRow();
            //    thisRow["itemcode"] = textBox1.Text;
            //    thisRow["description"] = textBox2.Text;
            //    thisRow["unit_of_measure"] = textBox3.Text;
            //    itmDataset.Tables["ItemMaster"].Rows.Add(thisRow);
            //    msg = dbo.Update(itmDataset, query, table);
            //    MessageBox.Show(msg, "ERP-PIMS");
            //}
            //else
            //{

            //    MessageBox.Show("Item exists.", "ERP-PIMS");
            //}

            //Item_Load(sender, e);
            #endregion

            this.Validate();
            this.itemMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //textBox1.Text = dataGridView1.CurrentRow.Cells["itemcode"].Value.ToString();
            //textBox2.Text = dataGridView1.CurrentRow.Cells["description"].Value.ToString();
            //textBox3.Text = dataGridView1.CurrentRow.Cells["unit_of_measure"].Value.ToString();
            button1.Enabled = false;
            button7.Enabled = button4.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                this.itemMasterTableAdapter.FillByitemcode(this.rELERPDBDataSet.ItemMaster, ((int)(System.Convert.ChangeType(textBox6.Text, typeof(int)))));
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //query = "select description,unit_of_measure from ItemMaster";

            itmDataset = dbo.Select(query, table);
            
            
            DataColumn[] keys = new DataColumn[1];
            keys[0] = itmDataset.Tables["ItemMaster"].Columns["itemcode"];
            itmDataset.Tables["ItemMaster"].PrimaryKey = keys;
            

            DataRow findRow = itmDataset.Tables["ItemMaster"].Rows.Find(textBox1.Text);

            if (findRow == null)
            {
                MessageBox.Show("Item doesn't exists.Please add.", "ERP-PIMS");
            }
            else
            {
                
                findRow["itemcode"] = textBox1.Text;
                findRow["description"] = textBox2.Text;
                findRow["unit_of_measure"] = textBox3.Text;
                msg = dbo.Update(itmDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }
            
            Item_Load(sender, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Item_Load(sender, e);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            itmDataset = dbo.Select(query, table);

            DataColumn[] keys = new DataColumn[1];
            keys[0] = itmDataset.Tables["ItemMaster"].Columns["itemcode"];
            itmDataset.Tables["ItemMaster"].PrimaryKey = keys;

            DataRow findRow = itmDataset.Tables["ItemMaster"].Rows.Find(textBox1.Text);

            if (findRow == null)
            {
                MessageBox.Show("Item doesn't exists.Cannot delete.", "ERP-PIMS");
            }
            else
            {

                findRow.Delete();
                msg = dbo.Update(itmDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }

            Item_Load(sender, e);
        }

        private void itemMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.itemMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void fillByitemcodeToolStripButton_Click(object sender, EventArgs e)
        {


        }

        private void itemMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataRowView myDataRowView = (DataRowView)itemMasterDataGridView.SelectedRows[0].DataBoundItem;
            RELERPDBDataSet.ItemMasterRow thisRow = (RELERPDBDataSet.ItemMasterRow)myDataRowView.Row;
            int myPosition = rELERPDBDataSet.ItemMaster.Rows.IndexOf(thisRow);

            ItemDetails frm = new ItemDetails(rELERPDBDataSet, myPosition);
            frm.ShowDialog();
        }



        

    }
}
