﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class GRNDetails : Form
    {
        public GRNDetails(RELERPDBDataSet reds, int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.gRMasterBindingSource.DataSource = this.rELERPDBDataSet;
            this.gRMasterBindingSource.Position = position;
        }

        private void gRMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.gRMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void GRNDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.GRMaster' table. You can move, or remove it, as needed.
            //this.gRMasterTableAdapter.Fill(this.rELERPDBDataSet.GRMaster);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.gRMasterBindingSource.EndEdit();

                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("GRN details updated.", "ERP-PIMS");

                }
                else
                {
                    MessageBox.Show("GRN details not updated.", "ERP-PIMS");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }
        }
    }
}
