﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class BudgetDetails : Form

    {

        public BudgetDetails(RELERPDBDataSet reds, int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.budgetMasterBindingSource.DataSource = this.rELERPDBDataSet;
            this.budgetMasterBindingSource.Position = position;

         }
    

    
        private void BudgetDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.BudgetMaster' table. You can move, or remove it, as needed.
            this.budgetMasterTableAdapter.Fill(this.rELERPDBDataSet.BudgetMaster);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.budgetMasterBindingSource.EndEdit();

                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("Budget details updated.", "ERP-PIMS");

                }
                else
                {
                    MessageBox.Show("Budget details not updated.", "ERP-PIMS");

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.budgetMasterBindingSource.RemoveCurrent();
            if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
            {
                MessageBox.Show("Budget details deleted.", "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("Budget details not deleted.", "ERP-PIMS");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
