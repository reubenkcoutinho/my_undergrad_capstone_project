﻿namespace ERP_PIMS
{
    partial class EditQuotation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label quotationnoLabel;
            System.Windows.Forms.Label rfqnoLabel;
            System.Windows.Forms.Label quotation_dateLabel;
            System.Windows.Forms.Label itemcodeLabel;
            System.Windows.Forms.Label rateLabel;
            System.Windows.Forms.Label deliverydateLabel;
            System.Windows.Forms.Label payment_termsLabel;
            System.Windows.Forms.Label taxLabel;
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.quotationMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.quotationMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.QuotationMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.quotationnoTextBox = new System.Windows.Forms.TextBox();
            this.rfqnoTextBox = new System.Windows.Forms.TextBox();
            this.quotation_dateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.rateTextBox = new System.Windows.Forms.TextBox();
            this.deliverydateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.payment_termsTextBox = new System.Windows.Forms.TextBox();
            this.taxTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            quotationnoLabel = new System.Windows.Forms.Label();
            rfqnoLabel = new System.Windows.Forms.Label();
            quotation_dateLabel = new System.Windows.Forms.Label();
            itemcodeLabel = new System.Windows.Forms.Label();
            rateLabel = new System.Windows.Forms.Label();
            deliverydateLabel = new System.Windows.Forms.Label();
            payment_termsLabel = new System.Windows.Forms.Label();
            taxLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quotationMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // quotationnoLabel
            // 
            quotationnoLabel.AutoSize = true;
            quotationnoLabel.Location = new System.Drawing.Point(94, 22);
            quotationnoLabel.Name = "quotationnoLabel";
            quotationnoLabel.Size = new System.Drawing.Size(66, 13);
            quotationnoLabel.TabIndex = 1;
            quotationnoLabel.Text = "quotationno:";
            // 
            // rfqnoLabel
            // 
            rfqnoLabel.AutoSize = true;
            rfqnoLabel.Location = new System.Drawing.Point(94, 48);
            rfqnoLabel.Name = "rfqnoLabel";
            rfqnoLabel.Size = new System.Drawing.Size(34, 13);
            rfqnoLabel.TabIndex = 3;
            rfqnoLabel.Text = "rfqno:";
            // 
            // quotation_dateLabel
            // 
            quotation_dateLabel.AutoSize = true;
            quotation_dateLabel.Location = new System.Drawing.Point(94, 75);
            quotation_dateLabel.Name = "quotation_dateLabel";
            quotation_dateLabel.Size = new System.Drawing.Size(78, 13);
            quotation_dateLabel.TabIndex = 5;
            quotation_dateLabel.Text = "quotation date:";
            // 
            // itemcodeLabel
            // 
            itemcodeLabel.AutoSize = true;
            itemcodeLabel.Location = new System.Drawing.Point(94, 100);
            itemcodeLabel.Name = "itemcodeLabel";
            itemcodeLabel.Size = new System.Drawing.Size(53, 13);
            itemcodeLabel.TabIndex = 7;
            itemcodeLabel.Text = "itemcode:";
            // 
            // rateLabel
            // 
            rateLabel.AutoSize = true;
            rateLabel.Location = new System.Drawing.Point(94, 126);
            rateLabel.Name = "rateLabel";
            rateLabel.Size = new System.Drawing.Size(28, 13);
            rateLabel.TabIndex = 9;
            rateLabel.Text = "rate:";
            // 
            // deliverydateLabel
            // 
            deliverydateLabel.AutoSize = true;
            deliverydateLabel.Location = new System.Drawing.Point(94, 153);
            deliverydateLabel.Name = "deliverydateLabel";
            deliverydateLabel.Size = new System.Drawing.Size(67, 13);
            deliverydateLabel.TabIndex = 11;
            deliverydateLabel.Text = "deliverydate:";
            // 
            // payment_termsLabel
            // 
            payment_termsLabel.AutoSize = true;
            payment_termsLabel.Location = new System.Drawing.Point(94, 178);
            payment_termsLabel.Name = "payment_termsLabel";
            payment_termsLabel.Size = new System.Drawing.Size(78, 13);
            payment_termsLabel.TabIndex = 13;
            payment_termsLabel.Text = "payment terms:";
            // 
            // taxLabel
            // 
            taxLabel.AutoSize = true;
            taxLabel.Location = new System.Drawing.Point(94, 204);
            taxLabel.Name = "taxLabel";
            taxLabel.Size = new System.Drawing.Size(24, 13);
            taxLabel.TabIndex = 15;
            taxLabel.Text = "tax:";
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // quotationMasterBindingSource
            // 
            this.quotationMasterBindingSource.DataMember = "QuotationMaster";
            this.quotationMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // quotationMasterTableAdapter
            // 
            this.quotationMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = null;
            this.tableAdapterManager.GRVMasterTableAdapter = null;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = null;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = null;
            this.tableAdapterManager.PRMasterTableAdapter = null;
            this.tableAdapterManager.QuotationMasterTableAdapter = this.quotationMasterTableAdapter;
            this.tableAdapterManager.RFQMasterTableAdapter = null;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // quotationnoTextBox
            // 
            this.quotationnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "quotationno", true));
            this.quotationnoTextBox.Location = new System.Drawing.Point(178, 19);
            this.quotationnoTextBox.Name = "quotationnoTextBox";
            this.quotationnoTextBox.ReadOnly = true;
            this.quotationnoTextBox.Size = new System.Drawing.Size(107, 20);
            this.quotationnoTextBox.TabIndex = 2;
            // 
            // rfqnoTextBox
            // 
            this.rfqnoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "rfqno", true));
            this.rfqnoTextBox.Location = new System.Drawing.Point(178, 45);
            this.rfqnoTextBox.Name = "rfqnoTextBox";
            this.rfqnoTextBox.Size = new System.Drawing.Size(107, 20);
            this.rfqnoTextBox.TabIndex = 4;
            // 
            // quotation_dateDateTimePicker
            // 
            this.quotation_dateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.quotationMasterBindingSource, "quotation_date", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.quotation_dateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.quotation_dateDateTimePicker.Location = new System.Drawing.Point(178, 71);
            this.quotation_dateDateTimePicker.Name = "quotation_dateDateTimePicker";
            this.quotation_dateDateTimePicker.Size = new System.Drawing.Size(107, 20);
            this.quotation_dateDateTimePicker.TabIndex = 6;
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(178, 97);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.ReadOnly = true;
            this.itemcodeTextBox.Size = new System.Drawing.Size(107, 20);
            this.itemcodeTextBox.TabIndex = 8;
            // 
            // rateTextBox
            // 
            this.rateTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "rate", true));
            this.rateTextBox.Location = new System.Drawing.Point(178, 123);
            this.rateTextBox.Name = "rateTextBox";
            this.rateTextBox.ReadOnly = true;
            this.rateTextBox.Size = new System.Drawing.Size(107, 20);
            this.rateTextBox.TabIndex = 10;
            // 
            // deliverydateDateTimePicker
            // 
            this.deliverydateDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.quotationMasterBindingSource, "deliverydate", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "d"));
            this.deliverydateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.deliverydateDateTimePicker.Location = new System.Drawing.Point(178, 149);
            this.deliverydateDateTimePicker.Name = "deliverydateDateTimePicker";
            this.deliverydateDateTimePicker.Size = new System.Drawing.Size(107, 20);
            this.deliverydateDateTimePicker.TabIndex = 12;
            // 
            // payment_termsTextBox
            // 
            this.payment_termsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "payment_terms", true));
            this.payment_termsTextBox.Location = new System.Drawing.Point(178, 175);
            this.payment_termsTextBox.Name = "payment_termsTextBox";
            this.payment_termsTextBox.Size = new System.Drawing.Size(107, 20);
            this.payment_termsTextBox.TabIndex = 14;
            // 
            // taxTextBox
            // 
            this.taxTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.quotationMasterBindingSource, "tax", true));
            this.taxTextBox.Location = new System.Drawing.Point(178, 201);
            this.taxTextBox.Name = "taxTextBox";
            this.taxTextBox.Size = new System.Drawing.Size(107, 20);
            this.taxTextBox.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(47, 262);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(149, 262);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(247, 262);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 19;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // EditQuotation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SteelBlue;
            this.ClientSize = new System.Drawing.Size(399, 305);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(quotationnoLabel);
            this.Controls.Add(this.quotationnoTextBox);
            this.Controls.Add(rfqnoLabel);
            this.Controls.Add(this.rfqnoTextBox);
            this.Controls.Add(quotation_dateLabel);
            this.Controls.Add(this.quotation_dateDateTimePicker);
            this.Controls.Add(itemcodeLabel);
            this.Controls.Add(this.itemcodeTextBox);
            this.Controls.Add(rateLabel);
            this.Controls.Add(this.rateTextBox);
            this.Controls.Add(deliverydateLabel);
            this.Controls.Add(this.deliverydateDateTimePicker);
            this.Controls.Add(payment_termsLabel);
            this.Controls.Add(this.payment_termsTextBox);
            this.Controls.Add(taxLabel);
            this.Controls.Add(this.taxTextBox);
            this.Name = "EditQuotation";
            this.Text = "Edit Quotation Details";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quotationMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource quotationMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.QuotationMasterTableAdapter quotationMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox quotationnoTextBox;
        private System.Windows.Forms.TextBox rfqnoTextBox;
        private System.Windows.Forms.DateTimePicker quotation_dateDateTimePicker;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private System.Windows.Forms.TextBox rateTextBox;
        private System.Windows.Forms.DateTimePicker deliverydateDateTimePicker;
        private System.Windows.Forms.TextBox payment_termsTextBox;
        private System.Windows.Forms.TextBox taxTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}