﻿namespace ERP_PIMS
{
    partial class ItemDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label itemcodeLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label unit_of_measureLabel;
            this.rELERPDBDataSet = new ERP_PIMS.RELERPDBDataSet();
            this.itemMasterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.itemMasterTableAdapter = new ERP_PIMS.RELERPDBDataSetTableAdapters.ItemMasterTableAdapter();
            this.tableAdapterManager = new ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager();
            this.itemcodeTextBox = new System.Windows.Forms.TextBox();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.unit_of_measureTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            itemcodeLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            unit_of_measureLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemMasterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // itemcodeLabel
            // 
            itemcodeLabel.AutoSize = true;
            itemcodeLabel.Location = new System.Drawing.Point(33, 47);
            itemcodeLabel.Name = "itemcodeLabel";
            itemcodeLabel.Size = new System.Drawing.Size(53, 13);
            itemcodeLabel.TabIndex = 1;
            itemcodeLabel.Text = "itemcode:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(33, 73);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(61, 13);
            descriptionLabel.TabIndex = 3;
            descriptionLabel.Text = "description:";
            // 
            // unit_of_measureLabel
            // 
            unit_of_measureLabel.AutoSize = true;
            unit_of_measureLabel.Location = new System.Drawing.Point(33, 99);
            unit_of_measureLabel.Name = "unit_of_measureLabel";
            unit_of_measureLabel.Size = new System.Drawing.Size(82, 13);
            unit_of_measureLabel.TabIndex = 5;
            unit_of_measureLabel.Text = "unit of measure:";
            // 
            // rELERPDBDataSet
            // 
            this.rELERPDBDataSet.DataSetName = "RELERPDBDataSet";
            this.rELERPDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // itemMasterBindingSource
            // 
            this.itemMasterBindingSource.DataMember = "ItemMaster";
            this.itemMasterBindingSource.DataSource = this.rELERPDBDataSet;
            // 
            // itemMasterTableAdapter
            // 
            this.itemMasterTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.BudgetMasterTableAdapter = null;
            this.tableAdapterManager.CostCenterMasterTableAdapter = null;
            this.tableAdapterManager.GRMasterTableAdapter = null;
            this.tableAdapterManager.GRVMasterTableAdapter = null;
            this.tableAdapterManager.IssuesMasterTableAdapter = null;
            this.tableAdapterManager.ItemMasterTableAdapter = this.itemMasterTableAdapter;
            this.tableAdapterManager.MReqMasterTableAdapter = null;
            this.tableAdapterManager.MRetMasterTableAdapter = null;
            this.tableAdapterManager.MRMasterTableAdapter = null;
            this.tableAdapterManager.POMasterTableAdapter = null;
            this.tableAdapterManager.PRMasterTableAdapter = null;
            this.tableAdapterManager.QuotationMasterTableAdapter = null;
            this.tableAdapterManager.RFQMasterTableAdapter = null;
            this.tableAdapterManager.StockMasterTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.UserMasterTableAdapter = null;
            // 
            // itemcodeTextBox
            // 
            this.itemcodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.itemMasterBindingSource, "itemcode", true));
            this.itemcodeTextBox.Location = new System.Drawing.Point(121, 44);
            this.itemcodeTextBox.Name = "itemcodeTextBox";
            this.itemcodeTextBox.Size = new System.Drawing.Size(100, 20);
            this.itemcodeTextBox.TabIndex = 2;
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.itemMasterBindingSource, "description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(121, 70);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(100, 20);
            this.descriptionTextBox.TabIndex = 4;
            // 
            // unit_of_measureTextBox
            // 
            this.unit_of_measureTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.itemMasterBindingSource, "unit_of_measure", true));
            this.unit_of_measureTextBox.Location = new System.Drawing.Point(121, 96);
            this.unit_of_measureTextBox.Name = "unit_of_measureTextBox";
            this.unit_of_measureTextBox.Size = new System.Drawing.Size(100, 20);
            this.unit_of_measureTextBox.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 229);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(201, 229);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Close";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ItemDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 264);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(itemcodeLabel);
            this.Controls.Add(this.itemcodeTextBox);
            this.Controls.Add(descriptionLabel);
            this.Controls.Add(this.descriptionTextBox);
            this.Controls.Add(unit_of_measureLabel);
            this.Controls.Add(this.unit_of_measureTextBox);
            this.Name = "ItemDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ItemDetails";
            this.Load += new System.EventHandler(this.ItemDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.rELERPDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemMasterBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RELERPDBDataSet rELERPDBDataSet;
        private System.Windows.Forms.BindingSource itemMasterBindingSource;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.ItemMasterTableAdapter itemMasterTableAdapter;
        private ERP_PIMS.RELERPDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox itemcodeTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox unit_of_measureTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}