﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class PRdetails : Form
    {
        string msg;
        int val;
        string query = "select * from RFQMaster";
        string table = "RFQMaster";
        DbOperations dbo = new DbOperations();
        DataSet rfqDataset = new DataSet();

        public PRdetails(RELERPDBDataSet reds ,int position)
        {
            InitializeComponent();
            this.rELERPDBDataSet = reds;
            this.pRMasterBindingSource.DataSource = this.rELERPDBDataSet;
            this.pRMasterBindingSource.Position = position;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.pRMasterBindingSource.EndEdit();
                if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
                {
                    MessageBox.Show("PR details updated.", "ERP-PIMS");
                }
                else
                {
                    MessageBox.Show("PR details not updated.", "ERP-PIMS");
                }

                if (statusCheckBox.Checked == true)
                {
                    MessageBox.Show("Adding RFQ...", "ERP-PIMS");
                    rfqDataset = dbo.Select(query, table);
                    ////dataGridView1.DataSource = prDataset.Tables[table];
                    foreach (DataRow theRow in rfqDataset.Tables[table].Rows)
                    {
                        val = Convert.ToInt16(theRow["rfqno"]);
                    }


                    DataColumn[] keys = new DataColumn[1];
                    keys[0] = rfqDataset.Tables[table].Columns["rfqno"];
                    rfqDataset.Tables[table].PrimaryKey = keys;

                    DataRow findRow = rfqDataset.Tables[table].Rows.Find(++val);

                    if (findRow == null)
                    {

                        DataRow thisRow = rfqDataset.Tables[table].NewRow();
                        thisRow["rfqno"] = (val).ToString();
                        thisRow["prno"] = prnoTextBox.Text;
                        thisRow["rfqDate"] = DateTime.Now.Date;
                        rfqDataset.Tables[table].Rows.Add(thisRow);
                        msg = dbo.Update(rfqDataset, query, table);
                        MessageBox.Show(msg, "ERP-PIMS");
                    }
                    else
                    {
                        MessageBox.Show("RFQ already exists", "ERP-PIMS");
                    }
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured:" + ex.ToString(), "ERP-PIMS");
            }
            this.Close();
        }

      

       

        private void PRdetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.PRMaster' table. You can move, or remove it, as needed.
            //this.pRMasterTableAdapter.Fill(this.rELERPDBDataSet.PRMaster);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.pRMasterBindingSource.RemoveCurrent();
            if (this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet) > 0)
            {
                MessageBox.Show("PR details deleted.", "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("PR details not deleted.", "ERP-PIMS");
            }
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
