﻿#region Using Directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
#endregion

namespace ERP_PIMS
{
    public partial class PO : Form
    {

        #region Declaring Variables
        string usrDetails, usrName, usrCode;
        string msg, queryb, queryc;
        int val;
        double bamt, camt, amt, rb, rc;
        string query = "select * from POMaster";
        string table = "POMaster";
        DbOperations dbo = new DbOperations();
        DataSet podataset = new DataSet();
        DataSet fromRfq = new DataSet();
        DataSet fromBc = new DataSet();
        DataSet fromCc = new DataSet();
        DataSet calc = new DataSet();
        #endregion

        #region Form functions
        public PO()
        {
            InitializeComponent();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void tabPage3_Enter(object sender, EventArgs e)
        {
            PO_Load(sender, e);
        }
        #endregion

        #region Back To Menu
        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        #endregion

        #region Report Button
        private void button2_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }
        #endregion

        #region Form Load
        private void PO_Load(object sender, EventArgs e)
        {
            po_noTextBox.Text = comboBox2.Text = itemcodeTextBox.Text = payment_termsTextBox.Text = "";
            textBox4.Text = textBox5.Text = taxTextBox.Text = textBox3.Text = "";
            // TODO: This line of code loads data into the 'rELERPDBDataSet.QuotationMaster' table. You can move, or remove it, as needed.
            this.quotationMasterTableAdapter.Fill(this.rELERPDBDataSet.QuotationMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.CostCenterMaster' table. You can move, or remove it, as needed.
            this.costCenterMasterTableAdapter.Fill(this.rELERPDBDataSet.CostCenterMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.BudgetMaster' table. You can move, or remove it, as needed.
            this.budgetMasterTableAdapter.Fill(this.rELERPDBDataSet.BudgetMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.RFQMaster' table. You can move, or remove it, as needed.
            this.rFQMasterTableAdapter.Fill(this.rELERPDBDataSet.RFQMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.PRMaster' table. You can move, or remove it, as needed.
            this.pRMasterTableAdapter.Fill(this.rELERPDBDataSet.PRMaster);
            this.pOMasterBindingSource.MoveLast();
            //this.lblDetails.Text += this.usrName;
            //this.lblStatus.Text += this.usrDetails;
            //this.lblUsercode.Text = this.usrCode;
            podataset = dbo.Select(query, table);
            this.lblUsercode.Text = this.usrCode;
            foreach (DataRow theRow in podataset.Tables[table].Rows)
            {
                msg = theRow["po_no"].ToString();
                val = Convert.ToInt16(msg);
            }
            po_noTextBox.Text = " " + ++val;
            comboBox2_SelectedIndexChanged(sender, e);

            var bbcode = rELERPDBDataSet.BudgetMaster.AsEnumerable();

            var getbbc = from bc in bbcode
                         where bc.from_period == DateTime.Now.Year
                         select bc;
            foreach (var bc in getbbc)
            {
                textBox4.Text = bc.budgetcode.ToString();
            }

            fromBc = dbo.Select("select * from CostCenterMaster where from_period=" + DateTime.Now.Year, "CostCenterMaster");

            foreach (DataRow thisRow in fromBc.Tables["CostCenterMaster"].Rows)
            {
                textBox5.Text = thisRow["cc_code"].ToString();

            }

        }
        #endregion

        #region Searching
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (radioButton4.Checked)
                {
                    this.pOMasterTableAdapter.FillByPoNo(this.rELERPDBDataSet.POMaster, ((int)(System.Convert.ChangeType(textBox1.Text, typeof(int)))));
                }
                else
                {
                    this.pOMasterTableAdapter.FillByitemCode(this.rELERPDBDataSet.POMaster, new System.Nullable<int>(((int)(System.Convert.ChangeType(textBox2.Text, typeof(int))))));
                }
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
        #endregion

        #region ADD
        private void button7_Click(object sender, EventArgs e)
        {

            query = "select * from POMaster";

            podataset = dbo.Select(query, table);

            DataColumn[] keys = new DataColumn[1];
            keys[0] = podataset.Tables[table].Columns["po_no"];
            podataset.Tables[table].PrimaryKey = keys;

            DataRow findRow = podataset.Tables[table].Rows.Find(po_noTextBox.Text);

            if (findRow == null)
            {

                DataRow thisRow = podataset.Tables[table].NewRow();
                thisRow["po_no"] = po_noTextBox.Text;
                thisRow["quotationno"] = Convert.ToInt16(comboBox2.Text);
                thisRow["itemcode"] = itemcodeTextBox.Text;
                thisRow["amount"] = textBox3.Text;
                thisRow["deliverydate"] = dateTimePicker1.Value.ToString();
                thisRow["deliverytime"] = dateTimePicker2.Value.ToString();
                thisRow["taxes"] = taxTextBox.Text;
                thisRow["payment_terms"] = payment_termsTextBox.Text;
                thisRow["budgetcode"] = textBox4.Text;
                thisRow["cc_code"] = textBox5.Text;
                bamt = bamt - amt;


                queryb = "select * from BudgetMaster where budgetcode =" + textBox4.Text;
                calc = dbo.Select(queryb, "BudgetMaster");
                DataColumn[] keyb = new DataColumn[1];
                keyb[0] = calc.Tables["BudgetMaster"].Columns["budgetcode"];
                calc.Tables["BudgetMaster"].PrimaryKey = keyb;

                DataRow findrow = calc.Tables["BudgetMaster"].Rows.Find(textBox4.Text);

                if (findrow != null)
                {
                    findrow["amount"] = Convert.ToDecimal(bamt);
                    msg = dbo.Update(calc, queryb, "BudgetMaster");
                }
                if (msg.Length <= 20)
                {
                    queryc = "select * from CostCenterMaster where cc_code =" + textBox5.Text;
                    calc = dbo.Select(queryc, "CostCenterMaster");
                    camt = Convert.ToDouble(calc.Tables["CostCenterMaster"].Rows[0]["amount"]);
                    rc = camt;
                    camt = camt + amt;
                    keys = new DataColumn[1];
                    keys[0] = calc.Tables["CostCenterMaster"].Columns["cc_code"];
                    calc.Tables["CostCenterMaster"].PrimaryKey = keys;

                    findrow = calc.Tables["CostCenterMaster"].Rows.Find(textBox5.Text);
                    if (findrow != null)
                    {
                        findrow["amount"] = Convert.ToDecimal(camt);
                        msg = dbo.Update(calc, queryc, "CostCenterMaster");
                    }


                }
                if (msg.Length > 20)
                {
                    //rollback budget and costcenter transaction
                    queryb = "select * from BudgetMaster where budgetcode =" + textBox4.Text;
                    calc = dbo.Select(queryb, "BudgetMaster");

                    DataColumn[] keyr = new DataColumn[1];
                    keyr[0] = calc.Tables["BudgetMaster"].Columns["budgetcode"];
                    calc.Tables["BudgetMaster"].PrimaryKey = keyr;

                    DataRow findrowr = calc.Tables["BudgetMaster"].Rows.Find(textBox4.Text);

                    if (findrowr != null)
                    {
                        findrowr["amount"] = Convert.ToDecimal(rb);
                        msg = dbo.Update(calc, queryb, "BudgetMaster");
                    }
                    queryc = "select * from CostCenterMaster where cc_code =" + textBox5.Text;
                    calc = dbo.Select(queryc, "CostCenterMaster");


                    DataColumn[] keyrc = new DataColumn[1];
                    keyrc[0] = calc.Tables["CostCenterMaster"].Columns["cc_code"];
                    calc.Tables["CostCenterMaster"].PrimaryKey = keyrc;

                    findrow = calc.Tables["CostCenterMaster"].Rows.Find(textBox5.Text);
                    if (findrow != null)
                    {
                        findrow["amount"] = Convert.ToDecimal(rb);
                        msg = dbo.Update(calc, queryc, "CostCenterMaster");
                    }



                }





                podataset.Tables[table].Rows.Add(thisRow);
                msg = dbo.Update(podataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");


            }
            else
            {
                MessageBox.Show("PO already added", "ERP-PIMS");
            }
            PO_Load(sender, e);
        }
        #endregion

        #region Unrequired Function
        private void pOMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.pOMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
        private void payment_termsTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void prnoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void prnoComboBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void rfqnoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void itemcodeTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void taxTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void quantityTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void rateTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void rfqnoTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void prnoTextBox_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void prnoComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }
        #endregion

        #region Viewing entries in the grid
        private void tabPage4_Enter(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.POMaster' table. You can move, or remove it, as needed.
            this.pOMasterTableAdapter.Fill(this.rELERPDBDataSet.POMaster);
            radioButton4.Checked = true;
            textBox2.Text = textBox1.Text = "";
        }
        #endregion

        #region Selecting Radio Buttons
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = true;
            textBox2.Enabled = false;
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = !true;
            textBox2.Enabled = !false;
        }
        #endregion

        #region Autoload
        private void prnoTextBox_TextChanged(object sender, EventArgs e)
        {
            prnoComboBox.Text = prnoTextBox.Text;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            rateTextBox.Visible = rfqnoComboBox.Visible = rfqnoTextBox.Visible = quantityTextBox.Visible =
                           prnoComboBox.Visible = prnoTextBox.Visible = false;
            textBox3.Text = "";
        }
        #endregion

        #region Calculate function
        private double calculateAmount(int quantity, double rate, double tax)
        {
            double amount;
            amount = (quantity * rate) * (1 + tax / 100);
            return amount;
        }
        #endregion

        #region Save
        private void pOMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataRowView myDataRowView = (DataRowView)pOMasterDataGridView.SelectedRows[0].DataBoundItem;
            RELERPDBDataSet.POMasterRow thisRow = (RELERPDBDataSet.POMasterRow)myDataRowView.Row;
            int myPosition = rELERPDBDataSet.POMaster.Rows.IndexOf(thisRow);

            PurchaseOrderDetails frm = new PurchaseOrderDetails(rELERPDBDataSet, myPosition);
            frm.ShowDialog();
        }
        #endregion

        #region Calculate Button
        private void button5_Click(object sender, EventArgs e)
        {
            try
            {

                amt = calculateAmount(Convert.ToInt16(quantityTextBox.Text), Convert.ToDouble(rateTextBox.Text), Convert.ToDouble(taxTextBox.Text));
                textBox3.Text = amt.ToString();

                queryb = "select * from BudgetMaster where budgetcode =" + textBox4.Text;
                calc = dbo.Select(queryb, "BudgetMaster");
                bamt = Convert.ToDouble(calc.Tables["BudgetMaster"].Rows[0]["amount"]);
                rb = bamt;


                if (amt > bamt)
                {
                    MessageBox.Show("Amount exceeds budget", "ERP-PIMS");
                    button7.Enabled = false;
                }
                else
                    button7.Enabled = true;
            }


            catch (Exception exe)
            {
                MessageBox.Show("Error Occured:" + exe.ToString(), "ERP-PIMS");
            }
        }
        #endregion

        #region GotoLink
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            quotations abc = new quotations();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //PO abc = new PO();
            //abc.MdiParent = this.MdiParent;
            //abc.Show();
            //this.Hide();
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }

        private void linkLabel12_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            Stock stk = new Stock();
            stk.MdiParent = this.MdiParent;
            stk.Show();
            this.Close();
        }

        private void linkLabel14_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }

        private void linkLabel13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Close();

        }
        #endregion

    }
}
