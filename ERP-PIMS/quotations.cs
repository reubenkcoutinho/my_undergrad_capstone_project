﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP_PIMS
{
    public partial class quotations : Form
    {
        string usrDetails, usrName, usrCode, msg;
        int val;
        string table = "QuotationMaster";
        string query = "Select * From QuotationMaster";
        DataSet quoDataset = new DataSet();
        DataSet fromPr = new DataSet();
        DataSet fromRfq = new DataSet();
        DbOperations dbo = new DbOperations();

        public quotations()
        {
            InitializeComponent();
        }

        public void setInfo(string name, string details, string code)
        {
            usrCode = code;
            usrDetails = details;
            usrName = name;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }

        private void quotations_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'rELERPDBDataSet.PRMaster' table. You can move, or remove it, as needed.
            this.pRMasterTableAdapter.Fill(this.rELERPDBDataSet.PRMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.RFQMaster' table. You can move, or remove it, as needed.
            this.rFQMasterTableAdapter.Fill(this.rELERPDBDataSet.RFQMaster);
            // TODO: This line of code loads data into the 'rELERPDBDataSet.QuotationMaster' table. You can move, or remove it, as needed.
            this.quotationMasterTableAdapter.Fill(this.rELERPDBDataSet.QuotationMaster);
            //this.lblDetails.Text += this.usrName;
            //this.lblStatus.Text += this.usrDetails;
            quoDataset = dbo.Select(query, table);
            this.lblUsercode.Text = this.usrCode;
            foreach (DataRow theRow in quoDataset.Tables[table].Rows)
            {
                msg = theRow["quotationno"].ToString();
                val = Convert.ToInt16(msg);
            }
            textBox1.Text = " " + ++val;
            comboBox2_SelectedIndexChanged(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            query = "select * from QuotationMaster";

            quoDataset = dbo.Select(query, table);

            DataColumn[] keys = new DataColumn[1];
            keys[0] = quoDataset.Tables[table].Columns["quotationno"];
            quoDataset.Tables[table].PrimaryKey = keys;

            DataRow findRow = quoDataset.Tables[table].Rows.Find(textBox1.Text);

            if (findRow == null)
            {

                DataRow thisRow = quoDataset.Tables[table].NewRow();
                thisRow["quotationno"] = textBox1.Text;
                thisRow["rfqno"] = comboBox2.Text;
                thisRow["itemcode"] = itemcodeTextBox.Text;
                thisRow["rate"] = rateTextBox.Text;
                thisRow["quotation_date"] = dateTimePicker1.Value;
                thisRow["deliverydate"] = dateTimePicker2.Value;
                thisRow["payment_terms"] = textBox5.Text;
                thisRow["tax"] = textBox8.Text;
                //thisRow["quantity"] = quantityTextBox.Text;
                quoDataset.Tables[table].Rows.Add(thisRow);
                msg = dbo.Update(quoDataset, query, table);
                MessageBox.Show(msg, "ERP-PIMS");
            }
            else
            {
                MessageBox.Show("GRN already added", "ERP-PIMS");
            }
            quotations_Load(sender, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                this.quotationMasterTableAdapter.FillByQuotationno(this.rELERPDBDataSet.QuotationMaster, ((int)(System.Convert.ChangeType(textBox10.Text, typeof(int)))));
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {

        }

        private void quotationMasterBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.quotationMasterBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.rELERPDBDataSet);

        }

        private void fillByQuotationnoToolStripButton_Click(object sender, EventArgs e)
        {


        }

        private void linkLabel22_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PR abc = new PR();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel21_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //quotations abc = new quotations();
            //abc.MdiParent = this.MdiParent;
            //abc.Show();
            //this.Hide();
        }

        private void linkLabel20_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PO abc = new PO();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel16_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel19_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Budget abc = new Budget();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel15_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MReq abc = new MReq();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MRecp abc = new MRecp();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            issue abc = new issue();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Stock stk = new Stock();
            stk.MdiParent = this.MdiParent;
            stk.Show();
        }

        private void linkLabel18_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Item abc = new Item();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void linkLabel17_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            GOODS abc = new GOODS();
            abc.MdiParent = this.MdiParent;
            abc.Show();
            this.Hide();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Form Genrep = new report();
            Genrep.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void quotationMasterDataGridView_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataRowView myDataRowView = (DataRowView)quotationMasterDataGridView.SelectedRows[0].DataBoundItem;
            RELERPDBDataSet.QuotationMasterRow thisRow = (RELERPDBDataSet.QuotationMasterRow)myDataRowView.Row;
            int myPosition = rELERPDBDataSet.QuotationMaster.Rows.IndexOf(thisRow);

            EditQuotation frm = new EditQuotation(rELERPDBDataSet, myPosition);
            frm.ShowDialog();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void prnoTextBox_TextChanged(object sender, EventArgs e)
        {
            prnoComboBox.Text = prnoTextBox.Text;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            prnoTextBox.Visible = prnoComboBox.Visible = false;
        }

      
    }
}
